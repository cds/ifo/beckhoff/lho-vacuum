﻿// $Header$

// Patrick J. Thomas, California Institute of Technology, LIGO Hanford

class Program {
	[System.STAThread]
	static void Main(string[] args) {
		string solution_file_path = @"C:\SlowControls\TwinCAT3\Vacuum\LHO\Library\Vacuum\Vacuum.sln";
		string library_file_path = @"C:\SlowControls\TwinCAT3\Vacuum\LHO\Library\Vacuum\Vacuum.library";

		EnvDteUtils.MessageFilter.Register();

		System.Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.10.0", true);
		object obj = System.Activator.CreateInstance(t, true);
		EnvDTE80.DTE2 dte = (EnvDTE80.DTE2)obj;

		try {
			dte.MainWindow.Activate();

			dynamic solution = dte.Solution;

			// Opens the solution.
			solution.Open(solution_file_path);

			dynamic project = solution.Projects.Item(1);

			TCatSysManagerLib.ITcSysManager system_manager = project.Object;

			// Sets the target platform.
			TCatSysManagerLib.ITcSysManager7 sysManPlatform = (TCatSysManagerLib.ITcSysManager7)system_manager;
			TCatSysManagerLib.ITcConfigManager configManager = (TCatSysManagerLib.ITcConfigManager)sysManPlatform.ConfigurationManager;
			configManager.ActiveTargetPlatform = "TwinCAT RT (x86)";

			TCatSysManagerLib.ITcSmTreeItem plcProject = system_manager.LookupTreeItem("TIPC^Vacuum^Vacuum Project");
			TCatSysManagerLib.ITcPlcIECProject iecProject = (TCatSysManagerLib.ITcPlcIECProject)plcProject;

			iecProject.SaveAsLibrary(library_file_path, true);
		}
		catch (System.Exception e) {
			System.Console.WriteLine("{0}", e);
		}

		dte.Quit();
		System.Runtime.InteropServices.Marshal.ReleaseComObject(dte);

		EnvDteUtils.MessageFilter.Revoke();
	}
}
