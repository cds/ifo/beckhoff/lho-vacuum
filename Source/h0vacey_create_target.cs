// $Header$

// Patrick J. Thomas, California Institute of Technology, LIGO Hanford

// Enable PowerShell scripts: PS> Set-ExecutionPolicy RemoteSigned

class Program {
	[System.STAThread]
	static void Main(string[] args) {
		string solution_name = "H0VACEY";
		string solution_folder_path = @"C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACEY\H0VACEY";
		string solution_file_path = @"C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACEY\H0VACEY\H0VACEY.sln";

		string project_name = "H0VACEY";
		string project_folder_path = @"C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACEY\H0VACEY\H0VACEY";


		System.Collections.ObjectModel.Collection<System.Management.Automation.PSObject> PSOutput;
		string gvl_declaration_text;
		string main_implementation_text;

		using (System.Management.Automation.PowerShell PowerShellInstance = System.Management.Automation.PowerShell.Create()) {
			PowerShellInstance.AddScript(@". C:\SlowControls\TwinCAT3\Vacuum\LHO\Source\Scripts\h0vacey.ps1", false);
			PowerShellInstance.Invoke();

			PowerShellInstance.AddScript(@"gvl_declaration", false);
			PSOutput = PowerShellInstance.Invoke();
			gvl_declaration_text = PSOutput[0].BaseObject.ToString();

			PowerShellInstance.AddScript(@"main_implementation", false);
			PSOutput = PowerShellInstance.Invoke();
			main_implementation_text = PSOutput[0].BaseObject.ToString();
		}


		if (System.IO.Directory.Exists(solution_folder_path)) {
			System.IO.Directory.Delete(solution_folder_path, true);
		}

		if (System.IO.Directory.Exists(project_folder_path)) {
			System.IO.Directory.Delete(project_folder_path, true);
		}

		System.IO.Directory.CreateDirectory(solution_folder_path);
		System.IO.Directory.CreateDirectory(project_folder_path);


		EnvDteUtils.MessageFilter.Register();

		System.Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.10.0", true);
		object obj = System.Activator.CreateInstance(t, true);
		EnvDTE80.DTE2 dte = (EnvDTE80.DTE2)obj;

		try {
			dte.MainWindow.Activate();

			dynamic solution = dte.Solution;
			solution.Create(solution_folder_path, solution_name);
			solution.SaveAs(solution_file_path);

			string template_path = @"C:\TwinCAT\3.1\Components\Base\PrjTemplate\TwinCAT Project.tsproj";
			dynamic project = solution.AddFromTemplate(template_path, project_folder_path, project_name);

			TCatSysManagerLib.ITcSysManager system_manager = project.Object;

			string su1_xml = @"<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit1</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>";
			string su2_xml = @"<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit2</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>";
			string su3_xml = @"<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit3</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>";
			string su4_xml = @"<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit4</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>";

			// Creates the device.
			TCatSysManagerLib.ITcSmTreeItem tiid = system_manager.LookupTreeItem("TIID");
			TCatSysManagerLib.ITcSmTreeItem ethercat_master = tiid.CreateChild("EtherCAT Master", 111);

			// Creates the terminals.

			TCatSysManagerLib.ITcSmTreeItem term_m0 = ethercat_master.CreateChild("Term M0 (EK1101)", 9099, "", "EK1101-0000-0018");


			TCatSysManagerLib.ITcSmTreeItem item;

			// Inficon BPG402 Pressure Gauges: Vendor id = 00000644, Product code = 0C, Revision no = 06
			item = ethercat_master.CreateChild("Pressure Gauge ETM (BPG 402)", 9099, "", "V00000644_P0C_R06");
			item.ConsumeXml(su1_xml);

			// Inficon BPG402 Pressure Gauges: Vendor id = 00000644, Product code = 0C, Revision no = 06
			item = ethercat_master.CreateChild("Pressure Gauge NEG1 (BPG 402)", 9099, "", "V00000644_P0C_R06");
			item.ConsumeXml(su2_xml);

			// Inficon BPG402 Pressure Gauges: Vendor id = 00000644, Product code = 0C, Revision no = 08
			item = ethercat_master.CreateChild("Pressure Gauge NEG2 (BPG 402)", 9099, "", "V00000644_P0C_R08");
			item.ConsumeXml(su3_xml);

			item = ethercat_master.CreateChild("Media Converter 1 (CU1521)", 9099, "", "CU1521");
			item = ethercat_master.CreateChild("Media Converter 2 (CU1521)", 9099, "", "CU1521");

			// Inficon BPG402 Pressure Gauges: Vendor id = 00000644, Product code = 0C, Revision no = 06
			item = ethercat_master.CreateChild("Pressure Gauge Y2-8 (BPG 402)", 9099, "", "V00000644_P0C_R06");
			item.ConsumeXml(su4_xml);


			term_m0.CreateChild("Term M1 (EL1004)", 9099, "", "EL1004-0000-0018");
			term_m0.CreateChild("Term M2 (EL1004)", 9099, "", "EL1004-0000-0018");
			term_m0.CreateChild("Term M3 (EL1004)", 9099, "", "EL1004-0000-0018");
			term_m0.CreateChild("Term M4 (EL1004)", 9099, "", "EL1004-0000-0018");

			term_m0.CreateChild("Term M5 (EL2624)", 9099, "", "EL2624-0000-0018");
			term_m0.CreateChild("Term M6 (EL2624)", 9099, "", "EL2624-0000-0018");
			term_m0.CreateChild("Term M7 (EL2624)", 9099, "", "EL2624-0000-0018");
			term_m0.CreateChild("Term M8 (EL2624)", 9099, "", "EL2624-0000-0018");
			term_m0.CreateChild("Term M9 (EL2624)", 9099, "", "EL2624-0000-0018");

			term_m0.CreateChild("Term M10 (EL9400)", 9099, "", "EL9400");

			term_m0.CreateChild("Term M11 (EL3004)", 9099, "", "EL3004-0000-0020");
			term_m0.CreateChild("Term M12 (EL3004)", 9099, "", "EL3004-0000-0020");
			term_m0.CreateChild("Term M13 (EL3004)", 9099, "", "EL3004-0000-0020");
			term_m0.CreateChild("Term M14 (EL3004)", 9099, "", "EL3004-0000-0020");
			term_m0.CreateChild("Term M15 (EL3004)", 9099, "", "EL3004-0000-0020");
			term_m0.CreateChild("Term M16 (EL3004)", 9099, "", "EL3004-0000-0020");

			term_m0.CreateChild("Term M17 (EL3024)", 9099, "", "EL3024-0000-0018");
			term_m0.CreateChild("Term M18 (EL3024)", 9099, "", "EL3024-0000-0018");
			term_m0.CreateChild("Term M19 (EL3024)", 9099, "", "EL3024-0000-0018");

			term_m0.CreateChild("Term M20 (EL9400)", 9099, "", "EL9400");

			term_m0.CreateChild("Term M21 (EL3314)", 9099, "", "EL3314-0000-0019");
			term_m0.CreateChild("Term M22 (EL3314)", 9099, "", "EL3314-0000-0019");

			term_m0.CreateChild("Term M23 (EL4024)", 9099, "", "EL4024-0000-0017");
			term_m0.CreateChild("Term M24 (EL4024)", 9099, "", "EL4024-0000-0017");

			term_m0.CreateChild("Term M25 (EL9011)", 9099, "", "EL9011");


			// Creates the PLC project.
			TCatSysManagerLib.ITcSmTreeItem tipc = system_manager.LookupTreeItem("TIPC");
			tipc.CreateChild("PLC1", 0, "", @"Standard PLC Template.plcproj");


			// Adds a reference to the Vacuum library.
			TCatSysManagerLib.ITcSmTreeItem references = system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^References");
			TCatSysManagerLib.ITcPlcLibraryManager library_manager = (TCatSysManagerLib.ITcPlcLibraryManager)references;
			library_manager.AddLibrary("Vacuum", "*", "LIGO");


			// Creates the GVL file.
			TCatSysManagerLib.ITcSmTreeItem gvls_folder = system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^GVLs");
			gvls_folder.CreateChild("GVL", 615);


			// Sets the GVL declaration text.
			TCatSysManagerLib.ITcSmTreeItem gvl = system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^GVLs^GVL");
			TCatSysManagerLib.ITcPlcDeclaration gvl_declaration = (TCatSysManagerLib.ITcPlcDeclaration)gvl;
			gvl_declaration.DeclarationText = gvl_declaration_text;


			// Sets the MAIN implementation text.
			TCatSysManagerLib.ITcSmTreeItem main = system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^POUs^MAIN");
			TCatSysManagerLib.ITcPlcImplementation main_implementation = (TCatSysManagerLib.ITcPlcImplementation)main;
			main_implementation.ImplementationText = main_implementation_text;


			// Activates the configuration.
			// If this is not done a subsequent activate configuration will not create the links. It is not clear why.
			system_manager.ActivateConfiguration();


			// Saves the project.
			project.Save();


			// Saves the solution.
			// solution.Save() does not exist.
			solution.SaveAs(solution_file_path);
		}
		catch (System.Exception e) {
			System.Console.WriteLine("{0}", e);
		}

		dte.Quit();
		System.Runtime.InteropServices.Marshal.ReleaseComObject(dte);

		EnvDteUtils.MessageFilter.Revoke();
	}
}
