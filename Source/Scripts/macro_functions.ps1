# $Header$

# Patrick J. Thomas, California Institute of Technology, LIGO Hanford


# EL1004

function el1004_declaration ($ifo, $sys, $bld, $term, $link_prefix) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL1004 4 Channel Digital Input *)

	(* Channel 1 *)

	{attribute 'TcLinkTo' := '${link_prefix}^Channel 1^Input'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_BOOL | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_BOOL_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 2 *)

	{attribute 'TcLinkTo' := '${link_prefix}^Channel 2^Input'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_BOOL | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_BOOL_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 3 *)

	{attribute 'TcLinkTo' := '${link_prefix}^Channel 3^Input'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_BOOL | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_BOOL_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 4 *)

	{attribute 'TcLinkTo' := '${link_prefix}^Channel 4^Input'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_BOOL | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_BOOL_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* WcState *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^WcState'}
	${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE | AT %I*: | BOOL; | (*~ (FALSE = Data valid, TRUE = Data invalid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Data invalid: ONAM)
	| | | (OPC_PROP[0107] :Data valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^InputToggle'}
	${ifo}_${sys}_${bld}_${term}_WCSTATE_INPUTTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	(* InfoData *)

	{attribute 'TcLinkTo' := '${link_prefix}^InfoData^State'}
	${ifo}_${sys}_${bld}_${term}_INFODATA_STATE | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)
"@

return $text
}

function el1004_error_implementation ($ifo, $sys, $bld, $term) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL1004 *)

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${term}_CHAN1_IN_BOOL_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN1_IN_BOOL_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${term}_CHAN2_IN_BOOL_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN2_IN_BOOL_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${term}_CHAN3_IN_BOOL_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN3_IN_BOOL_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${term}_CHAN4_IN_BOOL_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN4_IN_BOOL_ERROR := FALSE;
	END_IF
"@

return $text
}


#-------------------------------------------------------------------------------
# EL2624

function el2624_declaration ($ifo, $sys, $bld, $term, $link_prefix) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL2624 4 Channel Relay Output *)

	(* Channel 1 *)

	{attribute 'TcLinkTo' := '${link_prefix}^Channel 1^Output'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_BOOL | AT %Q*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_BOOL_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 2 *)

	{attribute 'TcLinkTo' := '${link_prefix}^Channel 2^Output'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_BOOL | AT %Q*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_BOOL_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 3 *)

	{attribute 'TcLinkTo' := '${link_prefix}^Channel 3^Output'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_BOOL | AT %Q*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_BOOL_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 4 *)

	{attribute 'TcLinkTo' := '${link_prefix}^Channel 4^Output'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_BOOL | AT %Q*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_BOOL_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* WcState *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^WcState'}
	${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE | AT %I*: | BOOL; | (*~ (FALSE = Data valid, TRUE = Data invalid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Data invalid: ONAM)
	| | | (OPC_PROP[0107] :Data valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* InfoData *)

	{attribute 'TcLinkTo' := '${link_prefix}^InfoData^State'}
	${ifo}_${sys}_${bld}_${term}_INFODATA_STATE | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)
"@

return $text
}

function el2624_error_implementation ($ifo, $sys, $bld, $term) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL2624 *)

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_BOOL_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_BOOL_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_BOOL_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_BOOL_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_BOOL_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_BOOL_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_BOOL_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_BOOL_ERROR := FALSE;
	END_IF
"@

return $text
}


#-------------------------------------------------------------------------------
# EL3004

function el3004_declaration ($ifo, $sys, $bld, $term, $link_prefix) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL3004 4 Channel Analog Input *)

	(* Channel 1 *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^Underrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_UNDERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^Overrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_OVERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^Limit 1'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_LIMIT1 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^Limit 2'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_LIMIT2 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^Error'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_ERROR | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange or Underrange, TRUE = Overrange or Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^TxPDO State'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_TXPDOSTATE | AT %I*: | BOOL; | (*~ (FALSE = TxPDO is valid, TRUE = TxPDO is not valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :TxPDO is not valid: ONAM)
	| | | (OPC_PROP[0107] :TxPDO is valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^TxPDO Toggle'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_TXPDOTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Value'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS | AT %I*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_VOLTS: | | LREAL; | (*~ (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Volts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 2 *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^Underrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_UNDERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^Overrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_OVERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^Limit 1'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_LIMIT1 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^Limit 2'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_LIMIT2 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^Error'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_ERROR | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange or Underrange, TRUE = Overrange or Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^TxPDO State'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_TXPDOSTATE | AT %I*: | BOOL; | (*~ (FALSE = TxPDO is valid, TRUE = TxPDO is not valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :TxPDO is not valid: ONAM)
	| | | (OPC_PROP[0107] :TxPDO is valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^TxPDO Toggle'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_TXPDOTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Value'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS | AT %I*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_VOLTS: | | LREAL; | (*~ (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Volts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 3 *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^Underrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_UNDERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^Overrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_OVERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^Limit 1'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_LIMIT1 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^Limit 2'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_LIMIT2 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^Error'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_ERROR | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange or Underrange, TRUE = Overrange or Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^TxPDO State'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_TXPDOSTATE | AT %I*: | BOOL; | (*~ (FALSE = TxPDO is valid, TRUE = TxPDO is not valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :TxPDO is not valid: ONAM)
	| | | (OPC_PROP[0107] :TxPDO is valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^TxPDO Toggle'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_TXPDOTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Value'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS | AT %I*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_VOLTS: | | LREAL; | (*~ (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Volts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 4 *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^Underrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_UNDERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^Overrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_OVERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^Limit 1'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_LIMIT1 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^Limit 2'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_LIMIT2 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^Error'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_ERROR | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange or Underrange, TRUE = Overrange or Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^TxPDO State'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_TXPDOSTATE | AT %I*: | BOOL; | (*~ (FALSE = TxPDO is valid, TRUE = TxPDO is not valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :TxPDO is not valid: ONAM)
	| | | (OPC_PROP[0107] :TxPDO is valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^TxPDO Toggle'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_TXPDOTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Value'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS | AT %I*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_VOLTS: | | LREAL; | (*~ (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Volts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* WcState *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^WcState'}
	${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE | AT %I*: | BOOL; | (*~ (FALSE = Data valid, TRUE = Data invalid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Data invalid: ONAM)
	| | | (OPC_PROP[0107] :Data valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^InputToggle'}
	${ifo}_${sys}_${bld}_${term}_WCSTATE_INPUTTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	(* InfoData *)

	{attribute 'TcLinkTo' := '${link_prefix}^InfoData^State'}
	${ifo}_${sys}_${bld}_${term}_INFODATA_STATE | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)
"@

return $text
}

function el3004_error_implementation ($ifo, $sys, $bld, $term) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL3004 *)

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 OR ${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_TXPDOSTATE = TRUE THEN
		${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 OR ${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_TXPDOSTATE = TRUE THEN
		${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 OR ${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_TXPDOSTATE = TRUE THEN
		${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 OR ${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_TXPDOSTATE = TRUE THEN
		${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS_ERROR := FALSE;
	END_IF


	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_VOLTS_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS_ERROR;
	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_VOLTS_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS_ERROR;
	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_VOLTS_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS_ERROR;
	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_VOLTS_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS_ERROR;
"@

return $text
}

function el3004_implementation ($ifo, $sys, $bld, $term) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL3004 *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_VOLTS := EL3004CountsToVoltsFun(${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS);

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_VOLTS := EL3004CountsToVoltsFun(${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS);

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_VOLTS := EL3004CountsToVoltsFun(${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS);

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_VOLTS := EL3004CountsToVoltsFun(${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS);
"@

return $text
}


#-------------------------------------------------------------------------------
# EL3024

function el3024_declaration ($ifo, $sys, $bld, $term, $link_prefix) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL3024 4 Channel Analog Input *)

	(* Channel 1 *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^Underrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_UNDERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^Overrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_OVERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^Limit 1'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_LIMIT1 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^Limit 2'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_LIMIT2 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^Error'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_ERROR | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange or Underrange, TRUE = Overrange or Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^TxPDO State'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_TXPDOSTATE | AT %I*: | BOOL; | (*~ (FALSE = TxPDO is valid, TRUE = TxPDO is not valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :TxPDO is not valid: ONAM)
	| | | (OPC_PROP[0107] :TxPDO is valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Status^TxPDO Toggle'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_TXPDOTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 1^Value'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS | AT %I*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_MA: | | LREAL; | (*~ (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 2 *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^Underrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_UNDERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^Overrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_OVERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^Limit 1'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_LIMIT1 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^Limit 2'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_LIMIT2 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^Error'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_ERROR | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange or Underrange, TRUE = Overrange or Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^TxPDO State'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_TXPDOSTATE | AT %I*: | BOOL; | (*~ (FALSE = TxPDO is valid, TRUE = TxPDO is not valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :TxPDO is not valid: ONAM)
	| | | (OPC_PROP[0107] :TxPDO is valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Status^TxPDO Toggle'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_TXPDOTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 2^Value'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS | AT %I*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_MA: | | LREAL; | (*~ (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 3 *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^Underrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_UNDERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^Overrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_OVERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^Limit 1'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_LIMIT1 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^Limit 2'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_LIMIT2 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^Error'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_ERROR | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange or Underrange, TRUE = Overrange or Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^TxPDO State'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_TXPDOSTATE | AT %I*: | BOOL; | (*~ (FALSE = TxPDO is valid, TRUE = TxPDO is not valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :TxPDO is not valid: ONAM)
	| | | (OPC_PROP[0107] :TxPDO is valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Status^TxPDO Toggle'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_TXPDOTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 3^Value'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS | AT %I*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_MA: | | LREAL; | (*~ (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 4 *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^Underrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_UNDERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^Overrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_OVERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^Limit 1'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_LIMIT1 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^Limit 2'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_LIMIT2 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^Error'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_ERROR | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange or Underrange, TRUE = Overrange or Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^TxPDO State'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_TXPDOSTATE | AT %I*: | BOOL; | (*~ (FALSE = TxPDO is valid, TRUE = TxPDO is not valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :TxPDO is not valid: ONAM)
	| | | (OPC_PROP[0107] :TxPDO is valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Status^TxPDO Toggle'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_TXPDOTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^AI Standard Channel 4^Value'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS | AT %I*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_MA: | | LREAL; | (*~ (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* WcState *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^WcState'}
	${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE | AT %I*: | BOOL; | (*~ (FALSE = Data valid, TRUE = Data invalid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Data invalid: ONAM)
	| | | (OPC_PROP[0107] :Data valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^InputToggle'}
	${ifo}_${sys}_${bld}_${term}_WCSTATE_INPUTTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	(* InfoData *)

	{attribute 'TcLinkTo' := '${link_prefix}^InfoData^State'}
	${ifo}_${sys}_${bld}_${term}_INFODATA_STATE | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)
"@

return $text
}

function el3024_error_implementation ($ifo, $sys, $bld, $term) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL3024 *)

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 OR ${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_TXPDOSTATE = TRUE THEN
		${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 OR ${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_TXPDOSTATE = TRUE THEN
		${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 OR ${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_TXPDOSTATE = TRUE THEN
		${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 OR ${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_TXPDOSTATE = TRUE THEN
		${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS_ERROR := FALSE;
	END_IF


	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_MA_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS_ERROR;
	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_MA_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS_ERROR;
	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_MA_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS_ERROR;
	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_MA_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS_ERROR;
"@

return $text
}

function el3024_implementation ($ifo, $sys, $bld, $term) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL3024 *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_MA := EL3024CountsToMilliAmpsFun(${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS);

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_MA := EL3024CountsToMilliAmpsFun(${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS);

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_MA := EL3024CountsToMilliAmpsFun(${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS);

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_MA := EL3024CountsToMilliAmpsFun(${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS);
"@

return $text
}


#-------------------------------------------------------------------------------
# EL3314

function el3314_declaration ($ifo, $sys, $bld, $term, $link_prefix) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL3314 4 Channel Analog Input Thermocouple *)

	(* Channel 1 *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 1^Status^Underrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_UNDERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 1^Status^Overrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_OVERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 1^Status^Limit 1'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_LIMIT1 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 1^Status^Limit 2'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_LIMIT2 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 1^Status^Error'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_ERROR | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange or Underrange, TRUE = Overrange or Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 1^Status^TxPDO State'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_TXPDOSTATE | AT %I*: | BOOL; | (*~ (FALSE = TxPDO is valid, TRUE = TxPDO is not valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :TxPDO is not valid: ONAM)
	| | | (OPC_PROP[0107] :TxPDO is valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 1^Status^TxPDO Toggle'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_TXPDOTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 1^Value'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS | AT %I*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_DEGC: | | LREAL; | (*~ (DegC)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 2 *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 2^Status^Underrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_UNDERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 2^Status^Overrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_OVERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 2^Status^Limit 1'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_LIMIT1 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 2^Status^Limit 2'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_LIMIT2 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 2^Status^Error'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_ERROR | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange or Underrange, TRUE = Overrange or Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 2^Status^TxPDO State'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_TXPDOSTATE | AT %I*: | BOOL; | (*~ (FALSE = TxPDO is valid, TRUE = TxPDO is not valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :TxPDO is not valid: ONAM)
	| | | (OPC_PROP[0107] :TxPDO is valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 2^Status^TxPDO Toggle'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_TXPDOTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 2^Value'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS | AT %I*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_DEGC: | | LREAL; | (*~ (DegC)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 3 *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 3^Status^Underrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_UNDERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 3^Status^Overrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_OVERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 3^Status^Limit 1'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_LIMIT1 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 3^Status^Limit 2'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_LIMIT2 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 3^Status^Error'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_ERROR | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange or Underrange, TRUE = Overrange or Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 3^Status^TxPDO State'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_TXPDOSTATE | AT %I*: | BOOL; | (*~ (FALSE = TxPDO is valid, TRUE = TxPDO is not valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :TxPDO is not valid: ONAM)
	| | | (OPC_PROP[0107] :TxPDO is valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 3^Status^TxPDO Toggle'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_TXPDOTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 3^Value'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS | AT %I*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_DEGC: | | LREAL; | (*~ (DegC)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 4 *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 4^Status^Underrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_UNDERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 4^Status^Overrange'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_OVERRANGE | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 4^Status^Limit 1'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_LIMIT1 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 4^Status^Limit 2'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_LIMIT2 | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 4^Status^Error'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_ERROR | AT %I*: | BOOL; | (*~ (FALSE = Not Overrange or Underrange, TRUE = Overrange or Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 4^Status^TxPDO State'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_TXPDOSTATE | AT %I*: | BOOL; | (*~ (FALSE = TxPDO is valid, TRUE = TxPDO is not valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :TxPDO is not valid: ONAM)
	| | | (OPC_PROP[0107] :TxPDO is valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 4^Status^TxPDO Toggle'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_TXPDOTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	{attribute 'TcLinkTo' := '${link_prefix}^TC Inputs Channel 4^Value'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS | AT %I*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_DEGC: | | LREAL; | (*~ (DegC)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* WcState *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^WcState'}
	${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE | AT %I*: | BOOL; | (*~ (FALSE = Data valid, TRUE = Data invalid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Data invalid: ONAM)
	| | | (OPC_PROP[0107] :Data valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^InputToggle'}
	${ifo}_${sys}_${bld}_${term}_WCSTATE_INPUTTOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)

	(* InfoData *)

	{attribute 'TcLinkTo' := '${link_prefix}^InfoData^State'}
	${ifo}_${sys}_${bld}_${term}_INFODATA_STATE | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)
"@

return $text
}

function el3314_error_implementation ($ifo, $sys, $bld, $term) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL3314 *)

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 OR ${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${term}_CHAN1_STATUS_TXPDOSTATE = TRUE THEN
		${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 OR ${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${term}_CHAN2_STATUS_TXPDOSTATE = TRUE THEN
		${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 OR ${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${term}_CHAN3_STATUS_TXPDOSTATE = TRUE THEN
		${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 OR ${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${term}_CHAN4_STATUS_TXPDOSTATE = TRUE THEN
		${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS_ERROR := FALSE;
	END_IF


	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_DEGC_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS_ERROR;
	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_DEGC_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS_ERROR;
	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_DEGC_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS_ERROR;
	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_DEGC_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS_ERROR;
"@

return $text
}

function el3314_implementation ($ifo, $sys, $bld, $term) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL3314 *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_IN_DEGC := EL3314CountsToDegreesCelsiusFun(${ifo}_${sys}_${bld}_${term}_CHAN1_IN_COUNTS);

	${ifo}_${sys}_${bld}_${term}_CHAN2_IN_DEGC := EL3314CountsToDegreesCelsiusFun(${ifo}_${sys}_${bld}_${term}_CHAN2_IN_COUNTS);

	${ifo}_${sys}_${bld}_${term}_CHAN3_IN_DEGC := EL3314CountsToDegreesCelsiusFun(${ifo}_${sys}_${bld}_${term}_CHAN3_IN_COUNTS);

	${ifo}_${sys}_${bld}_${term}_CHAN4_IN_DEGC := EL3314CountsToDegreesCelsiusFun(${ifo}_${sys}_${bld}_${term}_CHAN4_IN_COUNTS);
"@

return $text
}


#-------------------------------------------------------------------------------
# EL4024

function el4024_declaration ($ifo, $sys, $bld, $term, $link_prefix) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL4024 4 Channel Analog Output *)

	(* Channel 1 *)

	{attribute 'TcLinkTo' := '${link_prefix}^AO Outputs Channel 1^Analog output'}
	${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_COUNTS | AT %Q*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_MA: | | LREAL; | (*~ (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 2 *)

	{attribute 'TcLinkTo' := '${link_prefix}^AO Outputs Channel 2^Analog output'}
	${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_COUNTS | AT %Q*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_MA: | | LREAL; | (*~ (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 3 *)

	{attribute 'TcLinkTo' := '${link_prefix}^AO Outputs Channel 3^Analog output'}
	${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_COUNTS | AT %Q*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_MA: | | LREAL; | (*~ (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Channel 4 *)

	{attribute 'TcLinkTo' := '${link_prefix}^AO Outputs Channel 4^Analog output'}
	${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_COUNTS | AT %Q*: | INT; | (*~ (Counts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Counts: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_COUNTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_MA: | | LREAL; | (*~ (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU) *)

	${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* WcState *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^WcState'}
	${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE | AT %I*: | BOOL; | (*~ (FALSE = Data valid, TRUE = Data invalid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Data invalid: ONAM)
	| | | (OPC_PROP[0107] :Data valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* InfoData *)

	{attribute 'TcLinkTo' := '${link_prefix}^InfoData^State'}
	${ifo}_${sys}_${bld}_${term}_INFODATA_STATE | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only) *)
"@

return $text
}

function el4024_error_implementation ($ifo, $sys, $bld, $term) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL4024 *)

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_COUNTS_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_COUNTS_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_COUNTS_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${term}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${term}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_COUNTS_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_COUNTS_ERROR := FALSE;
	END_IF


	${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_MA_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_COUNTS_ERROR;
	${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_MA_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_COUNTS_ERROR;
	${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_MA_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_COUNTS_ERROR;
	${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_MA_ERROR := ${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_COUNTS_ERROR;
"@

return $text
}

function el4024_implementation ($ifo, $sys, $bld, $term) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${term} EL4024 *)

	${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_COUNTS := EL4024MilliAmpsToCountsFun(${ifo}_${sys}_${bld}_${term}_CHAN1_OUT_MA);

	${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_COUNTS := EL4024MilliAmpsToCountsFun(${ifo}_${sys}_${bld}_${term}_CHAN2_OUT_MA);

	${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_COUNTS := EL4024MilliAmpsToCountsFun(${ifo}_${sys}_${bld}_${term}_CHAN3_OUT_MA);

	${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_COUNTS := EL4024MilliAmpsToCountsFun(${ifo}_${sys}_${bld}_${term}_CHAN4_OUT_MA);
"@

return $text
}


#-------------------------------------------------------------------------------
# BPG 402 V1.2.1.0

function bpg402_v1_2_1_0_declaration ($ifo, $sys, $bld, $loc, $inst, $link_prefix) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} BPG 402 V1.2.1.0 *)

	(* Module 1 *)

	(* Reading Valid *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Reading Valid Combination Gauge'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_VALID | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 1 Reading Valid Combination Gauge (FALSE = Not Valid, TRUE = Valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Reading Valid: DESC)
	| | | (OPC_PROP[0106] :Valid: ONAM)
	| | | (OPC_PROP[0107] :Not Valid: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	(* Overrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Overrange Exceeded Combination Gauge'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_OVERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 1 Overrange Exceeded Combination Gauge (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Overrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Underrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Underrange Exceeded Combination Gauge'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_UNDERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 1 Underrange Exceeded Combination Gauge (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Underrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Sensor Value *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Sensor Value Combination Gauge'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR | AT %I*: | REAL; | (*~ ${loc} ${inst} Module 1 Sensor Value Combination Gauge (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Sensor Value: DESC)
	| | | (OPC_PROP[0102] :1000: HOPR)
	| | | (OPC_PROP[0103] :3.0E-11: LOPR)
	| | | (OPC_PROP[0307] :0.001: HIHI)
	| | | (OPC_PROP[0308] :1E-5: HIGH)
	| | | (OPC_PROP[0309] :3.16E-10: LOW)
	| | | (OPC_PROP[0310] :3.16E-11: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Active Sensor Number *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Active Sensor Number Combination Gauge'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_SENSOR | AT %I*: | UINT; | (*~ ${loc} ${inst} Module 1 Active Sensor Number Combination Gauge
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Active Sensor Number: DESC) *)

	(* Trip Point Output *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Trip Point Output All Instance'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_TRIP | AT %I*: | UDINT; | (*~ ${loc} ${inst} Module 1 Trip Point Output All Instance
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Trip Point Output All: DESC) *)

	(* Module 2 *)

	(* Reading Valid *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Reading Valid Combination Gauge'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_VALID | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 2 Reading Valid Combination Gauge (FALSE = Not Valid, TRUE = Valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Reading Valid: DESC)
	| | | (OPC_PROP[0106] :Valid: ONAM)
	| | | (OPC_PROP[0107] :Not Valid: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	(* Overrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Overrange Exceeded Combination Gauge'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_OVERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 2 Overrange Exceeded Combination Gauge (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Overrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Underrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Underrange Exceeded Combination Gauge'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_UNDERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 2 Underrange Exceeded Combination Gauge (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Underrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Sensor Value *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Sensor Value Combination Gauge'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR | AT %I*: | REAL; | (*~ ${loc} ${inst} Module 2 Sensor Value Combination Gauge (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Sensor Value: DESC)
	| | | (OPC_PROP[0102] :1000: HOPR)
	| | | (OPC_PROP[0103] :3.0E-11: LOPR)
	| | | (OPC_PROP[0307] :0.001: HIHI)
	| | | (OPC_PROP[0308] :1E-5: HIGH)
	| | | (OPC_PROP[0309] :3.16E-10: LOW)
	| | | (OPC_PROP[0310] :3.16E-11: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Active Sensor Number *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Active Sensor Number Combination Gauge'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_SENSOR | AT %I*: | UINT; | (*~ ${loc} ${inst} Module 2 Active Sensor Number Combination Gauge
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Active Sensor Number: DESC) *)

	(* Trip Point Output *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Trip Point Output All Instance'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_TRIP | AT %I*: | UDINT; | (*~ ${loc} ${inst} Module 2 Trip Point Output All Instance
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Trip Point Output All: DESC) *)

	(* WcState *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^WcState'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_WCSTATE_WCSTATE | AT %I*: | BOOL; | (*~ (FALSE = Data valid, TRUE = Data invalid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Working Counter State: DESC)
	| | | (OPC_PROP[0106] :Data invalid: ONAM)
	| | | (OPC_PROP[0107] :Data valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^InputToggle'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_WCSTATE_TOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Input Toggle: DESC) *)

	(* InfoData *)

	{attribute 'TcLinkTo' := '${link_prefix}^InfoData^State'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_INFODATA_STATE | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Slave State: DESC) *)

	(* Error *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function bpg402_v1_2_1_0_error_implementation ($ifo, $sys, $bld, $loc, $inst) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} BPG 402 V1.2.1.0 *)

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_VALID = FALSE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_OVERRANGE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_UNDERRANGE = TRUE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_VALID = FALSE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_OVERRANGE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_UNDERRANGE = TRUE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR_ERROR := FALSE;
	END_IF
"@

return $text
}


#-------------------------------------------------------------------------------
# BPG 402 V1.3.0.0

function bpg402_v1_3_0_0_declaration ($ifo, $sys, $bld, $loc, $inst, $link_prefix) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} BPG 402 V1.3.0.0 *)

	(* Module 1 *)

	(* Reading Valid *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Combination Gauge Reading Valid'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_VALID | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 1 Combination Gauge Reading Valid (FALSE = Not Valid, TRUE = Valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Reading Valid: DESC)
	| | | (OPC_PROP[0106] :Valid: ONAM)
	| | | (OPC_PROP[0107] :Not Valid: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	(* Overrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Combination Gauge Overrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_OVERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 1 Combination Gauge Overrange Exceeded (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Overrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Underrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Combination Gauge Underrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_UNDERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 1 Combination Gauge Underrange Exceeded (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Underrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Active Value *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Combination Gauge Active Value'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR | AT %I*: | REAL; | (*~ ${loc} ${inst} Module 1 Combination Gauge Active Value (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Active Value: DESC)
	| | | (OPC_PROP[0102] :1000: HOPR)
	| | | (OPC_PROP[0103] :3.0E-11: LOPR)
	| | | (OPC_PROP[0307] :0.001: HIHI)
	| | | (OPC_PROP[0308] :1E-5: HIGH)
	| | | (OPC_PROP[0309] :3.16E-10: LOW)
	| | | (OPC_PROP[0310] :3.16E-11: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Active Sensor Number *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Combination Gauge Active Sensor Number'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_SENSOR | AT %I*: | UINT; | (*~ ${loc} ${inst} Module 1 Combination Gauge Active Sensor Number
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Active Sensor Number: DESC) *)

	(* Trip Point Output *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Trip Point Output All Instance'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_TRIP | AT %I*: | UDINT; | (*~ ${loc} ${inst} Module 1 Trip Point Output All Instance
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Trip Point Output All: DESC) *)

	(* Module 2 *)

	(* Reading Valid *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Combination Gauge Reading Valid'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_VALID | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 2 Combination Gauge Reading Valid (FALSE = Not Valid, TRUE = Valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Reading Valid: DESC)
	| | | (OPC_PROP[0106] :Valid: ONAM)
	| | | (OPC_PROP[0107] :Not Valid: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	(* Overrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Combination Gauge Overrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_OVERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 2 Combination Gauge Overrange Exceeded (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Overrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Underrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Combination Gauge Underrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_UNDERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 2 Combination Gauge Underrange Exceeded (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Underrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Active Value *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Combination Gauge Active Value'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR | AT %I*: | REAL; | (*~ ${loc} ${inst} Module 2 Combination Gauge Active Value (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Active Value: DESC)
	| | | (OPC_PROP[0102] :1000: HOPR)
	| | | (OPC_PROP[0103] :3.0E-11: LOPR)
	| | | (OPC_PROP[0307] :0.001: HIHI)
	| | | (OPC_PROP[0308] :1E-5: HIGH)
	| | | (OPC_PROP[0309] :3.16E-10: LOW)
	| | | (OPC_PROP[0310] :3.16E-11: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Active Sensor Number *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Combination Gauge Active Sensor Number'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_SENSOR | AT %I*: | UINT; | (*~ ${loc} ${inst} Module 2 Combination Gauge Active Sensor Number
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Active Sensor Number: DESC) *)

	(* Trip Point Output *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Trip Point Output All Instance'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_TRIP | AT %I*: | UDINT; | (*~ ${loc} ${inst} Module 2 Trip Point Output All Instance
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Trip Point Output All: DESC) *)

	(* WcState *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^WcState'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_WCSTATE_WCSTATE | AT %I*: | BOOL; | (*~ (FALSE = Data valid, TRUE = Data invalid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Working Counter State: DESC)
	| | | (OPC_PROP[0106] :Data invalid: ONAM)
	| | | (OPC_PROP[0107] :Data valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^InputToggle'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_WCSTATE_TOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Input Toggle: DESC) *)

	(* InfoData *)

	{attribute 'TcLinkTo' := '${link_prefix}^InfoData^State'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_INFODATA_STATE | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Slave State: DESC) *)

	(* Error *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function bpg402_v1_3_0_0_error_implementation ($ifo, $sys, $bld, $loc, $inst) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} BPG 402 V1.3.0.0 *)

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_VALID = FALSE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_OVERRANGE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_UNDERRANGE = TRUE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_VALID = FALSE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_OVERRANGE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_UNDERRANGE = TRUE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR_ERROR := FALSE;
	END_IF
"@

return $text
}


#-------------------------------------------------------------------------------
# BCG450

function bcg450_declaration ($ifo, $sys, $bld, $loc, $inst, $link_prefix) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} BCG 450 *)

	(* Module 1 *)

	(* Reading Valid *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Combination Gauge Reading Valid'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_VALID | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 1 Combination Gauge Reading Valid (FALSE = Not Valid, TRUE = Valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Reading Valid: DESC)
	| | | (OPC_PROP[0106] :Valid: ONAM)
	| | | (OPC_PROP[0107] :Not Valid: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	(* Overrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Combination Gauge Overrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_OVERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 1 Combination Gauge Overrange Exceeded (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Overrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Underrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Combination Gauge Underrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_UNDERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 1 Combination Gauge Underrange Exceeded (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Underrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Active Value *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Combination Gauge Active Value'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR | AT %I*: | REAL; | (*~ ${loc} ${inst} Module 1 Combination Gauge Active Value (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Active Value: DESC)
	| | | (OPC_PROP[0102] :1000: HOPR)
	| | | (OPC_PROP[0103] :3.0E-11: LOPR)
	| | | (OPC_PROP[0307] :0.001: HIHI)
	| | | (OPC_PROP[0308] :1E-5: HIGH)
	| | | (OPC_PROP[0309] :3.16E-10: LOW)
	| | | (OPC_PROP[0310] :3.16E-11: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Active Sensor Number *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Combination Gauge Active Sensor Number'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_SENSOR | AT %I*: | UINT; | (*~ ${loc} ${inst} Module 1 Combination Gauge Active Sensor Number
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Active Sensor Number: DESC) *)

	(* Trip Point Output *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 1^Trip Point Output All Instance'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_TRIP | AT %I*: | UDINT; | (*~ ${loc} ${inst} Module 1 Trip Point Output All Instance
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 1 Trip Point Output All: DESC) *)

	(* Module 2 *)

	(* Reading Valid *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Reading Valid Module 2'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_VALID | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 2 Reading Valid (FALSE = Not Valid, TRUE = Valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Reading Valid: DESC)
	| | | (OPC_PROP[0106] :Valid: ONAM)
	| | | (OPC_PROP[0107] :Not Valid: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	(* Overrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Overrange Exceeded Module 2'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_OVERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 2 Overrange Exceeded (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Overrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Underrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Underrange Exceeded Module 2'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_UNDERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 2 Underrange Exceeded (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Underrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Sensor Value *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 2^Sensor Value Module 2'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR | AT %I*: | REAL; | (*~ ${loc} ${inst} Module 2 Sensor Value (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 2 Sensor Value: DESC)
	| | | (OPC_PROP[0102] :1000: HOPR)
	| | | (OPC_PROP[0103] :3.0E-11: LOPR)
	| | | (OPC_PROP[0307] :0.001: HIHI)
	| | | (OPC_PROP[0308] :1E-5: HIGH)
	| | | (OPC_PROP[0309] :3.16E-10: LOW)
	| | | (OPC_PROP[0310] :3.16E-11: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Module 3 *)

	(* Reading Valid *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 3^Combination Gauge Reading Valid'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD3_VALID | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 3 Combination Gauge Reading Valid (FALSE = Not Valid, TRUE = Valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 3 Reading Valid: DESC)
	| | | (OPC_PROP[0106] :Valid: ONAM)
	| | | (OPC_PROP[0107] :Not Valid: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	(* Overrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 3^Combination Gauge Overrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD3_OVERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 3 Combination Gauge Overrange Exceeded (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 3 Overrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Underrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 3^Combination Gauge Underrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD3_UNDERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 3 Combination Gauge Underrange Exceeded (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 3 Underrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Active Value *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 3^Combination Gauge Active Value'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD3_PRESS_TORR | AT %I*: | REAL; | (*~ ${loc} ${inst} Module 3 Combination Gauge Active Value (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 3 Active Value: DESC)
	| | | (OPC_PROP[0102] :1000: HOPR)
	| | | (OPC_PROP[0103] :3.0E-11: LOPR)
	| | | (OPC_PROP[0307] :0.001: HIHI)
	| | | (OPC_PROP[0308] :1E-5: HIGH)
	| | | (OPC_PROP[0309] :3.16E-10: LOW)
	| | | (OPC_PROP[0310] :3.16E-11: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD3_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Active Sensor Number *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 3^Combination Gauge Active Sensor Number'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD3_SENSOR | AT %I*: | UINT; | (*~ ${loc} ${inst} Module 3 Combination Gauge Active Sensor Number
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 3 Active Sensor Number: DESC) *)

	(* Trip Point Output *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 3^Trip Point Output All Instance'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD3_TRIP | AT %I*: | UDINT; | (*~ ${loc} ${inst} Module 3 Trip Point Output All Instance
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 3 Trip Point Output All: DESC) *)

	(* Module 4 *)

	(* Reading Valid *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 4^Combination Gauge Reading Valid'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD4_VALID | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 4 Combination Gauge Reading Valid (FALSE = Not Valid, TRUE = Valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 4 Reading Valid: DESC)
	| | | (OPC_PROP[0106] :Valid: ONAM)
	| | | (OPC_PROP[0107] :Not Valid: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	(* Overrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 4^Combination Gauge Overrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD4_OVERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 4 Combination Gauge Overrange Exceeded (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 4 Overrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Underrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 4^Combination Gauge Underrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD4_UNDERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Module 4 Combination Gauge Underrange Exceeded (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 4 Underrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Active Value *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 4^Combination Gauge Active Value'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD4_PRESS_TORR | AT %I*: | REAL; | (*~ ${loc} ${inst} Module 4 Combination Gauge Active Value (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 4 Active Value: DESC)
	| | | (OPC_PROP[0102] :1000: HOPR)
	| | | (OPC_PROP[0103] :3.0E-11: LOPR)
	| | | (OPC_PROP[0307] :0.001: HIHI)
	| | | (OPC_PROP[0308] :1E-5: HIGH)
	| | | (OPC_PROP[0309] :3.16E-10: LOW)
	| | | (OPC_PROP[0310] :3.16E-11: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD4_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	(* Active Sensor Number *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 4^Combination Gauge Active Sensor Number'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD4_SENSOR | AT %I*: | UINT; | (*~ ${loc} ${inst} Module 4 Combination Gauge Active Sensor Number
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 4 Active Sensor Number: DESC) *)

	(* Trip Point Output *)

	{attribute 'TcLinkTo' := '${link_prefix}^Transmit PDO Mapping Module 4^Trip Point Output All Instance'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD4_TRIP | AT %I*: | UDINT; | (*~ ${loc} ${inst} Module 4 Trip Point Output All Instance
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Module 4 Trip Point Output All: DESC) *)

	(* WcState *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^WcState'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_WCSTATE_WCSTATE | AT %I*: | BOOL; | (*~ (FALSE = Data valid, TRUE = Data invalid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Working Counter State: DESC)
	| | | (OPC_PROP[0106] :Data invalid: ONAM)
	| | | (OPC_PROP[0107] :Data valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^InputToggle'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_WCSTATE_TOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Input Toggle: DESC) *)

	(* InfoData *)

	{attribute 'TcLinkTo' := '${link_prefix}^InfoData^State'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_INFODATA_STATE | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Slave State: DESC) *)

	(* Error *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function bcg450_error_implementation ($ifo, $sys, $bld, $loc, $inst) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} BCG 450 *)

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_VALID = FALSE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_OVERRANGE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_UNDERRANGE = TRUE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_VALID = FALSE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_OVERRANGE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_UNDERRANGE = TRUE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD3_VALID = FALSE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD3_OVERRANGE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD3_UNDERRANGE = TRUE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD3_PRESS_TORR_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD3_PRESS_TORR_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD4_VALID = FALSE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD4_OVERRANGE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD4_UNDERRANGE = TRUE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD4_PRESS_TORR_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD4_PRESS_TORR_ERROR := FALSE;
	END_IF
"@

return $text
}


#-------------------------------------------------------------------------------
# 390 Micro-Ion ATM

function _390_micro_ion_atm_declaration ($ifo, $sys, $bld, $loc, $inst, $link_prefix) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} 390 Micro-Ion ATM *)


	(* Differential Piezo TxPDO Default *)

	(* Reading Valid *)

	{attribute 'TcLinkTo' := '${link_prefix}^Differential Piezo TxPDO Default^Reading Valid'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_VALID | AT %I*: | BOOL; | (*~ ${loc} ${inst} Reading Valid (FALSE = Not Valid, TRUE = Valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Reading Valid: DESC)
	| | | (OPC_PROP[0106] :Valid: ONAM)
	| | | (OPC_PROP[0107] :Not Valid: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	(* Overrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Differential Piezo TxPDO Default^Overrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_OVERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Overrange Exceeded (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Overrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Underrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Differential Piezo TxPDO Default^Underrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_UNDERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Underrange Exceeded (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Underrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Sensor Value *)

	{attribute 'TcLinkTo' := '${link_prefix}^Differential Piezo TxPDO Default^Sensor Value'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR | AT %I*: | REAL; | (*~ ${loc} ${inst} Sensor Value (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Sensor Value: DESC)
	| | | (OPC_PROP[0102] :1000: HOPR)
	| | | (OPC_PROP[0103] :3.0E-11: LOPR)
	| | | (OPC_PROP[0307] :0.001: HIHI)
	| | | (OPC_PROP[0308] :1E-5: HIGH)
	| | | (OPC_PROP[0309] :3.16E-10: LOW)
	| | | (OPC_PROP[0310] :3.16E-11: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	(* Combo TxPDO Default *)

	(* Active Exception Status *)

	{attribute 'TcLinkTo' := '${link_prefix}^Combo TxPDO Default^Active Exception Status'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_ACTIVE_EXCEPTION_STATUS | AT %I*: | USINT; | (*~ ${loc} ${inst} Active Exception Status
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Active Exception Status: DESC) *)

	(* Reading Valid *)

	{attribute 'TcLinkTo' := '${link_prefix}^Combo TxPDO Default^Combination Gauge Reading Valid'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_VALID | AT %I*: | BOOL; | (*~ ${loc} ${inst} Combination Gauge Reading Valid (FALSE = Not Valid, TRUE = Valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Reading Valid: DESC)
	| | | (OPC_PROP[0106] :Valid: ONAM)
	| | | (OPC_PROP[0107] :Not Valid: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	(* Overrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Combo TxPDO Default^Combination Gauge Overrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_OVERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Combination Gauge Overrange Exceeded (FALSE = Not Overrange, TRUE = Overrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Overrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Overrange: ONAM)
	| | | (OPC_PROP[0107] :Not Overrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Underrange Exceeded *)

	{attribute 'TcLinkTo' := '${link_prefix}^Combo TxPDO Default^Combination Gauge Underrange Exceeded'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_UNDERRANGE | AT %I*: | BOOL; | (*~ ${loc} ${inst} Combination Gauge Underrange Exceeded (FALSE = Not Underrange, TRUE = Underrange)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Underrange Exceeded: DESC)
	| | | (OPC_PROP[0106] :Underrange: ONAM)
	| | | (OPC_PROP[0107] :Not Underrange: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	(* Active Sensor Number *)

	{attribute 'TcLinkTo' := '${link_prefix}^Combo TxPDO Default^Combination Gauge Active Sensor Number'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_ACTIVE_SENSOR | AT %I*: | UINT; | (*~ ${loc} ${inst} Combination Gauge Active Sensor Number
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Active Sensor Number: DESC) *)

	(* Active Value *)

	{attribute 'TcLinkTo' := '${link_prefix}^Combo TxPDO Default^Combination Gauge Active Value'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR | AT %I*: | REAL; | (*~ ${loc} ${inst} Combination Gauge Active Value (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Active Value: DESC)
	| | | (OPC_PROP[0102] :1000: HOPR)
	| | | (OPC_PROP[0103] :3.0E-11: LOPR)
	| | | (OPC_PROP[0307] :0.001: HIHI)
	| | | (OPC_PROP[0308] :1E-5: HIGH)
	| | | (OPC_PROP[0309] :3.16E-10: LOW)
	| | | (OPC_PROP[0310] :3.16E-11: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	(* Trip Point Output All *)

	{attribute 'TcLinkTo' := '${link_prefix}^Combo TxPDO Default^Trip Point Output All'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_TRIP | AT %I*: | DINT; | (*~ ${loc} ${inst} Trip Point Output All
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Trip Point Output All: DESC) *)


	(* WcState *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^WcState'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_WCSTATE_WCSTATE | AT %I*: | BOOL; | (*~ (FALSE = Data valid, TRUE = Data invalid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Working Counter State: DESC)
	| | | (OPC_PROP[0106] :Data invalid: ONAM)
	| | | (OPC_PROP[0107] :Data valid: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV) *)

	{attribute 'TcLinkTo' := '${link_prefix}^WcState^InputToggle'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_WCSTATE_TOGGLE | AT %I*: | BOOL; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Input Toggle: DESC) *)

	(* InfoData *)

	{attribute 'TcLinkTo' := '${link_prefix}^InfoData^State'}
	${ifo}_${sys}_${bld}_${loc}_${inst}_INFODATA_STATE | AT %I*: | UINT; | (*~
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Slave State: DESC) *)

	(* Error *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function _390_micro_ion_atm_error_implementation ($ifo, $sys, $bld, $loc, $inst) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} 390 Micro-Ion ATM *)

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_WCSTATE_WCSTATE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_INFODATA_STATE <> 8 THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_VALID = FALSE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_OVERRANGE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_UNDERRANGE = TRUE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD1_PRESS_TORR_ERROR := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_ERROR = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_VALID = FALSE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_OVERRANGE = TRUE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_UNDERRANGE = TRUE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR_ERROR := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_MOD2_PRESS_TORR_ERROR := FALSE;
	END_IF
"@

return $text
}


#-------------------------------------------------------------------------------
# Task Cycle Time

function task_cycle_time_declaration ($ifo, $sys, $bld) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* Task Cycle Time *)

	${ifo}_${sys}_${bld}_GET_TASK_CYCLE_TIME_FB: | | FB_CTRL_GET_TASK_CYCLETIME; | (* Get Task Cycle Time *)

	${ifo}_${sys}_${bld}_TASK_CYCLE_TIME: | | TIME; | (*~ PLC Task Cycle Time
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :PLC Task Cycle Time: DESC) *)

	${ifo}_${sys}_${bld}_TASK_CYCLE_TIME_VALID: | | BOOL; | (*~ PLC Task Cycle Time Valid (FALSE = Not Valid, TRUE = Valid)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :PLC Task Cycle Time Valid: DESC)
	| | | (OPC_PROP[0106] :Valid: ONAM)
	| | | (OPC_PROP[0107] :Not Valid: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	${ifo}_${sys}_${bld}_TASK_CYCLE_TIME_STATE_CODE: | | INT; | (*~ PLC Task Cycle Time FB State Code
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :PLC Task Cycle Time FB State Code: DESC) *)

	${ifo}_${sys}_${bld}_TASK_CYCLE_TIME_ERR_FLAG: | | BOOL; | (*~ PLC Task Cycle Time FB Error Flag (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :PLC Task Cycle Time FB Error Flag: DESC)
	| | | (OPC_PROP[0106] :Alarm: ONAM)
	| | | (OPC_PROP[0107] :Normal: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_TASK_CYCLE_TIME_ERR_CODE: | | INT; | (*~ PLC Task Cycle Time FB Error Code
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :PLC Task Cycle Time FB Error Code: DESC) *)
"@

return $text
}

function task_cycle_time_implementation ($ifo, $sys, $bld) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* Task Cycle Time *)

	${ifo}_${sys}_${bld}_GET_TASK_CYCLE_TIME_FB(
		eMode := eCTRL_MODE_ACTIVE,
		tTaskCycleTime => ${ifo}_${sys}_${bld}_TASK_CYCLE_TIME,
		bCycleTimeValid => ${ifo}_${sys}_${bld}_TASK_CYCLE_TIME_VALID,
		eState => ${ifo}_${sys}_${bld}_TASK_CYCLE_TIME_STATE_CODE,
		eErrorId => ${ifo}_${sys}_${bld}_TASK_CYCLE_TIME_ERR_CODE,
		bError => ${ifo}_${sys}_${bld}_TASK_CYCLE_TIME_ERR_FLAG
	);
"@

return $text
}


#-------------------------------------------------------------------------------
# Optical Lever Temperature

function optical_lever_temperature_declaration ($ifo, $sys, $bld, $loc) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} Optical Lever Temperature *)

	${ifo}_${sys}_${bld}_${loc}_OP_LEV_TEMP_DEGC: | | LREAL; | (*~ ${loc} Optical Lever Temperature (DegC) [${alias_prefix}-${bld}:${loc}_OP_LEV_TEMP]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU)
	| | | (OPC_PROP[0101] :${loc} Optical Lever Temperature: DESC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_OP_LEV_TEMP: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_OP_LEV_TEMP_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_OP_LEV_TEMP_DEGC_SMOO: | | LREAL; | (*~ ${loc} Optical Lever Temperature (DegC) Smoothing Factor
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} Smoothing Factor: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_OP_LEV_TEMP_DEGC_SMOOTH_FB: | | SmoothFB; | (* ${loc} Optical Lever Temperature (DegC) Smoothing Filter *)
"@

return $text
}

function optical_lever_temperature_implementation ($ifo, $sys, $bld, $loc, $io_in_degc) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} Optical Lever Temperature *)

	${ifo}_${sys}_${bld}_${loc}_OP_LEV_TEMP_DEGC := ${io_in_degc};
	${ifo}_${sys}_${bld}_${loc}_OP_LEV_TEMP_DEGC_ERROR := ${io_in_degc}_ERROR;
	${ifo}_${sys}_${bld}_${loc}_OP_LEV_TEMP_DEGC_SMOOTH_FB(SMOOTH_FACTOR := ${ifo}_${sys}_${bld}_${loc}_OP_LEV_TEMP_DEGC_SMOO, VALUE := ${ifo}_${sys}_${bld}_${loc}_OP_LEV_TEMP_DEGC);
"@

return $text
}


#-------------------------------------------------------------------------------
# Room Temperature

function room_temperature_declaration ($ifo, $sys, $bld, $loc, $subloc) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${subloc} Room Temperature *)

	${ifo}_${sys}_${bld}_${loc}_${subloc}_ROOM_TEMP_DEGC: | | LREAL; | (*~ ${loc} ${subloc} Room Temperature (DegC) [${alias_prefix}-${bld}:${loc}_${subloc}_ROOM_TEMP]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU)
	| | | (OPC_PROP[0101] :${loc} ${subloc} Room Temperature: DESC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_${subloc}_ROOM_TEMP: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_${subloc}_ROOM_TEMP_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_${subloc}_ROOM_TEMP_DEGC_SMOO: | | LREAL; | (*~ ${loc} ${subloc} Room Temperature (DegC) Smoothing Factor
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${subloc} Smoothing Factor: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${subloc}_ROOM_TEMP_DEGC_SMOOTH_FB: | | SmoothFB; | (* ${loc} ${subloc} Room Temperature (DegC) Smoothing Filter *)
"@

return $text
}

function room_temperature_implementation ($ifo, $sys, $bld, $loc, $subloc, $io_in_degc) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${subloc} Room Temperature *)

	${ifo}_${sys}_${bld}_${loc}_${subloc}_ROOM_TEMP_DEGC := ${io_in_degc};
	${ifo}_${sys}_${bld}_${loc}_${subloc}_ROOM_TEMP_DEGC_ERROR := ${io_in_degc}_ERROR;
	${ifo}_${sys}_${bld}_${loc}_${subloc}_ROOM_TEMP_DEGC_SMOOTH_FB(SMOOTH_FACTOR := ${ifo}_${sys}_${bld}_${loc}_${subloc}_ROOM_TEMP_DEGC_SMOO, VALUE := ${ifo}_${sys}_${bld}_${loc}_${subloc}_ROOM_TEMP_DEGC);
"@

return $text
}


#-------------------------------------------------------------------------------
# Temperature

function temperature_declaration ($ifo, $sys, $bld, $loc) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} Temperature *)

	${ifo}_${sys}_${bld}_${loc}_TEMP_DEGC: | | LREAL; | (*~ ${loc} Temperature (DegC) [${alias_prefix}-${bld}:${loc}_TEMP]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU)
	| | | (OPC_PROP[0101] :${loc} Temperature: DESC)
	| | | (OPC_PROP[0102] :2300: HOPR)
	| | | (OPC_PROP[0307] :160: HIHI)
	| | | (OPC_PROP[0308] :150: HIGH)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_TEMP: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_TEMP_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_TEMP_DEGC_SMOO: | | LREAL; | (*~ ${loc} Temperature (DegC) Smoothing Factor
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} Smoothing Factor: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_TEMP_DEGC_SMOOTH_FB: | | SmoothFB; | (* ${loc} Temperature (DegC) Smoothing Filter *)
"@

return $text
}

function temperature_implementation ($ifo, $sys, $bld, $loc, $io_in_degc) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} Temperature *)

	${ifo}_${sys}_${bld}_${loc}_TEMP_DEGC := ${io_in_degc};
	${ifo}_${sys}_${bld}_${loc}_TEMP_DEGC_ERROR := ${io_in_degc}_ERROR;
	${ifo}_${sys}_${bld}_${loc}_TEMP_DEGC_SMOOTH_FB(SMOOTH_FACTOR := ${ifo}_${sys}_${bld}_${loc}_TEMP_DEGC_SMOO, VALUE := ${ifo}_${sys}_${bld}_${loc}_TEMP_DEGC);
"@

return $text
}


#-------------------------------------------------------------------------------
# Ion Pump Controller MiniVac

function ion_pump_controller_minivac_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller MiniVac *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump High Voltage (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump High Voltage: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump High Voltage (kiloVolts) [${alias_prefix}-${bld}:${loc}_EI${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :kV: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump High Voltage: DESC)
	| | | (OPC_PROP[0102] :8: HOPR)
	| | | (OPC_PROP[0308] :8: HIGH)
	| | | (OPC_PROP[0309] :-0.001: LOW)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_EI${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Ion Current (Volts) [${alias_prefix}-${bld}:${loc}_II${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[0308] :10: HIGH)
	| | | (OPC_PROP[0309] :0: LOW)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_II${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Ion Current (Amps) [${alias_prefix}-${bld}:${loc}_${inst}AMP]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Amp: EGU)
	| | | (OPC_PROP[0102] :0.000010: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_${inst}AMP: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Pressure (Torr) [${alias_prefix}-${bld}:${loc}_VI${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Pressure: DESC)
	| | | (OPC_PROP[0102] :1E-4: HOPR)
	| | | (OPC_PROP[0103] :1E-11: LOPR)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_VI${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump Status (FALSE = Not OK, TRUE = OK)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Status: DESC)
	| | | (OPC_PROP[0106] :Pump OK: ONAM)
	| | | (OPC_PROP[0107] :Pump Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)
"@

return $text
}

function ion_pump_controller_minivac_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_hv_volts, $io_in_press_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller MiniVac *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS := ${io_in_hv_volts};
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR := ${io_in_hv_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS := IonPumpControllerMiniVacVoltsToKiloVoltsFun(${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS_ERROR := ${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS := ${io_in_press_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR := ${io_in_press_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS := IonPumpControllerMiniVacVoltsToAmpsFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR := IonPumpControllerMiniVacAmpsToTorrFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS);
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS := IonPumpControllerMiniVacTorrToStatusFun(${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR);
"@

return $text
}


#-------------------------------------------------------------------------------
# Ion Pump Controller IPCMini

function ion_pump_controller_ipcmini_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller IPCMini *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump High Voltage (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump High Voltage: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump High Voltage (kiloVolts) [${alias_prefix}-${bld}:${loc}_EI${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :kV: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump High Voltage: DESC)
	| | | (OPC_PROP[0102] :8: HOPR)
	| | | (OPC_PROP[0308] :8: HIGH)
	| | | (OPC_PROP[0309] :-0.001: LOW)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_EI${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Ion Current (Volts) [${alias_prefix}-${bld}:${loc}_II${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[0308] :10: HIGH)
	| | | (OPC_PROP[0309] :0: LOW)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_II${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Ion Current (Amps) [${alias_prefix}-${bld}:${loc}_${inst}AMP]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Amp: EGU)
	| | | (OPC_PROP[0102] :0.000010: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_${inst}AMP: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Pressure (Torr) [${alias_prefix}-${bld}:${loc}_VI${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Pressure: DESC)
	| | | (OPC_PROP[0102] :1E-4: HOPR)
	| | | (OPC_PROP[0103] :1E-11: LOPR)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_VI${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump Status (FALSE = Not OK, TRUE = OK)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Status: DESC)
	| | | (OPC_PROP[0106] :Pump OK: ONAM)
	| | | (OPC_PROP[0107] :Pump Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)
"@

return $text
}

function ion_pump_controller_ipcmini_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_hv_volts, $io_in_press_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller IPCMini *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS := ${io_in_hv_volts};
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR := ${io_in_hv_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS := IonPumpControllerIPCMiniVoltsToKiloVoltsFun(${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS_ERROR := ${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS := ${io_in_press_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR := ${io_in_press_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS := IonPumpControllerIPCMiniVoltsToAmpsFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR := IonPumpControllerIPCMiniAmpsToTorrFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS);
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS := IonPumpControllerIPCMiniTorrToStatusFun(${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR);
"@

return $text
}


#-------------------------------------------------------------------------------
# Annulus Ion Pump

function annulus_ion_pump_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Annulus Ion Pump *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Annulus Ion Pump Ion Current (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Volts: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Annulus Ion Pump Ion Current: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA: | | LREAL; | (*~ ${loc} ${inst} Annulus Ion Pump Ion Current (MilliAmps) [${alias_prefix}-${bld}:75IP${loc}_II${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Annulus Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[0102] :10: HOPR)
	| | | (OPC_PROP[0308] :10: HIGH)
	| | | (OPC_PROP[0309] :.01: LOW)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_75IP${loc}_II${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_LOGMA: | | LREAL; | (*~ ${loc} ${inst} Annulus Ion Pump Ion Current (MilliAmps Log Scale) [${alias_prefix}-${bld}:75IP${loc}_${inst}LOG]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Log mA: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Annulus Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :-5: LOPR)
	| | | (OPC_PROP[0308] :1: HIGH)
	| | | (OPC_PROP[0309] :-2: LOW)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_75IP${loc}_${inst}LOG: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_LOGMA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_AIP_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Annulus Ion Pump Pressure (Torr) [${alias_prefix}-${bld}:75IP${loc}_AIPTORR]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Annulus Ion Pump Pressure: DESC)
	| | | (OPC_PROP[0102] :1.0E-04: HOPR)
	| | | (OPC_PROP[0103] :1.0E-10: LOPR)
	| | | (OPC_PROP[0308] :3.47E-05: HIGH)
	| | | (OPC_PROP[0309] :5.28E-08: LOW)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_75IP${loc}_AIPTORR: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_AIP_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function annulus_ion_pump_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Annulus Ion Pump *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS := ${io_in_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS_ERROR := ${io_in_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA := AnnulusIonPumpVoltsToMilliAmpsFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_LOGMA := AnnulusIonPumpMilliAmpsToLogMilliAmpsFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA);
	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_LOGMA_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_AIP_PRESS_TORR := AnnulusIonPumpMilliAmpsToTorrFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA / 3.0);
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_AIP_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Cyropump Annulus Ion Pump

function cyropump_annulus_ion_pump_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Cyropump Annulus Ion Pump *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Cyropump Annulus Ion Pump Ion Current (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Volts: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Annulus Ion Pump Ion Current: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA: | | LREAL; | (*~ ${loc} ${inst} Cyropump Annulus Ion Pump Ion Current (MilliAmps) [${alias_prefix}-${bld}:75IP${loc}_II${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Annulus Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[0102] :10: HOPR)
	| | | (OPC_PROP[0308] :30.9009: HIGH)
	| | | (OPC_PROP[0309] :1.2588E-5: LOW)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_75IP${loc}_II${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_LOGMA: | | LREAL; | (*~ ${loc} ${inst} Cyropump Annulus Ion Pump Ion Current (MilliAmps Log Scale) [${alias_prefix}-${bld}:75IP${loc}_${inst}LOG]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Log mA: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Annulus Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :-5: LOPR)
	| | | (OPC_PROP[0308] :1.4900: HIGH)
	| | | (OPC_PROP[0309] :-4.9000: LOW)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_75IP${loc}_${inst}LOG: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_LOGMA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_AIP_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Cyropump Annulus Ion Pump Pressure (Torr) [${alias_prefix}-${bld}:75IP${loc}_AIPTORR]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Annulus Ion Pump Pressure: DESC)
	| | | (OPC_PROP[0102] :1.0E-04: HOPR)
	| | | (OPC_PROP[0103] :1.0E-10: LOPR)
	| | | (OPC_PROP[0308] :0.0001: HIGH)
	| | | (OPC_PROP[0309] :1.0E-10: LOW)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_75IP${loc}_AIPTORR: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_AIP_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function cyropump_annulus_ion_pump_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Cyropump Annulus Ion Pump *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS := ${io_in_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS_ERROR := ${io_in_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA := AnnulusIonPumpVoltsToMilliAmpsFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_LOGMA := AnnulusIonPumpMilliAmpsToLogMilliAmpsFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA);
	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_LOGMA_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_AIP_PRESS_TORR := AnnulusIonPumpMilliAmpsToTorrFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA / 3.0);
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_AIP_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Gate Valve Annulus Ion Pump

function gate_valve_annulus_ion_pump_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Annulus Ion Pump *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Annulus Ion Pump Ion Current (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Volts: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Annulus Ion Pump Ion Current: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA: | | LREAL; | (*~ ${loc} ${inst} Annulus Ion Pump Ion Current (MilliAmps) [${alias_prefix}-${bld}:${loc}_II${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Annulus Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[0102] :10: HOPR)
	| | | (OPC_PROP[0308] :10: HIGH)
	| | | (OPC_PROP[0309] :.01: LOW)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_II${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_LOGMA: | | LREAL; | (*~ ${loc} ${inst} Annulus Ion Pump Ion Current (MilliAmps Log Scale) [${alias_prefix}-${bld}:${loc}_${inst}LOG]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Log mA: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Annulus Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :-5: LOPR)
	| | | (OPC_PROP[0308] :1: HIGH)
	| | | (OPC_PROP[0309] :-2: LOW)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_${inst}LOG: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_LOGMA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_AIP_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Annulus Ion Pump Pressure (Torr) [${alias_prefix}-${bld}:${loc}_AIPTORR]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Annulus Ion Pump Pressure: DESC)
	| | | (OPC_PROP[0102] :1.0E-04: HOPR)
	| | | (OPC_PROP[0103] :1.0E-10: LOPR)
	| | | (OPC_PROP[0308] :3.47E-05: HIGH)
	| | | (OPC_PROP[0309] :5.28E-08: LOW)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_AIPTORR: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_AIP_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function gate_valve_annulus_ion_pump_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Annulus Ion Pump *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS := ${io_in_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS_ERROR := ${io_in_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA := AnnulusIonPumpVoltsToMilliAmpsFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_LOGMA := AnnulusIonPumpMilliAmpsToLogMilliAmpsFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA);
	${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_LOGMA_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_AIP_PRESS_TORR := AnnulusIonPumpMilliAmpsToTorrFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA / 3.0);
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_AIP_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_AIP_IC_MA_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Ion Pump Controller Gamma

function ion_pump_controller_gamma_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller Gamma *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump High Voltage (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump High Voltage: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump High Voltage (kiloVolts) [${alias_prefix}-${bld}:${loc}_EI${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :kV: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump High Voltage: DESC)
	| | | (OPC_PROP[0102] :8: HOPR)
	| | | (OPC_PROP[0308] :8: HIGH)
	| | | (OPC_PROP[0309] :-0.001: LOW)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_EI${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Ion Current (Volts) [${alias_prefix}-${bld}:${loc}_II${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[0308] :10: HIGH)
	| | | (OPC_PROP[0309] :0: LOW)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_II${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Ion Current (Amps) [${alias_prefix}-${bld}:${loc}_${inst}AMP]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Amp: EGU)
	| | | (OPC_PROP[0102] :0.000010: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_${inst}AMP: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Pressure (Torr) [${alias_prefix}-${bld}:${loc}_VI${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Pressure: DESC)
	| | | (OPC_PROP[0102] :1E-4: HOPR)
	| | | (OPC_PROP[0103] :1E-11: LOPR)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_VI${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump Status (FALSE = Not OK, TRUE = OK)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Status: DESC)
	| | | (OPC_PROP[0106] :Pump OK: ONAM)
	| | | (OPC_PROP[0107] :Pump Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)
"@

return $text
}

function ion_pump_controller_gamma_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_hv_volts, $io_in_press_volts, $pump_size) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller Gamma *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS := ${io_in_hv_volts};
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR := ${io_in_hv_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS := IonPumpControllerGammaVoltsToKiloVoltsFun(${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS_ERROR := ${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS := ${io_in_press_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR := ${io_in_press_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS := IonPumpControllerGammaVoltsToAmpsFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR := IonPumpControllerGammaAmpsToTorrFun(AMPS := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS, PUMP_SIZE := ${pump_size});
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS := IonPumpControllerGammaTorrToStatusFun(${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR);
"@

return $text
}


#-------------------------------------------------------------------------------
# Gauge Controller MKS 937

function gauge_controller_mks_937_declaration ($ifo, $sys, $bld, $loc, $inst) {

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Gauge Controller MKS 937 *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump High Voltage (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump High Voltage: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump High Voltage (kiloVolts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :kV: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump High Voltage: DESC)
	| | | (OPC_PROP[0102] :8: HOPR)
	| | | (OPC_PROP[0308] :8: HIGH)
	| | | (OPC_PROP[0309] :-0.001: LOW)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} MKS 937 Gauge Controller (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} MKS 937 Gauge Controller Volts: DESC)
	| | | (OPC_PROP[0308] :10: HIGH)
	| | | (OPC_PROP[0309] :0: LOW)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Ion Current (Amps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Amp: EGU)
	| | | (OPC_PROP[0102] :0.000010: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[8500] :4: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} MKS 937 Gauge Controller (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} MKS 937 Gauge Controller Torr: DESC)
	| | | (OPC_PROP[0102] :1E-4: HOPR)
	| | | (OPC_PROP[0103] :1E-11: LOPR)
	| | | (OPC_PROP[8500] :2: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump Status (FALSE = Not OK, TRUE = OK)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Status: DESC)
	| | | (OPC_PROP[0106] :Pump OK: ONAM)
	| | | (OPC_PROP[0107] :Pump Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)
"@

return $text
}

function gauge_controller_mks_937_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_press_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} MKS 937 Gauge Controller *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS := ${io_in_press_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR := ${io_in_press_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR := GaugeControllerMKS937VoltsToTorrFun(VOLTS := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Ion Pump Controller Gamma Single Channel

function ion_pump_controller_gamma_single_channel_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller Gamma Single Channel *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Ion Current (Volts) [${alias_prefix}-${bld}:${loc}_II${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[0308] :10: HIGH)
	| | | (OPC_PROP[0309] :0: LOW)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_II${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Ion Current (Amps) [${alias_prefix}-${bld}:${loc}_${inst}AMP]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Amp: EGU)
	| | | (OPC_PROP[0102] :0.000010: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_${inst}AMP: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Pressure (Torr) [${alias_prefix}-${bld}:${loc}_VI${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Pressure: DESC)
	| | | (OPC_PROP[0102] :1E-4: HOPR)
	| | | (OPC_PROP[0103] :1E-11: LOPR)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_VI${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump Status (FALSE = Not OK, TRUE = OK)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Status: DESC)
	| | | (OPC_PROP[0106] :Pump OK: ONAM)
	| | | (OPC_PROP[0107] :Pump Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)
"@

return $text
}

function ion_pump_controller_gamma_single_channel_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_press_volts, $pump_size) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller Gamma Single Channel *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS := ${io_in_press_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR := ${io_in_press_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS := IonPumpControllerGammaVoltsToAmpsFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR := IonPumpControllerGammaAmpsToTorrFun(AMPS := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS, PUMP_SIZE := ${pump_size});
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_AMPS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS := IonPumpControllerGammaTorrToStatusFun(${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR);
"@

return $text
}


#-------------------------------------------------------------------------------
# Ion Pump Controller Agilent UHV Single Controller

function ion_pump_controller_agilent_uhv_single_controller_declaration ($ifo, $sys, $bld, $loc, $inst) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller Agilent UHV Single Controller *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Ion Current (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[0308] :5: HIGH)
	| | | (OPC_PROP[0309] :0: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Pressure (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Pressure: DESC)
	| | | (OPC_PROP[0308] :0.0001: HIGH)
	| | | (OPC_PROP[0309] :1E-10: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump Status (FALSE = Not OK, TRUE = OK)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Status: DESC)
	| | | (OPC_PROP[0106] :Pump OK: ONAM)
	| | | (OPC_PROP[0107] :Pump Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)
"@

return $text
}

function ion_pump_controller_agilent_uhv_single_controller_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_press_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller DualVac Single Controller *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS := ${io_in_press_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR := ${io_in_press_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR := IonPumpControllerDualVacVoltsToTorrFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS := IonPumpControllerDualVacTorrToStatusFun(${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR);
"@

return $text
}


#-------------------------------------------------------------------------------
# Ion Pump Controller DualVac

function ion_pump_controller_dualvac_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller DualVac *)

	(* Section A *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}_A_FAULT: | | BOOL; | (*~ ${loc} ${inst} Ion Pump A Fault (FALSE = Fault, TRUE = OK) [${alias_prefix}-${bld}:${loc}_XA${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A Fault: DESC)
	| | | (OPC_PROP[0106] :OK: ONAM)
	| | | (OPC_PROP[0107] :Fault: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_XA${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}_A_FAULT_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump A High Voltage (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A High Voltage: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_KVOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump A High Voltage (kiloVolts) [${alias_prefix}-${bld}:${loc}_EI${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :kV: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A High Voltage: DESC)
	| | | (OPC_PROP[0102] :7: HOPR)
	| | | (OPC_PROP[0308] :7: HIGH)
	| | | (OPC_PROP[0309] :-0.001: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_EI${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_KVOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_A_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump A Ion Current (Volts) [${alias_prefix}-${bld}:${loc}_II${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A Ion Current: DESC)
	| | | (OPC_PROP[0308] :5: HIGH)
	| | | (OPC_PROP[0309] :0: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_II${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_A_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_A_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Ion Pump A Pressure (Torr) [${alias_prefix}-${bld}:${loc}_VI${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A Pressure: DESC)
	| | | (OPC_PROP[0308] :0.0001: HIGH)
	| | | (OPC_PROP[0309] :1E-10: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_VI${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_A_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_CS${inst}_A_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump A Status (FALSE = Not OK, TRUE = OK) [${alias_prefix}-${bld}:${loc}_CS${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A Status: DESC)
	| | | (OPC_PROP[0106] :Pump A OK: ONAM)
	| | | (OPC_PROP[0107] :Pump A Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_CS${inst}A: ALIAS) *)


	${ifo}_${sys}_${bld}_${loc}_HS${inst}A_A_START: | | BOOL; | (*~ ${loc} ${inst} Ion Pump A Start Signal (TRUE = Start) [${alias_prefix}-${bld}:${loc}_HS${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A Start Signal: DESC)
	| | | (OPC_PROP[0106] :Start: ONAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_HS${inst}B_A_STOP: | | BOOL; | (*~ ${loc} ${inst} Ion Pump A Stop Signal (FALSE = Stop) [${alias_prefix}-${bld}:${loc}_HS${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A Stop Signal: DESC)
	| | | (OPC_PROP[0107] :Stop: ZNAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}B: ALIAS) *)


	(* Section B *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}_B_FAULT: | | BOOL; | (*~ ${loc} ${inst} Ion Pump B Fault (FALSE = Fault, TRUE = OK) [${alias_prefix}-${bld}:${loc}_XA${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B Fault: DESC)
	| | | (OPC_PROP[0106] :OK: ONAM)
	| | | (OPC_PROP[0107] :Fault: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_XA${inst}B: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}_B_FAULT_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump B High Voltage (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B High Voltage: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_KVOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump B High Voltage (kiloVolts) [${alias_prefix}-${bld}:${loc}_EI${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :kV: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B High Voltage: DESC)
	| | | (OPC_PROP[0102] :7: HOPR)
	| | | (OPC_PROP[0308] :7: HIGH)
	| | | (OPC_PROP[0309] :-0.001: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_EI${inst}B: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_KVOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_B_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump B Ion Current (Volts) [${alias_prefix}-${bld}:${loc}_II${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B Ion Current: DESC)
	| | | (OPC_PROP[0308] :5: HIGH)
	| | | (OPC_PROP[0309] :0: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_II${inst}B: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_B_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_B_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Ion Pump B Pressure (Torr) [${alias_prefix}-${bld}:${loc}_VI${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B Pressure: DESC)
	| | | (OPC_PROP[0308] :0.0001: HIGH)
	| | | (OPC_PROP[0309] :1E-10: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_VI${inst}B: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_B_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_CS${inst}_B_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump B Status (FALSE = Not OK, TRUE = OK) [${alias_prefix}-${bld}:${loc}_CS${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B Status: DESC)
	| | | (OPC_PROP[0106] :Pump B OK: ONAM)
	| | | (OPC_PROP[0107] :Pump B Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_CS${inst}B: ALIAS) *)


	${ifo}_${sys}_${bld}_${loc}_HS${inst}C_B_START: | | BOOL; | (*~ ${loc} ${inst} Ion Pump B Start Signal (TRUE = Start) [${alias_prefix}-${bld}:${loc}_HS${inst}C]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B Start Signal: DESC)
	| | | (OPC_PROP[0106] :Start: ONAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}C: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_HS${inst}D_B_STOP: | | BOOL; | (*~ ${loc} ${inst} Ion Pump B Stop Signal (FALSE = Stop) [${alias_prefix}-${bld}:${loc}_HS${inst}D]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B Stop Signal: DESC)
	| | | (OPC_PROP[0107] :Stop: ZNAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}D: ALIAS) *)


	(* Combined Status *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump Status (FALSE = Not OK, TRUE = OK) [${alias_prefix}-${bld}:${loc}_STATUS]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Status: DESC)
	| | | (OPC_PROP[0106] :OK: ONAM)
	| | | (OPC_PROP[0107] :Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_STATUS: ALIAS) *)
"@

return $text
}

function ion_pump_controller_dualvac_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_a_fault, $io_in_a_hv_volts, $io_in_a_press_volts, $io_out_a_start, $io_out_a_stop, $io_in_b_fault, $io_in_b_hv_volts, $io_in_b_press_volts, $io_out_b_start, $io_out_b_stop) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller DualVac *)

	(* Section A *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}_A_FAULT := ${io_in_a_fault};
	${ifo}_${sys}_${bld}_${loc}_XA${inst}_A_FAULT_ERROR := ${io_in_a_fault}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_VOLTS := ${io_in_a_hv_volts};
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_VOLTS_ERROR := ${io_in_a_hv_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_KVOLTS := IonPumpControllerDualVacVoltsToKiloVoltsFun(${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_KVOLTS_ERROR := ${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_A_IC_VOLTS := ${io_in_a_press_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_A_IC_VOLTS_ERROR := ${io_in_a_press_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_A_PRESS_TORR := IonPumpControllerDualVacVoltsToTorrFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_A_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_A_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_A_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_CS${inst}_A_STATUS := IonPumpControllerDualVacTorrToStatusFun(${ifo}_${sys}_${bld}_${loc}_VI${inst}_A_PRESS_TORR);

	${io_out_a_start} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}A_A_START;
	${io_out_a_stop} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}B_A_STOP;

	(* Section B *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}_B_FAULT := ${io_in_b_fault};
	${ifo}_${sys}_${bld}_${loc}_XA${inst}_B_FAULT_ERROR := ${io_in_b_fault}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_VOLTS := ${io_in_b_hv_volts};
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_VOLTS_ERROR := ${io_in_b_hv_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_KVOLTS := IonPumpControllerDualVacVoltsToKiloVoltsFun(${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_KVOLTS_ERROR := ${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_B_IC_VOLTS := ${io_in_b_press_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_B_IC_VOLTS_ERROR := ${io_in_b_press_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_B_PRESS_TORR := IonPumpControllerDualVacVoltsToTorrFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_B_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_B_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_B_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_CS${inst}_B_STATUS := IonPumpControllerDualVacTorrToStatusFun(${ifo}_${sys}_${bld}_${loc}_VI${inst}_B_PRESS_TORR);

	${io_out_b_start} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}C_B_START;
	${io_out_b_stop} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}D_B_STOP;

	(* Combined Status *)

	IF ${ifo}_${sys}_${bld}_${loc}_CS${inst}_A_STATUS = TRUE AND ${ifo}_${sys}_${bld}_${loc}_CS${inst}_B_STATUS = TRUE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_STATUS := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_STATUS := FALSE;
	END_IF
"@

return $text
}


#-------------------------------------------------------------------------------
# Ion Pump Controller DualVac Single Controller

function ion_pump_controller_dualvac_single_controller_declaration ($ifo, $sys, $bld, $loc, $inst) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller DualVac Single Controller *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump High Voltage (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump High Voltage: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump High Voltage (kiloVolts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :kV: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump High Voltage: DESC)
	| | | (OPC_PROP[0102] :7: HOPR)
	| | | (OPC_PROP[0308] :7: HIGH)
	| | | (OPC_PROP[0309] :-0.001: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Ion Current (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Ion Current: DESC)
	| | | (OPC_PROP[0308] :5: HIGH)
	| | | (OPC_PROP[0309] :0: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Ion Pump Pressure (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Pressure: DESC)
	| | | (OPC_PROP[0308] :0.0001: HIGH)
	| | | (OPC_PROP[0309] :1E-10: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump Status (FALSE = Not OK, TRUE = OK)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Status: DESC)
	| | | (OPC_PROP[0106] :Pump OK: ONAM)
	| | | (OPC_PROP[0107] :Pump Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)
"@

return $text
}

function ion_pump_controller_dualvac_single_controller_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_hv_volts, $io_in_press_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller DualVac Single Controller *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS := ${io_in_hv_volts};
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR := ${io_in_hv_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS := IonPumpControllerDualVacVoltsToKiloVoltsFun(${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_KVOLTS_ERROR := ${ifo}_${sys}_${bld}_${loc}_EI${inst}_HV_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS := ${io_in_press_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR := ${io_in_press_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR := IonPumpControllerDualVacVoltsToTorrFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_CS${inst}_STATUS := IonPumpControllerDualVacTorrToStatusFun(${ifo}_${sys}_${bld}_${loc}_VI${inst}_PRESS_TORR);
"@

return $text
}


#-------------------------------------------------------------------------------
# Ion Pump Controller MultiVac

function ion_pump_controller_multivac_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller MultiVac *)

	(* Section A *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}_A_FAULT: | | BOOL; | (*~ ${loc} ${inst} Ion Pump A Fault (FALSE = Fault, TRUE = OK) [${alias_prefix}-${bld}:${loc}_XA${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A Fault: DESC)
	| | | (OPC_PROP[0106] :OK: ONAM)
	| | | (OPC_PROP[0107] :Fault: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_XA${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}_A_FAULT_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump A High Voltage (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A High Voltage: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_KVOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump A High Voltage (kiloVolts) [${alias_prefix}-${bld}:${loc}_EI${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :kV: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A High Voltage: DESC)
	| | | (OPC_PROP[0102] :7: HOPR)
	| | | (OPC_PROP[0308] :7: HIGH)
	| | | (OPC_PROP[0309] :-0.001: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_EI${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_KVOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_A_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump A Ion Current (Volts) [${alias_prefix}-${bld}:${loc}_II${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A Ion Current: DESC)
	| | | (OPC_PROP[0308] :5: HIGH)
	| | | (OPC_PROP[0309] :0: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_II${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_A_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_A_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Ion Pump A Pressure (Torr) [${alias_prefix}-${bld}:${loc}_VI${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A Pressure: DESC)
	| | | (OPC_PROP[0308] :0.0001: HIGH)
	| | | (OPC_PROP[0309] :1E-10: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_VI${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_A_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_CS${inst}_A_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump A Status (FALSE = Not OK, TRUE = OK) [${alias_prefix}-${bld}:${loc}_CS${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A Status: DESC)
	| | | (OPC_PROP[0106] :Pump A OK: ONAM)
	| | | (OPC_PROP[0107] :Pump A Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_CS${inst}A: ALIAS) *)


	${ifo}_${sys}_${bld}_${loc}_HS${inst}A_A_START: | | BOOL; | (*~ ${loc} ${inst} Ion Pump A Start Signal (TRUE = Start) [${alias_prefix}-${bld}:${loc}_HS${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A Start Signal: DESC)
	| | | (OPC_PROP[0106] :Start: ONAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_HS${inst}B_A_STOP: | | BOOL; | (*~ ${loc} ${inst} Ion Pump A Stop Signal (FALSE = Stop) [${alias_prefix}-${bld}:${loc}_HS${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump A Stop Signal: DESC)
	| | | (OPC_PROP[0107] :Stop: ZNAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}B: ALIAS) *)


	(* Section B *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}_B_FAULT: | | BOOL; | (*~ ${loc} ${inst} Ion Pump B Fault (FALSE = Fault, TRUE = OK) [${alias_prefix}-${bld}:${loc}_XA${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B Fault: DESC)
	| | | (OPC_PROP[0106] :OK: ONAM)
	| | | (OPC_PROP[0107] :Fault: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_XA${inst}B: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}_B_FAULT_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump B High Voltage (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B High Voltage: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_KVOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump B High Voltage (kiloVolts) [${alias_prefix}-${bld}:${loc}_EI${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :kV: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B High Voltage: DESC)
	| | | (OPC_PROP[0102] :7: HOPR)
	| | | (OPC_PROP[0308] :7: HIGH)
	| | | (OPC_PROP[0309] :-0.001: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_EI${inst}B: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_KVOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_II${inst}_B_IC_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Ion Pump B Ion Current (Volts) [${alias_prefix}-${bld}:${loc}_II${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B Ion Current: DESC)
	| | | (OPC_PROP[0308] :5: HIGH)
	| | | (OPC_PROP[0309] :0: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_II${inst}B: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_II${inst}_B_IC_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_VI${inst}_B_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Ion Pump B Pressure (Torr) [${alias_prefix}-${bld}:${loc}_VI${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B Pressure: DESC)
	| | | (OPC_PROP[0308] :0.0001: HIGH)
	| | | (OPC_PROP[0309] :1E-10: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_VI${inst}B: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_B_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_CS${inst}_B_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump B Status (FALSE = Not OK, TRUE = OK) [${alias_prefix}-${bld}:${loc}_CS${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B Status: DESC)
	| | | (OPC_PROP[0106] :Pump B OK: ONAM)
	| | | (OPC_PROP[0107] :Pump B Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_CS${inst}B: ALIAS) *)


	${ifo}_${sys}_${bld}_${loc}_HS${inst}C_B_START: | | BOOL; | (*~ ${loc} ${inst} Ion Pump B Start Signal (TRUE = Start) [${alias_prefix}-${bld}:${loc}_HS${inst}C]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B Start Signal: DESC)
	| | | (OPC_PROP[0106] :Start: ONAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}C: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_HS${inst}D_B_STOP: | | BOOL; | (*~ ${loc} ${inst} Ion Pump B Stop Signal (FALSE = Stop) [${alias_prefix}-${bld}:${loc}_HS${inst}D]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump B Stop Signal: DESC)
	| | | (OPC_PROP[0107] :Stop: ZNAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}D: ALIAS) *)


	(* Combined Status *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_STATUS: | | BOOL; | (*~ ${loc} ${inst} Ion Pump Status (FALSE = Not OK, TRUE = OK) [${alias_prefix}-${bld}:${loc}_STATUS]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Ion Pump Status: DESC)
	| | | (OPC_PROP[0106] :OK: ONAM)
	| | | (OPC_PROP[0107] :Not OK: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_STATUS: ALIAS) *)
"@

return $text
}

function ion_pump_controller_multivac_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_a_fault, $io_in_a_hv_volts, $io_in_a_press_volts, $io_out_a_start, $io_out_a_stop, $io_in_b_fault, $io_in_b_hv_volts, $io_in_b_press_volts, $io_out_b_start, $io_out_b_stop) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Ion Pump Controller MultiVac *)

	(* Section A *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}_A_FAULT := ${io_in_a_fault};
	${ifo}_${sys}_${bld}_${loc}_XA${inst}_A_FAULT_ERROR := ${io_in_a_fault}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_VOLTS := ${io_in_a_hv_volts};
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_VOLTS_ERROR := ${io_in_a_hv_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_KVOLTS := IonPumpControllerMultiVacVoltsToKiloVoltsFun(${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_KVOLTS_ERROR := ${ifo}_${sys}_${bld}_${loc}_EI${inst}_A_HV_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_A_IC_VOLTS := ${io_in_a_press_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_A_IC_VOLTS_ERROR := ${io_in_a_press_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_A_PRESS_TORR := IonPumpControllerMultiVacVoltsToTorrFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_A_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_A_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_A_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_CS${inst}_A_STATUS := IonPumpControllerMultiVacTorrToStatusFun(${ifo}_${sys}_${bld}_${loc}_VI${inst}_A_PRESS_TORR);

	${io_out_a_start} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}A_A_START;
	${io_out_a_stop} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}B_A_STOP;

	(* Section B *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}_B_FAULT := ${io_in_b_fault};
	${ifo}_${sys}_${bld}_${loc}_XA${inst}_B_FAULT_ERROR := ${io_in_b_fault}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_VOLTS := ${io_in_b_hv_volts};
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_VOLTS_ERROR := ${io_in_b_hv_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_KVOLTS := IonPumpControllerMultiVacVoltsToKiloVoltsFun(${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_KVOLTS_ERROR := ${ifo}_${sys}_${bld}_${loc}_EI${inst}_B_HV_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_II${inst}_B_IC_VOLTS := ${io_in_b_press_volts};
	${ifo}_${sys}_${bld}_${loc}_II${inst}_B_IC_VOLTS_ERROR := ${io_in_b_press_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_VI${inst}_B_PRESS_TORR := IonPumpControllerMultiVacVoltsToTorrFun(${ifo}_${sys}_${bld}_${loc}_II${inst}_B_IC_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_VI${inst}_B_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_II${inst}_B_IC_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_CS${inst}_B_STATUS := IonPumpControllerMultiVacTorrToStatusFun(${ifo}_${sys}_${bld}_${loc}_VI${inst}_B_PRESS_TORR);

	${io_out_b_start} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}C_B_START;
	${io_out_b_stop} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}D_B_STOP;

	(* Combined Status *)

	IF ${ifo}_${sys}_${bld}_${loc}_CS${inst}_A_STATUS = TRUE AND ${ifo}_${sys}_${bld}_${loc}_CS${inst}_B_STATUS = TRUE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_STATUS := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_STATUS := FALSE;
	END_IF
"@

return $text
}


#-------------------------------------------------------------------------------
# Electric Gate Valve

function electric_gate_valve_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Valve Position *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}M_VALVE_MTR: | | BOOL; | (*~ ${loc} ${inst} Valve Motor (FALSE = Stopped, TRUE = Running) [${alias_prefix}-${bld}:${loc}_XA${inst}M]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Valve Motor: DESC)
	| | | (OPC_PROP[0106] :Running: ONAM)
	| | | (OPC_PROP[0107] :Stopped: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_XA${inst}M: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}M_VALVE_MTR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_VALVE_CLOSE_LIM: | | BOOL; | (*~ ${loc} ${inst} Valve Closed Limit Switch (TRUE = GV Closed) [${alias_prefix}-${bld}:${loc}_ZSC${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Closed Limit Switch: DESC)
	| | | (OPC_PROP[0106] :CLOSED: ONAM)
	| | | (OPC_PROP[0107] :Not Closed: ZNAM)
	| | | (OPC_PROP[8700] :MINOR: OSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_ZSC${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_VALVE_CLOSE_LIM_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_ZSO${inst}_VALVE_OPEN_LIM: | | BOOL; | (*~ ${loc} ${inst} Valve Open Limit Switch (TRUE = GV Open) [${alias_prefix}-${bld}:${loc}_ZSO${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Open Limit Switch: DESC)
	| | | (OPC_PROP[0106] :OPEN: ONAM)
	| | | (OPC_PROP[0107] :Not Open: ZNAM)
	| | | (OPC_PROP[8700] :MINOR: OSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_ZSO${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_ZSO${inst}_VALVE_OPEN_LIM_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_ZSM${inst}_VALVE_POS: | | INT; | (*~ ${loc} ${inst} Valve Position [${alias_prefix}-${bld}:${loc}_ZSM${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Valve Position: DESC)
	| | | (OPC_PROP[0102] :2: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_ZSM${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_ZSM${inst}_VALVE_POS_STR: | | STRING; | (*~ ${loc} ${inst} Valve Position String
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Valve Position String: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_ZSM${inst}A_VALVE_ANIM: | | UINT; | (*~ ${loc} ${inst} Valve Animation [${alias_prefix}-${bld}:${loc}_ZSM${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Valve Animation: DESC)
	| | | (OPC_PROP[0102] :65536: HOPR)
	| | | (OPC_PROP[0309] :65279: LOW)
	| | | (OPC_PROP[0310] :256: LOLO)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_ZSM${inst}A: ALIAS) *)


	${ifo}_${sys}_${bld}_${loc}_HS${inst}A_VALVE_OPEN: | | BOOL; | (*~ ${loc} ${inst} Valve Open Solenoid Output (TRUE = Open GV) [${alias_prefix}-${bld}:${loc}_HS${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${loc} Valve Open Solenoid: DESC)
	| | | (OPC_PROP[0106] :Open GV: ONAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_HS${inst}B_VALVE_RUN_MTR: | | BOOL; | (*~ ${loc} ${inst} Valve Close Motor Solenoid Output (TRUE = Run Motor) [${alias_prefix}-${bld}:${loc}_HS${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Valve Close Motor Solenoid: DESC)
	| | | (OPC_PROP[0106] :Run Motor: ONAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}B: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_HS${inst}C_VALVE_CLOSE: | | BOOL; | (*~ ${loc} ${inst} Valve Close Direction Solenoid Output (TRUE = Close GV) [${alias_prefix}-${bld}:${loc}_HS${inst}C]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Valve Close Direction Solenoid: DESC)
	| | | (OPC_PROP[0106] :Close GV: ONAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}C: ALIAS) *)
"@

return $text
}

function electric_gate_valve_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_mtr, $io_in_close_lim, $io_in_open_lim, $io_out_open, $io_out_run_mtr, $io_out_close) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Valve Position *)

	${ifo}_${sys}_${bld}_${loc}_XA${inst}M_VALVE_MTR := ${io_in_mtr};
	${ifo}_${sys}_${bld}_${loc}_XA${inst}M_VALVE_MTR_ERROR := ${io_in_mtr}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_VALVE_CLOSE_LIM := ${io_in_close_lim};
	${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_VALVE_CLOSE_LIM_ERROR := ${io_in_close_lim}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_ZSO${inst}_VALVE_OPEN_LIM := ${io_in_open_lim};
	${ifo}_${sys}_${bld}_${loc}_ZSO${inst}_VALVE_OPEN_LIM_ERROR := ${io_in_open_lim}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_ZSM${inst}_VALVE_POS := GateValveLimitSwitchesToPositionNumberFun(CLOSED_LIMIT_SWITCH := ${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_VALVE_CLOSE_LIM, OPEN_LIMIT_SWITCH := ${ifo}_${sys}_${bld}_${loc}_ZSO${inst}_VALVE_OPEN_LIM);
	${ifo}_${sys}_${bld}_${loc}_ZSM${inst}_VALVE_POS_STR := GateValvePositionNumberToPositionStringFun(${ifo}_${sys}_${bld}_${loc}_ZSM${inst}_VALVE_POS);
	${ifo}_${sys}_${bld}_${loc}_ZSM${inst}A_VALVE_ANIM := GateValvePositionNumberToAnimationNumberFun(${ifo}_${sys}_${bld}_${loc}_ZSM${inst}_VALVE_POS);

	${io_out_open} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}A_VALVE_OPEN;
	${io_out_run_mtr} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}B_VALVE_RUN_MTR;
	${io_out_close} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}C_VALVE_CLOSE;
"@

return $text
}


#-------------------------------------------------------------------------------
# Pneumatic Gate Valve

function pneumatic_gate_valve_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Valve Position *)

	${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_VALVE_CLOSE_LIM: | | BOOL; | (*~ ${loc} ${inst} Closed Limit Switch (TRUE = GV Closed) [${alias_prefix}-${bld}:${loc}_ZSC${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Closed Limit Switch: DESC)
	| | | (OPC_PROP[0106] :CLOSED: ONAM)
	| | | (OPC_PROP[0107] :Not Closed: ZNAM)
	| | | (OPC_PROP[8700] :MINOR: OSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_ZSC${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_VALVE_CLOSE_LIM_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_ZSO${inst}_VALVE_OPEN_LIM: | | BOOL; | (*~ ${loc} ${inst} Open Limit Switch (TRUE = GV Open) [${alias_prefix}-${bld}:${loc}_ZSO${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Open Limit Switch: DESC)
	| | | (OPC_PROP[0106] :OPEN: ONAM)
	| | | (OPC_PROP[0107] :Not Open: ZNAM)
	| | | (OPC_PROP[8700] :MINOR: OSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_ZSO${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_ZSO${inst}_VALVE_OPEN_LIM_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_ZSM${inst}_VALVE_POS: | | INT; | (*~ ${loc} ${inst} Valve Position [${alias_prefix}-${bld}:${loc}_ZSM${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Valve Position: DESC)
	| | | (OPC_PROP[0102] :2: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_ZSM${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_ZSM${inst}_VALVE_POS_STR: | | STRING; | (*~ ${loc} ${inst} Valve Position String
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} Valve Position String: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_ZSM${inst}A_VALVE_ANIM: | | UINT; | (*~ ${loc} ${inst} Valve Animation [${alias_prefix}-${bld}:${loc}_ZSM${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Valve Animation: DESC)
	| | | (OPC_PROP[0102] :65536: HOPR)
	| | | (OPC_PROP[0309] :65279: LOW)
	| | | (OPC_PROP[0310] :256: LOLO)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_ZSM${inst}A: ALIAS) *)


	${ifo}_${sys}_${bld}_${loc}_HS${inst}A_VALVE_OPEN: | | BOOL; | (*~ ${loc} ${inst} Valve Open Solenoid Output (TRUE = Open GV) [${alias_prefix}-${bld}:${loc}_HS${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Valve Open Solenoid: DESC)
	| | | (OPC_PROP[0106] :Open GV: ONAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_HS${inst}B_VALVE_CLOSE: | | BOOL; | (*~ ${loc} ${inst} Valve Close Solenoid Output (TRUE = Close GV) [${alias_prefix}-${bld}:${loc}_HS${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Valve Close Solenoid: DESC)
	| | | (OPC_PROP[0106] :Close GV: ONAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}B: ALIAS) *)
"@

return $text
}

function pneumatic_gate_valve_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_close_lim, $io_in_open_lim, $io_out_open, $io_out_close) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Valve Position *)

	${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_VALVE_CLOSE_LIM := ${io_in_close_lim};
	${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_VALVE_CLOSE_LIM_ERROR := ${io_in_close_lim}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_ZSO${inst}_VALVE_OPEN_LIM := ${io_in_open_lim};
	${ifo}_${sys}_${bld}_${loc}_ZSO${inst}_VALVE_OPEN_LIM_ERROR := ${io_in_open_lim}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_ZSM${inst}_VALVE_POS := GateValveLimitSwitchesToPositionNumberFun(CLOSED_LIMIT_SWITCH := ${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_VALVE_CLOSE_LIM, OPEN_LIMIT_SWITCH := ${ifo}_${sys}_${bld}_${loc}_ZSO${inst}_VALVE_OPEN_LIM);
	${ifo}_${sys}_${bld}_${loc}_ZSM${inst}_VALVE_POS_STR := GateValvePositionNumberToPositionStringFun(${ifo}_${sys}_${bld}_${loc}_ZSM${inst}_VALVE_POS);
	${ifo}_${sys}_${bld}_${loc}_ZSM${inst}A_VALVE_ANIM := GateValvePositionNumberToAnimationNumberFun(${ifo}_${sys}_${bld}_${loc}_ZSM${inst}_VALVE_POS);

	${io_out_open} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}A_VALVE_OPEN;
	${io_out_close} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}B_VALVE_CLOSE;
"@

return $text
}


#-------------------------------------------------------------------------------
# Pirani

function pirani_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Pirani Pressure *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Pirani Pressure (Volts) [${alias_prefix}-${bld}:${loc}_PT${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Volts: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Pirani Pressure: DESC)
	| | | (OPC_PROP[0307] :6: HIHI)
	| | | (OPC_PROP[0308] :2.51: HIGH)
	| | | (OPC_PROP[0310] :.1: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8730] :MINOR: LLSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_PT${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_VOLTS_SMOO: | | LREAL; | (*~ ${loc} ${inst} Pirani Pressure (Volts) Smoothing Factor
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Smoothing Factor: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_VOLTS_SMOOTH_FB: | | SmoothFB; | (* ${loc} ${inst} Pirani Pressure (Volts) Smoothing Filter *)


	${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Pirani Pressure (Torr) [${alias_prefix}-${bld}:${loc}_${inst}ATORR]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Pirani Pressure: DESC)
	| | | (OPC_PROP[0102] :1000: HOPR)
	| | | (OPC_PROP[0103] :1.0E-4: LOPR)
	| | | (OPC_PROP[0307] :1.89E3: HIHI)
	| | | (OPC_PROP[0308] :1.08: HIGH)
	| | | (OPC_PROP[0310] :0: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8730] :MINOR: LLSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_${inst}ATORR: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function pirani_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Pirani Pressure *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_VOLTS := ${io_in_volts};
	${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_VOLTS_ERROR := ${io_in_volts}_ERROR;
	${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_VOLTS_SMOOTH_FB(SMOOTH_FACTOR := ${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_VOLTS_SMOO, VALUE := ${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_VOLTS);

	${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_TORR := PiraniGaugeVoltsToTorrFun(${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_VOLTS_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Televac CC-10

function cc10_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Televac CC-10 Pressure *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_VOLTS: | | LREAL; | (*~ ${loc} ${inst} CC-10 Pressure (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Volts: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} CC-10 Pressure: DESC)
	| | | (OPC_PROP[8500] :2: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_VOLTS_SMOO: | | LREAL; | (*~ ${loc} ${inst} CC-10 Pressure (Volts) Smoothing Factor
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Smoothing Factor: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_VOLTS_SMOOTH_FB: | | SmoothFB; | (* ${loc} ${inst} CC-10 Pressure (Volts) Smoothing Filter *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_SELECT: | | UINT; | (*~ ${loc} ${inst} CC-10 Formula Select (0 = 0.5 V / decade, not 0 = 1 V / decade)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Formula Select: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_N: | | INT; | (*~ ${loc} ${inst} CC-10 Full Scale Pressure Value
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Full Scale Pressure Value: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} CC-10 Pressure (Torr)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} CC-10 Pressure: DESC)
	| | | (OPC_PROP[8500] :2: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function cc10_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Televac CC-10 Pressure *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_VOLTS := ${io_in_volts};
	${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_VOLTS_ERROR := ${io_in_volts}_ERROR;
	${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_VOLTS_SMOOTH_FB(SMOOTH_FACTOR := ${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_VOLTS_SMOO, VALUE := ${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_VOLTS);

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_TORR := CC10VoltsToTorrFun(${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_VOLTS, ${ifo}_${sys}_${bld}_${loc}_PT${inst}_SELECT, ${ifo}_${sys}_${bld}_${loc}_PT${inst}_N);
	${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_PT${inst}_PRESS_VOLTS_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Cold Cathode Power

function cold_cathode_power_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Cold Cathode Power *)

	${ifo}_${sys}_${bld}_${loc}_HS${inst}_PWR_CTRL_FB: | | ColdCathodeGaugePowerControlFB; | (* ${loc} ${inst} Cold Cathode Power Control *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_PIRANI_INTLK: | | BOOL; | (*~ ${loc} ${inst} Cold Cathode Pirani Interlock (FALSE = Out Of Safe Range)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Cold Cathode Pirani Intlk: DESC)
	| | | (OPC_PROP[0106] :Good: ONAM)
	| | | (OPC_PROP[0107] :Bad: ZNAM)
	| | | (OPC_PROP[8700] :NO_ALARM: OSV)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_PWR_REQ: | | BOOL; | (*~ ${loc} ${inst} Cold Cathode Power Request (FALSE = Off, TRUE = On) [${alias_prefix}-${bld}:${loc}_HSCCONOFF]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Cold Cathode Power Request: DESC)
	| | | (OPC_PROP[0106] :On: ONAM)
	| | | (OPC_PROP[0107] :Off: ZNAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HSCCONOFF: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_HS${inst}_PWR_CTRL: | | BOOL; | (*~ ${loc} ${inst} Cold Cathode Power Control (FALSE = Off, TRUE = On) [${alias_prefix}-${bld}:${loc}_HS${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Cold Cathode Power Control: DESC)
	| | | (OPC_PROP[0106] :On: ONAM)
	| | | (OPC_PROP[0107] :Off: ZNAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_HS${inst}: ALIAS) *)
"@

return $text
}

function cold_cathode_power_implementation ($ifo, $sys, $bld, $loc, $inst, $io_out_pwr_ctrl) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Cold Cathode Power *)

	${ifo}_${sys}_${bld}_${loc}_HS${inst}_PWR_CTRL_FB(
		PIRANI_PRESS_TORR := ${ifo}_${sys}_${bld}_${loc}_PT${inst}A_PRESS_TORR,
		COLD_CATHODE_PWR_REQ := ${ifo}_${sys}_${bld}_${loc}_${inst}_PWR_REQ,
		COLD_CATHODE_PIRANI_INTLK => ${ifo}_${sys}_${bld}_${loc}_${inst}_PIRANI_INTLK,
		COLD_CATHODE_PWR_CTRL => ${ifo}_${sys}_${bld}_${loc}_HS${inst}_PWR_CTRL
	);

	${io_out_pwr_ctrl} := ${ifo}_${sys}_${bld}_${loc}_HS${inst}_PWR_CTRL;
"@

return $text
}


#-------------------------------------------------------------------------------
# Cold Cathode

function cold_cathode_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Cold Cathode Pressure *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_VOLTS: | | LREAL; | (*~ ${loc} ${inst} Cold Cathode Pressure (Volts) [${alias_prefix}-${bld}:${loc}_PT${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Volts: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Cold Cathode Pressure: DESC)
	| | | (OPC_PROP[0307] :8: HIHI)
	| | | (OPC_PROP[0308] :6: HIGH)
	| | | (OPC_PROP[0309] :1.5: LOW)
	| | | (OPC_PROP[0310] :.5: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_PT${inst}B: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_TORR: | | LREAL; | (*~ ${loc} ${inst} Cold Cathode Pressure (Torr) [${alias_prefix}-${bld}:${loc}_${inst}BTORR]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Cold Cathode Pressure: DESC)
	| | | (OPC_PROP[0102] :1.0E-3: HOPR)
	| | | (OPC_PROP[0103] :3.0E-11: LOPR)
	| | | (OPC_PROP[0307] :0.001: HIHI)
	| | | (OPC_PROP[0308] :1E-5: HIGH)
	| | | (OPC_PROP[0309] :3.16E-10: LOW)
	| | | (OPC_PROP[0310] :3.16E-11: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_${inst}BTORR: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_TORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_LOGTORR: | | LREAL; | (*~ ${loc} ${inst} Cold Cathode Pressure (Torr Log Scale) [${alias_prefix}-${bld}:${loc}_${inst}BLOG]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Log Torr: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Cold Cathode Pressure: DESC)
	| | | (OPC_PROP[0102] :0: HOPR)
	| | | (OPC_PROP[0103] :-10: LOPR)
	| | | (OPC_PROP[0307] :-3: HIHI)
	| | | (OPC_PROP[0308] :-5: HIGH)
	| | | (OPC_PROP[0309] :-9.5: LOW)
	| | | (OPC_PROP[0310] :-10.5: LOLO)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_${inst}BLOG: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_LOGTORR_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function cold_cathode_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Cold Cathode Pressure *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_VOLTS := ${io_in_volts};
	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_VOLTS_ERROR := ${io_in_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_TORR := ColdCathodeGaugeVoltsToTorrFun(${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_LOGTORR := ColdCathodeGaugeTorrToLogTorrFun(${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_TORR);
	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_LOGTORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_TORR_ERROR;
"@

return $text
}

function bpg402_as_cold_cathode_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Inficon BPG402 Pressure *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_VOLTS := ${io_in_volts};
	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_VOLTS_ERROR := ${io_in_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_TORR := InficonBPG402VoltsToTorrFun(${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_TORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_VOLTS_ERROR;

	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_LOGTORR := InficonBPG402TorrToLogTorrFun(${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_TORR);
	${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_LOGTORR_ERROR := ${ifo}_${sys}_${bld}_${loc}_PT${inst}B_PRESS_TORR_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Instrument Air

function instrument_air_declaration ($ifo, $sys, $bld, $inst, $low, $high) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* Instrument Air ${inst} *)

	${ifo}_${sys}_${bld}_INSTAIR_PT${inst}_PRESS_VOLTS: | | LREAL; | (*~ Instrument Air ${inst} Pressure (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :V: EGU)
	| | | (OPC_PROP[0101] :Instrument Air ${inst} Pressure: DESC) *)

	${ifo}_${sys}_${bld}_INSTAIR_PT${inst}_PRESS_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_INSTAIR_PT${inst}_PRESS_PSIG: | | LREAL; | (*~ Instrument Air ${inst} Pressure (PSIG) [${alias_prefix}-${bld}:INSTAIR_PT${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :PSIG: EGU)
	| | | (OPC_PROP[0101] :Instrument Air ${inst} Pressure: DESC)
	| | | (OPC_PROP[0102] :200: HOPR)
	| | | (OPC_PROP[0308] :${high}: HIGH)
	| | | (OPC_PROP[0309] :${low}: LOW)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_INSTAIR_PT${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_INSTAIR_PT${inst}_PRESS_PSIG_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function instrument_air_implementation ($ifo, $sys, $bld, $inst, $io_in_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* Instrument Air ${inst} *)

	${ifo}_${sys}_${bld}_INSTAIR_PT${inst}_PRESS_VOLTS := ${io_in_volts};
	${ifo}_${sys}_${bld}_INSTAIR_PT${inst}_PRESS_VOLTS_ERROR := ${io_in_volts}_ERROR;

	${ifo}_${sys}_${bld}_INSTAIR_PT${inst}_PRESS_PSIG := InstrumentAirVoltsToPoundsPerSquareInchGaugeFun(${ifo}_${sys}_${bld}_INSTAIR_PT${inst}_PRESS_VOLTS);
	${ifo}_${sys}_${bld}_INSTAIR_PT${inst}_PRESS_PSIG_ERROR := ${ifo}_${sys}_${bld}_INSTAIR_PT${inst}_PRESS_VOLTS_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Fan

function fan_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* Fan${loc} ${inst} Acceleration *)

	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_1_ACC_MA: | | LREAL; | (*~ Fan${loc} ${inst} 1 Acceleration (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU)
	| | | (OPC_PROP[0101] :Fan${loc} ${inst} 1 Acceleration: DESC) *)

	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_1_ACC_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_1_ACC_INCHSEC: | | LREAL; | (*~ Fan${loc} ${inst} 1 Acceleration (Inch/Sec) [${alias_prefix}-${bld}:ACCF${loc}_${inst}_1]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :inch/sec: EGU)
	| | | (OPC_PROP[0101] :Fan${loc} ${inst} 1 Acceleration: DESC)
	| | | (OPC_PROP[0102] :0.5: HOPR)
	| | | (OPC_PROP[0308] :0.5: HIGH)
	| | | (OPC_PROP[8500] :3: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_ACCF${loc}_${inst}_1: ALIAS) *)

	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_1_ACC_INCHSEC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_2_ACC_MA: | | LREAL; | (*~ Fan${loc} ${inst} 2 Acceleration (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU)
	| | | (OPC_PROP[0101] :Fan${loc} ${inst} 2 Acceleration: DESC) *)

	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_2_ACC_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_2_ACC_INCHSEC: | | LREAL; | (*~ Fan${loc} ${inst} 2 Acceleration (Inch/Sec) [${alias_prefix}-${bld}:ACCF${loc}_${inst}_2]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :inch/sec: EGU)
	| | | (OPC_PROP[0101] :Fan${loc} ${inst} 2 Acceleration: DESC)
	| | | (OPC_PROP[0102] :0.5: HOPR)
	| | | (OPC_PROP[0308] :0.5: HIGH)
	| | | (OPC_PROP[8500] :3: PREC)
	| | | (OPC_PROP[8728] :MAJOR: HSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_ACCF${loc}_${inst}_2: ALIAS) *)

	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_2_ACC_INCHSEC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function fan_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_1_ma, $io_in_2_ma) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* Fan${loc} ${inst} Acceleration *)

	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_1_ACC_MA := ${io_in_1_ma};
	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_1_ACC_MA_ERROR := ${io_in_1_ma}_ERROR;

	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_1_ACC_INCHSEC := FanVibrationMilliAmpsToInchesPerSecondFun(${ifo}_${sys}_${bld}_FAN${loc}_${inst}_1_ACC_MA);
	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_1_ACC_INCHSEC_ERROR := ${ifo}_${sys}_${bld}_FAN${loc}_${inst}_1_ACC_MA_ERROR;

	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_2_ACC_MA := ${io_in_2_ma};
	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_2_ACC_MA_ERROR := ${io_in_2_ma}_ERROR;

	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_2_ACC_INCHSEC := FanVibrationMilliAmpsToInchesPerSecondFun(${ifo}_${sys}_${bld}_FAN${loc}_${inst}_2_ACC_MA);
	${ifo}_${sys}_${bld}_FAN${loc}_${inst}_2_ACC_INCHSEC_ERROR := ${ifo}_${sys}_${bld}_FAN${loc}_${inst}_2_ACC_MA_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Dewar Level

function dewar_level_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Dewar Level *)

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_MA: | | LREAL; | (*~ ${loc} ${inst} Dewar Level (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Dewar Level: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_PCT: | | LREAL; | (*~ ${loc} ${inst} Dewar Level (% Full) [${alias_prefix}-${bld}:${loc}_LT${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :% Full: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Dewar Level: DESC)
	| | | (OPC_PROP[0102] :100: HOPR)
	| | | (OPC_PROP[0307] :100: HIHI)
	| | | (OPC_PROP[0308] :99: HIGH)
	| | | (OPC_PROP[0309] :10: LOW)
	| | | (OPC_PROP[0310] :5: LOLO)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_LT${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_PCT_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_${inst}_DEWAR_LEVEL_CAP_GAL: | | LREAL; | (*~ ${loc} ${inst} Dewar Level Capacity (Gallons) [${alias_prefix}-${bld}:${loc}_DEWARGAL]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0100] :Gal: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Dewar Level Capacity: DESC)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_DEWARGAL: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_GAL: | | LREAL; | (*~ ${loc} ${inst} Dewar Level (Gallons) [${alias_prefix}-${bld}:${loc}_LT${inst}GAL]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Gal: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Dewar Level: DESC)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_LT${inst}GAL: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_GAL_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function dewar_level_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_ma) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Dewar Level *)

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_MA := ${io_in_ma};
	${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_MA_ERROR := ${io_in_ma}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_PCT := DewarMilliAmpsToPercentFullFun(${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_MA);
	${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_PCT_ERROR := ${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_MA_ERROR;

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_GAL := DewarPercentFullToGallonsFun(PERCENT_FULL := ${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_PCT, CAPACITY := ${ifo}_${sys}_${bld}_${loc}_${inst}_DEWAR_LEVEL_CAP_GAL);
	${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_GAL_ERROR := ${ifo}_${sys}_${bld}_${loc}_LT${inst}_DEWAR_LEVEL_PCT_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Dewar Lower Temperature

function dewar_lower_temperature_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Dewar Lower Temperature *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_LOWER_DEGC: | | LREAL; | (*~ ${loc} ${inst} Dewar Lower Temperature (DegC) [${alias_prefix}-${bld}:DW_TE${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Dewar Lower Temperature: DESC)
	| | | (OPC_PROP[0102] :2300: HOPR)
	| | | (OPC_PROP[0307] :200: HIHI)
	| | | (OPC_PROP[0308] :195: HIGH)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_DW_TE${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_LOWER_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_LOWER_DEGC_SMOO: | | LREAL; | (*~ ${loc} ${inst} Dewar Lower Temperature (DegC) Smoothing Factor
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Smoothing Factor: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_LOWER_DEGC_SMOOTH_FB: | | SmoothFB; | (* ${loc} ${inst} Dewar Lower Temperature (DegC) Smoothing Filter *)
"@

return $text
}

function dewar_lower_temperature_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_degc) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Dewar Lower Temperature *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_LOWER_DEGC := ${io_in_degc};
	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_LOWER_DEGC_ERROR := ${io_in_degc}_ERROR;
	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_LOWER_DEGC_SMOOTH_FB(SMOOTH_FACTOR := ${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_LOWER_DEGC_SMOO, VALUE := ${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_LOWER_DEGC);
"@

return $text
}


#-------------------------------------------------------------------------------
# Dewar Upper Temperature

function dewar_upper_temperature_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Dewar Upper Temperature *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_UPPER_DEGC: | | LREAL; | (*~ ${loc} ${inst} Dewar Upper Temperature (DegC) [${alias_prefix}-${bld}:DW_TE${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Dewar Upper Temperature: DESC)
	| | | (OPC_PROP[0102] :2300: HOPR)
	| | | (OPC_PROP[0307] :200: HIHI)
	| | | (OPC_PROP[0308] :195: HIGH)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_DW_TE${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_UPPER_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_UPPER_DEGC_SMOO: | | LREAL; | (*~ ${loc} ${inst} Dewar Upper Temperature (DegC) Smoothing Factor
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Smoothing Factor: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_UPPER_DEGC_SMOOTH_FB: | | SmoothFB; | (* ${loc} ${inst} Dewar Upper Temperature (DegC) Smoothing Filter *)
"@

return $text
}

function dewar_upper_temperature_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_degc) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Dewar Upper Temperature *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_UPPER_DEGC := ${io_in_degc};
	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_UPPER_DEGC_ERROR := ${io_in_degc}_ERROR;
	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_UPPER_DEGC_SMOOTH_FB(SMOOTH_FACTOR := ${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_UPPER_DEGC_SMOO, VALUE := ${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_UPPER_DEGC);
"@

return $text
}


#-------------------------------------------------------------------------------
# Dewar Future Use Temperature

function dewar_future_use_temperature_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Dewar Temperature (Future Use) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_FUTURE_USE_DEGC: | | LREAL; | (*~ ${loc} ${inst} Dewar Temperature (Future Use) (DegC) [${alias_prefix}-${bld}:DW_TE${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Dewar Temperature (Future Use): DESC)
	| | | (OPC_PROP[0102] :2300: HOPR)
	| | | (OPC_PROP[0307] :200: HIHI)
	| | | (OPC_PROP[0308] :195: HIGH)
	| | | (OPC_PROP[8500] :4: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_DW_TE${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_FUTURE_USE_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_FUTURE_USE_DEGC_SMOO: | | LREAL; | (*~ ${loc} ${inst} Dewar Temperature (Future Use) (DegC) Smoothing Factor
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Smoothing Factor: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_FUTURE_USE_DEGC_SMOOTH_FB: | | SmoothFB; | (* ${loc} ${inst} Dewar Temperature (Future Use) (DegC) Smoothing Filter *)
"@

return $text
}

function dewar_future_use_temperature_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_degc) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Dewar Temperature (Future Use) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_FUTURE_USE_DEGC := ${io_in_degc};
	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_FUTURE_USE_DEGC_ERROR := ${io_in_degc}_ERROR;
	${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_FUTURE_USE_DEGC_SMOOTH_FB(SMOOTH_FACTOR := ${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_FUTURE_USE_DEGC_SMOO, VALUE := ${ifo}_${sys}_${bld}_${loc}_TE${inst}_DEWAR_TEMP_FUTURE_USE_DEGC);
"@

return $text
}


#-------------------------------------------------------------------------------
# Pump Level

function pump_level_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Pump Level *)

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_MA: | | LREAL; | (*~ ${loc} ${inst} Pump Level (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Pump Level: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_MA_SMOO: | | LREAL; | (*~ ${loc} ${inst} Pump Level (MilliAmps) Smoothing Factor
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Smoothing Factor: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_MA_SMOOTH_FB: | | SmoothFB; | (* ${loc} ${inst} Pump Level (MilliAmps) Smoothing Filter *)


	${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_PCT: | | LREAL; | (*~ ${loc} ${inst} Pump Level (% Full) [${alias_prefix}-${bld}:${loc}_LT${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :% Full: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Pump Level: DESC)
	| | | (OPC_PROP[0102] :100: HOPR)
	| | | (OPC_PROP[0307] :99: HIHI)
	| | | (OPC_PROP[0308] :98: HIGH)
	| | | (OPC_PROP[0309] :84: LOW)
	| | | (OPC_PROP[0310] :83: LOLO)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MINOR: LSV)
	| | | (OPC_PROP[8730] :MAJOR: LLSV)
	| | | (OPC_PROP[?] :0: ADEL)
	| | | (OPC_PROP[?] :1: HYST)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_LT${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_PCT_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function pump_level_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_ma) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Pump Level *)

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_MA := ${io_in_ma};
	${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_MA_ERROR := ${io_in_ma}_ERROR;
	${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_MA_SMOOTH_FB(SMOOTH_FACTOR := ${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_MA_SMOO, VALUE := ${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_MA);

	${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_PCT := CryopumpMilliAmpsToPercentFullFun(${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_MA);
	${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_PCT_ERROR := ${ifo}_${sys}_${bld}_${loc}_LT${inst}_PUMP_LEVEL_MA_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Liquid Level Control Valve

function liquid_level_control_valve_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Liquid Level Control Valve *)

	${ifo}_${sys}_${bld}_${loc}_LIC${inst}_LLCV_PI_FB: | | PIControllerFB; | (* ${loc} ${inst} Liquid Level Control Valve PI Controller *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PIRANI_INTLK: | | BOOL; | (*~ ${loc} ${inst} Liquid Level Control Valve Pirani Interlock (FALSE = Out Of Safe Range)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV Pirani Intlk: DESC)
	| | | (OPC_PROP[0106] :Good: ONAM)
	| | | (OPC_PROP[0107] :Bad: ZNAM)
	| | | (OPC_PROP[8700] :NO_ALARM: OSV)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_DISCHARGE_INTLK: | | BOOL; | (*~ ${loc} ${inst} Liquid Level Control Valve Discharge Interlock (FALSE = Out Of Safe Range)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV Discharge Intlk: DESC)
	| | | (OPC_PROP[0106] :Good: ONAM)
	| | | (OPC_PROP[0107] :Bad: ZNAM)
	| | | (OPC_PROP[8700] :NO_ALARM: OSV)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_INTLK: | | BOOL; | (*~ ${loc} ${inst} Liquid Level Control Valve Interlock (FALSE = Out Of Safe Range) [${alias_prefix}-${bld}:${loc}_XV${inst}INT]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV Intlk: DESC)
	| | | (OPC_PROP[0106] :Good: ONAM)
	| | | (OPC_PROP[0107] :Bad: ZNAM)
	| | | (OPC_PROP[8700] :NO_ALARM: OSV)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_XV${inst}INT: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_MAN_POS_PCT: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve Manual Position (% Open) [${alias_prefix}-${bld}:${loc}_MANSET]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0100] :% Open: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV Manual Position: DESC)
	| | | (OPC_PROP[0102] :100: HOPR)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_MANSET: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_SETPT_PCT: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve PID Setpoint (% Full) [${alias_prefix}-${bld}:${loc}_LN2SET]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0100] :% Full: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV PID Setpoint: DESC)
	| | | (OPC_PROP[0102] :100: HOPR)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_LN2SET: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_KP: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve PID Proportional Gain
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV PID Proportional Gain: DESC)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_KI: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve PID Integral Gain
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV PID Integral Gain: DESC)
	| | | (OPC_PROP[8500] :6: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_OFFSET: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve PID Offset
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV PID Offset: DESC)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_ITERM_MAX_LIM: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve PID Integral Term Max Limit
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV PID ITerm Max Lim: DESC)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_ITERM_MIN_LIM: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve PID Integral Term Min Limit
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV PID ITerm Min Lim: DESC)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_OUT_MAX_LIM: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve PID Out Max Limit
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV PID Out Max Lim: DESC)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_OUT_MIN_LIM: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve PID Out Min Limit
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV PID Out Min Lim: DESC)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_OUT_DEAD_BAND: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve PID Out Dead Band
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV PID Out Dead Band: DESC)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_CTRL: | | BOOL; | (*~ ${loc} ${inst} Liquid Level Control Valve Manual/PID Control Select (FALSE = Manual, TRUE = PID) [${alias_prefix}-${bld}:${loc}_LVLSELECT]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV Position Control Select: DESC)
	| | | (OPC_PROP[0106] :PID: ONAM)
	| | | (OPC_PROP[0107] :Manual: ZNAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_LVLSELECT: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_CYCLE_TIME: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve PID Cycle Time (MilliSeconds)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0100] :ms: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV PID Cycle Time: DESC)
	| | | (OPC_PROP[8500] :5: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_LIC${inst}_LLCV_POS_CTRL_PCT: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve Position Control (% Open) [${alias_prefix}-${bld}:${loc}_LIC${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :% Open: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV Position Control: DESC)
	| | | (OPC_PROP[0102] :100: HOPR)
	| | | (OPC_PROP[8500] :2: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_LIC${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_LIC${inst}_LLCV_POS_CTRL_MA: | | LREAL; | (*~ ${loc} ${inst} Liquid Level Control Valve Position Control (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV Position Control: DESC)
	| | | (OPC_PROP[8500] :2: PREC) *)

"@

return $text
}

function liquid_level_control_valve_implementation ($ifo, $sys, $bld, $loc, $inst, $pirani_press_torr, $discharge_press_psig, $cp_pump_level_pct, $io_out_ma) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Liquid Level Control Valve *)

	${ifo}_${sys}_${bld}_${loc}_LIC${inst}_LLCV_PI_FB(
		INPUT := ${cp_pump_level_pct},
		MANUAL_SETTING := ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_MAN_POS_PCT,
		SETPOINT := ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_SETPT_PCT,
		KP := ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_KP,
		KI := ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_KI,
		OFFSET := ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_OFFSET,
		ITERM_MAX_LIM := ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_ITERM_MAX_LIM,
		ITERM_MIN_LIM := ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_ITERM_MIN_LIM,
		OUTPUT_MAX_LIM := ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_OUT_MAX_LIM,
		OUTPUT_MIN_LIM := ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_OUT_MIN_LIM,
		DEAD_BAND := ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_OUT_DEAD_BAND,
		MODE := ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_CTRL,
		TASK_CYCLE_TIME := T#10MS,
		CONTROL_CYCLE_TIME := LREAL_TO_TIME(${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PID_CYCLE_TIME),
		OUTPUT => ${ifo}_${sys}_${bld}_${loc}_LIC${inst}_LLCV_POS_CTRL_PCT
	);

	IF ${pirani_press_torr} > 0 AND ${pirani_press_torr} <= 0.005 THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PIRANI_INTLK := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PIRANI_INTLK := FALSE;
	END_IF

	IF ${discharge_press_psig} < 10 THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_DISCHARGE_INTLK := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_DISCHARGE_INTLK := FALSE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_PIRANI_INTLK = FALSE OR ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_DISCHARGE_INTLK = FALSE THEN
		${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_INTLK := FALSE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_INTLK := TRUE;
	END_IF

	IF ${ifo}_${sys}_${bld}_${loc}_${inst}_LLCV_INTLK = FALSE THEN
		${ifo}_${sys}_${bld}_${loc}_LIC${inst}_LLCV_POS_CTRL_PCT := 0;
	END_IF

	${ifo}_${sys}_${bld}_${loc}_LIC${inst}_LLCV_POS_CTRL_MA := CryopumpLiquidLevelControlValvePercentOpenToMilliAmpsFun(${ifo}_${sys}_${bld}_${loc}_LIC${inst}_LLCV_POS_CTRL_PCT);
	${io_out_ma} := ${ifo}_${sys}_${bld}_${loc}_LIC${inst}_LLCV_POS_CTRL_MA;
"@

return $text
}


#-------------------------------------------------------------------------------
# Discharge Pressure

function discharge_pressure_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Discharge Pressure *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_DISCHARGE_PRESS_MA: | | LREAL; | (*~ ${loc} ${inst} Discharge Pressure (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Discharge Pressure: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_DISCHARGE_PRESS_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_PT${inst}_DISCHARGE_PRESS_PSIG: | | LREAL; | (*~ ${loc} ${inst} Discharge Pressure (PSIG) [${alias_prefix}-${bld}:${loc}_PT${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :PSIG: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Discharge Pressure: DESC)
	| | | (OPC_PROP[0102] :25: HOPR)
	| | | (OPC_PROP[0307] :5.0: HIHI)
	| | | (OPC_PROP[0308] :2.0: HIGH)
	| | | (OPC_PROP[0309] :-0.5: LOW)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[?] :0.1: HYST)
	| | | (OPC_PROP[?] :0.1: MDEL)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_PT${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_DISCHARGE_PRESS_PSIG_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function discharge_pressure_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_ma) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Discharge Pressure *)

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_DISCHARGE_PRESS_MA := ${io_in_ma};
	${ifo}_${sys}_${bld}_${loc}_PT${inst}_DISCHARGE_PRESS_MA_ERROR := ${io_in_ma}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_PT${inst}_DISCHARGE_PRESS_PSIG := DischargeLineMilliAmpsToPoundsPerSquareInchGaugeFun(${ifo}_${sys}_${bld}_${loc}_PT${inst}_DISCHARGE_PRESS_MA);
	${ifo}_${sys}_${bld}_${loc}_PT${inst}_DISCHARGE_PRESS_PSIG_ERROR := ${ifo}_${sys}_${bld}_${loc}_PT${inst}_DISCHARGE_PRESS_MA_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Discharge Flow

function discharge_flow_declaration ($ifo, $sys, $bld, $loc, $inst) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Discharge Flow *)

	${ifo}_${sys}_${bld}_${loc}_FL${inst}_DISCHARGE_FLOW_MA: | | LREAL; | (*~ ${loc} ${inst} Discharge Flow (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Discharge Flow: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_FL${inst}_DISCHARGE_FLOW_MA_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_FL${inst}_DISCHARGE_FLOW_SLPM: | | LREAL; | (*~ ${loc} ${inst} Discharge Flow (SLPM)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :SLPM: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Discharge Flow: DESC)
	| | | (OPC_PROP[0102] :500: HOPR) *)

	${ifo}_${sys}_${bld}_${loc}_FL${inst}_DISCHARGE_FLOW_SLPM_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function discharge_flow_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_ma) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Discharge Flow *)

	${ifo}_${sys}_${bld}_${loc}_FL${inst}_DISCHARGE_FLOW_MA := ${io_in_ma};
	${ifo}_${sys}_${bld}_${loc}_FL${inst}_DISCHARGE_FLOW_MA_ERROR := ${io_in_ma}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_FL${inst}_DISCHARGE_FLOW_SLPM := DischargeLineMilliAmpsToStandardLitrePerMinuteFun(${ifo}_${sys}_${bld}_${loc}_FL${inst}_DISCHARGE_FLOW_MA);
	${ifo}_${sys}_${bld}_${loc}_FL${inst}_DISCHARGE_FLOW_SLPM_ERROR := ${ifo}_${sys}_${bld}_${loc}_FL${inst}_DISCHARGE_FLOW_MA_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Discharge Temperature

function discharge_temperature_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Discharge Temperature *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_DISCHARGE_TEMP_DEGC: | | LREAL; | (*~ ${loc} ${inst} Discharge Temperature (DegC) [${alias_prefix}-${bld}:TE${loc}_TE${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Discharge Temperature: DESC)
	| | | (OPC_PROP[0102] :2300: HOPR)
	| | | (OPC_PROP[0307] :160: HIHI)
	| | | (OPC_PROP[0308] :150: HIGH)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_TE${loc}_TE${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_DISCHARGE_TEMP_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_DISCHARGE_TEMP_DEGC_SMOO: | | LREAL; | (*~ ${loc} ${inst} Discharge Temperature (DegC) Smoothing Factor
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Smoothing Factor: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_DISCHARGE_TEMP_DEGC_SMOOTH_FB: | | SmoothFB; | (* ${loc} ${inst} Discharge Temperature (DegC) Smoothing Filter *)
"@

return $text
}

function discharge_temperature_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_degc) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Discharge Temperature *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_DISCHARGE_TEMP_DEGC := ${io_in_degc};
	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_DISCHARGE_TEMP_DEGC_ERROR := ${io_in_degc}_ERROR;
	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_DISCHARGE_TEMP_DEGC_SMOOTH_FB(SMOOTH_FACTOR := ${ifo}_${sys}_${bld}_${loc}_TE${inst}A_DISCHARGE_TEMP_DEGC_SMOO, VALUE := ${ifo}_${sys}_${bld}_${loc}_TE${inst}A_DISCHARGE_TEMP_DEGC);
"@

return $text
}


#-------------------------------------------------------------------------------
# Discharge Temperature Spare

function discharge_temperature_spare_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Discharge Temperature (Spare) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}B_DISCHARGE_TEMP_DEGC: | | LREAL; | (*~ ${loc} ${inst} Discharge Temperature (Spare) (DegC) [${alias_prefix}-${bld}:TE${loc}_TE${inst}B]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Discharge Temperature (Spare): DESC)
	| | | (OPC_PROP[0102] :2300: HOPR)
	| | | (OPC_PROP[0307] :160: HIHI)
	| | | (OPC_PROP[0308] :150: HIGH)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_TE${loc}_TE${inst}B: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}B_DISCHARGE_TEMP_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}B_DISCHARGE_TEMP_DEGC_SMOO: | | LREAL; | (*~ ${loc} ${inst} Discharge Temperature (Spare) (DegC) Smoothing Factor
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Smoothing Factor: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}B_DISCHARGE_TEMP_DEGC_SMOOTH_FB: | | SmoothFB; | (* ${loc} ${inst} Discharge Temperature (Spare) (DegC) Smoothing Filter *)
"@

return $text
}

function discharge_temperature_spare_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_degc) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Discharge Temperature (Spare) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}B_DISCHARGE_TEMP_DEGC := ${io_in_degc};
	${ifo}_${sys}_${bld}_${loc}_TE${inst}B_DISCHARGE_TEMP_DEGC_ERROR := ${io_in_degc}_ERROR;
	${ifo}_${sys}_${bld}_${loc}_TE${inst}B_DISCHARGE_TEMP_DEGC_SMOOTH_FB(SMOOTH_FACTOR := ${ifo}_${sys}_${bld}_${loc}_TE${inst}B_DISCHARGE_TEMP_DEGC_SMOO, VALUE := ${ifo}_${sys}_${bld}_${loc}_TE${inst}B_DISCHARGE_TEMP_DEGC);
"@

return $text
}


#-------------------------------------------------------------------------------
# Discharge Temperature Comparison

function discharge_temperature_comparison_declaration ($ifo, $sys, $bld, $loc) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} Discharge Temperature Comparison *)

	${ifo}_${sys}_${bld}_${loc}_DISCHARGE_TEMP_COMP: | | BOOL; | (*~ ${loc} Discharge Temperature Comparison (FALSE = In range, TRUE = Out of range) [${alias_prefix}-${bld}:TE${loc}_CMPRSN]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} Discharge Temperature Comparison: DESC)
	| | | (OPC_PROP[0106] :Out of range: ONAM)
	| | | (OPC_PROP[0107] :In range: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_TE${loc}_CMPRSN: ALIAS) *)
"@

return $text
}

function discharge_temperature_comparison_implementation ($ifo, $sys, $bld, $loc, $inst) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} Discharge Temperature Comparison *)

	IF ABS(${ifo}_${sys}_${bld}_${loc}_TE${inst}A_DISCHARGE_TEMP_DEGC - ${ifo}_${sys}_${bld}_${loc}_TE${inst}B_DISCHARGE_TEMP_DEGC) > 1.0 THEN
		${ifo}_${sys}_${bld}_${loc}_DISCHARGE_TEMP_COMP := TRUE;
	ELSE
		${ifo}_${sys}_${bld}_${loc}_DISCHARGE_TEMP_COMP := FALSE;
	END_IF
"@

return $text
}


#-------------------------------------------------------------------------------
# Liquid Level Control Valve Enable Position

function liquid_level_control_valve_enable_position_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Liquid Level Control Valve Enable Position *)

	${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_LLCV_ENABLE_POS: | | BOOL; | (*~ ${loc} ${inst} Liquid Level Control Valve Enable Position (FALSE = Enabled, TRUE = Disabled) [${alias_prefix}-${bld}:${loc}_ZSC${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV Enable Position: DESC)
	| | | (OPC_PROP[0106] :Disabled: ONAM)
	| | | (OPC_PROP[0107] :Enabled: ZNAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_ZSC${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_LLCV_ENABLE_POS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function liquid_level_control_valve_enable_position_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_pos) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Liquid Level Control Valve Enable Position *)

	${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_LLCV_ENABLE_POS := ${io_in_pos};
	${ifo}_${sys}_${bld}_${loc}_ZSC${inst}_LLCV_ENABLE_POS_ERROR := ${io_in_pos}_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Liquid Level Control Valve Enable

function liquid_level_control_valve_enable_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Liquid Level Control Valve Enable *)

	${ifo}_${sys}_${bld}_${loc}_XV${inst}_LLCV_ENABLE_CTRL: | | BOOL; | (*~ ${loc} ${inst} Liquid Level Control Valve Enable Control (FALSE = Disable, TRUE = Enable) [${alias_prefix}-${bld}:${loc}_XV${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} LLCV Enable Control: DESC)
	| | | (OPC_PROP[0106] :Enable: ONAM)
	| | | (OPC_PROP[0107] :Disable: ZNAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_XV${inst}: ALIAS) *)
"@

return $text
}

function liquid_level_control_valve_enable_implementation ($ifo, $sys, $bld, $loc, $inst, $io_out_ctrl) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Liquid Level Control Valve Enable *)

	${io_out_ctrl} := ${ifo}_${sys}_${bld}_${loc}_XV${inst}_LLCV_ENABLE_CTRL;
"@

return $text
}


#-------------------------------------------------------------------------------
# Regen Temperature

function regen_temperature_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Regen Temperature *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_REGEN_TEMP_DEGC: | | LREAL; | (*~ ${loc} ${inst} Regen Temperature (DegC) [${alias_prefix}-${bld}:${loc}_TE${inst}A]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen Temperature: DESC)
	| | | (OPC_PROP[0102] :250: HOPR)
	| | | (OPC_PROP[0307] :200: HIHI)
	| | | (OPC_PROP[0308] :195: HIGH)
	| | | (OPC_PROP[8500] :3: PREC)
	| | | (OPC_PROP[8727] :MAJOR: HHSV)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_TE${inst}A: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_REGEN_TEMP_DEGC_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_REGEN_TEMP_DEGC_SMOO: | | LREAL; | (*~ ${loc} ${inst} Regen Temperature (DegC) Smoothing Factor
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Smoothing Factor: DESC)
	| | | (OPC_PROP[0102] :1: HOPR)
	| | | (OPC_PROP[0103] :0: LOPR)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_REGEN_TEMP_DEGC_SMOOTH_FB: | | SmoothFB; | (* ${loc} ${inst} Regen Temperature (DegC) Smoothing Filter *)
"@

return $text
}

function regen_temperature_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_degc) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Regen Temperature *)

	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_REGEN_TEMP_DEGC := ${io_in_degc};
	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_REGEN_TEMP_DEGC_ERROR := ${io_in_degc}_ERROR;
	${ifo}_${sys}_${bld}_${loc}_TE${inst}A_REGEN_TEMP_DEGC_SMOOTH_FB(SMOOTH_FACTOR := ${ifo}_${sys}_${bld}_${loc}_TE${inst}A_REGEN_TEMP_DEGC_SMOO, VALUE := ${ifo}_${sys}_${bld}_${loc}_TE${inst}A_REGEN_TEMP_DEGC);
"@

return $text
}


#-------------------------------------------------------------------------------
# Regen Temperature Alarm

function regen_temperature_alarm_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Regen Temperature Alarm *)

	${ifo}_${sys}_${bld}_${loc}_TSH${inst}_REGEN_TEMP_ALRM: | | BOOL; | (*~ ${loc} ${inst} Regen Temperature Alarm (FALSE = Over Temperature, TRUE = Normal) [${alias_prefix}-${bld}:${loc}_TSH${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen Temperature Alarm: DESC)
	| | | (OPC_PROP[0106] :Normal: ONAM)
	| | | (OPC_PROP[0107] :Overtemp: ZNAM)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_TSH${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_TSH${inst}_REGEN_TEMP_ALRM_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function regen_temperature_alarm_implementation ($ifo, $sys, $bld, $loc, $inst, $io_in_temp_alrm) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Regen Temperature Alarm *)

	${ifo}_${sys}_${bld}_${loc}_TSH${inst}_REGEN_TEMP_ALRM := ${io_in_temp_alrm};
	${ifo}_${sys}_${bld}_${loc}_TSH${inst}_REGEN_TEMP_ALRM_ERROR := ${io_in_temp_alrm}_ERROR;
"@

return $text
}


#-------------------------------------------------------------------------------
# Regen

function regen_declaration ($ifo, $sys, $bld, $loc, $inst) {
if ($ifo -eq 'H0') {
	$alias_prefix = 'HVE'
}
elseif ($ifo -eq 'L0') {
	$alias_prefix = 'LVE'
}

$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Regen *)

	${ifo}_${sys}_${bld}_${loc}_TY${inst}_REGEN_HTR_CTRL_FB: | | RegenHeaterTemperatureControlFB; | (* ${loc} ${inst} Regen Heater Control FB *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_SETPT_REQ_DEGC: | | LREAL; | (*~ ${loc} ${inst} Regen PID Setpoint Request (degC) [${alias_prefix}-${bld}:${loc}_REGENSET]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0100] :Deg. C: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen PID Setpoint Request: DESC)
	| | | (OPC_PROP[0102] :400: HOPR)
	| | | (OPC_PROP[0103] :0.1: LOPR)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_REGENSET: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_SETPT_DEGC: | | LREAL; | (*~ ${loc} ${inst} Regen PID Setpoint (degC) [${alias_prefix}-${bld}:${loc}_REGENVAL]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen PID Setpoint: DESC)
	| | | (OPC_PROP[0102] :400: HOPR)
	| | | (OPC_PROP[0103] :0.1: LOPR)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_REGENVAL: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_RUN_REQ: | | BOOL; | (*~ ${loc} ${inst} Regen Run Request (FALSE = Stop, TRUE = Run)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen Run Request: DESC)
	| | | (OPC_PROP[0106] :Start: ONAM)
	| | | (OPC_PROP[0107] :Stop: ZNAM) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_RUN_CTRL: | | BOOL; | (*~ ${loc} ${inst} Regen Run Control (FALSE = Stopped, TRUE = Running) [${alias_prefix}-${bld}:${loc}_REGENSTAT]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen Run Control: DESC)
	| | | (OPC_PROP[0106] :Start: ONAM)
	| | | (OPC_PROP[0107] :Stop: ZNAM)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_REGENSTAT: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_TEMP_INTLK: | | BOOL; | (*~ ${loc} ${inst} Regen Temperature Interlock (FALSE = Out Of Safe Range)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen Temperature Intlk: DESC)
	| | | (OPC_PROP[0106] :Good: ONAM)
	| | | (OPC_PROP[0107] :Bad: ZNAM)
	| | | (OPC_PROP[8700] :NO_ALARM: OSV)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_TEMP_ALRM_INTLK: | | BOOL; | (*~ ${loc} ${inst} Regen Temperature Alarm Interlock (FALSE = Out Of Safe Range)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen Temperature Alarm Intlk: DESC)
	| | | (OPC_PROP[0106] :Good: ONAM)
	| | | (OPC_PROP[0107] :Bad: ZNAM)
	| | | (OPC_PROP[8700] :NO_ALARM: OSV)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_GV_A_CLOSE_INTLK: | | BOOL; | (*~ ${loc} ${inst} Regen Gate Valve A Closed Interlock (FALSE = Out Of Safe Range)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen Gate Valve A Closed Intlk: DESC)
	| | | (OPC_PROP[0106] :Good: ONAM)
	| | | (OPC_PROP[0107] :Bad: ZNAM)
	| | | (OPC_PROP[8700] :NO_ALARM: OSV)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_GV_B_CLOSE_INTLK: | | BOOL; | (*~ ${loc} ${inst} Regen Gate Valve B Closed Interlock (FALSE = Out Of Safe Range)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen Gate Valve B Closed Intlk: DESC)
	| | | (OPC_PROP[0106] :Good: ONAM)
	| | | (OPC_PROP[0107] :Bad: ZNAM)
	| | | (OPC_PROP[8700] :NO_ALARM: OSV)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_LLCV_POS_INTLK: | | BOOL; | (*~ ${loc} ${inst} Regen Liquid Level Control Valve Position Interlock (FALSE = Out Of Safe Range)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen LLCV Position Intlk: DESC)
	| | | (OPC_PROP[0106] :Good: ONAM)
	| | | (OPC_PROP[0107] :Bad: ZNAM)
	| | | (OPC_PROP[8700] :NO_ALARM: OSV)
	| | | (OPC_PROP[8701] :MAJOR: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_INTLK: | | BOOL; | (*~ ${loc} ${inst} Regen Interlock (FALSE = Out Of Safe Range) [${alias_prefix}-${bld}:${loc}_REGENINT]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen Intlk: DESC)
	| | | (OPC_PROP[0106] :Good: ONAM)
	| | | (OPC_PROP[0107] :Bad: ZNAM)
	| | | (OPC_PROP[8700] :NO_ALARM: OSV)
	| | | (OPC_PROP[8701] :MAJOR: ZSV)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_REGENINT: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_KP: | | LREAL; | (*~ ${loc} ${inst} Regen PID Proportional Gain
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen PID Proportional Gain: DESC)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_TN: | | LREAL; | (*~ ${loc} ${inst} Regen PID Integral Gain (Seconds)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0100] :s: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen PID Integral Gain: DESC)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_TV: | | LREAL; | (*~ ${loc} ${inst} Regen PID Derivative Gain (Seconds)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0100] :s: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen PID Derivative Gain: DESC)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_TD: | | LREAL; | (*~ ${loc} ${inst} Regen PID Derivative Damping (Seconds)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0100] :s: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen PID Derivative Damping: DESC)
	| | | (OPC_PROP[8500] :3: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_CYCLE_TIME: | | LREAL; | (*~ ${loc} ${inst} Regen PID Cycle Time (Seconds)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :3: read/write)
	| | | (OPC_PROP[0100] :s: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen PID Cycle Time: DESC)
	| | | (OPC_PROP[8500] :5: PREC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_ERR_FLAG: | | BOOL; | (*~ ${loc} ${inst} Regen PID Error Flag (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen PID Error Flag: DESC)
	| | | (OPC_PROP[0106] :Alarm: ONAM)
	| | | (OPC_PROP[0107] :Normal: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_ERR_CODE: | | UINT; | (*~ ${loc} ${inst} Regen PID Error Code
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen PID Error Code: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_ERR_STR: | | STRING; | (*~ ${loc} ${inst} Regen PID Error String
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen PID Error String: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_TY${inst}_REGEN_HTR_CTRL_DEGC: | | LREAL; | (*~ ${loc} ${inst} Regen Heater Control (DegC) [${alias_prefix}-${bld}:${loc}_TY${inst}]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Deg. C: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen Heater Control: DESC)
	| | | (OPC_PROP[8500] :1: PREC)
	| | | (OPC_PROP[8800] :${alias_prefix}-${bld}_${loc}_TY${inst}: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_TY${inst}_REGEN_HTR_CTRL_MA: | | LREAL; | (*~ ${loc} ${inst} Regen Heater Control (MilliAmps)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :mA: EGU)
	| | | (OPC_PROP[0101] :${loc} ${inst} Regen Heater Control: DESC)
	| | | (OPC_PROP[8500] :1: PREC) *)
"@

return $text
}

function regen_implementation ($ifo, $sys, $bld, $loc, $inst, $cp_regen_temp_degc, $gv_a_valve_close_lim, $gv_b_valve_close_lim, $cp_llcv_pos_pct, $cp_regen_temp_alrm, $io_out_ma) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} ${inst} Regen *)

	${ifo}_${sys}_${bld}_${loc}_TY${inst}_REGEN_HTR_CTRL_FB(
		CP_REGEN_TEMP_DEGC := ${cp_regen_temp_degc},
		GV_A_VALVE_CLOSE_LIM := ${gv_a_valve_close_lim},
		GV_B_VALVE_CLOSE_LIM := ${gv_b_valve_close_lim},
		CP_LLCV_POS_PCT := ${cp_llcv_pos_pct},
		CP_REGEN_TEMP_ALRM := ${cp_regen_temp_alrm},
		CP_REGEN_PID_SETPT_REQ_DEGC := ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_SETPT_REQ_DEGC,
		CP_REGEN_PID_KP := ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_KP,
		CP_REGEN_PID_TN := ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_TN,
		CP_REGEN_PID_TV := ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_TV,
		CP_REGEN_PID_TD := ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_TD,
		CP_REGEN_PID_CYCLE_TIME := ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_CYCLE_TIME,
		CP_REGEN_RUN_REQ := ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_RUN_REQ,
		CP_REGEN_TEMP_INTLK => ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_TEMP_INTLK,
		CP_REGEN_TEMP_ALRM_INTLK => ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_TEMP_ALRM_INTLK,
		CP_REGEN_GV_A_CLOSE_INTLK => ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_GV_A_CLOSE_INTLK,
		CP_REGEN_GV_B_CLOSE_INTLK => ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_GV_B_CLOSE_INTLK,
		CP_REGEN_LLCV_POS_INTLK => ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_LLCV_POS_INTLK,
		CP_REGEN_INTLK => ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_INTLK,
		CP_REGEN_RUN_CTRL => ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_RUN_CTRL,
		CP_REGEN_PID_SETPT_DEGC => ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_SETPT_DEGC,
		CP_REGEN_PID_ERR_FLAG => ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_ERR_FLAG,
		CP_REGEN_PID_ERR_CODE => ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_ERR_CODE,
		CP_REGEN_PID_ERR_STR => ${ifo}_${sys}_${bld}_${loc}_${inst}_REGEN_PID_ERR_STR,
		CP_REGEN_HTR_CTRL_DEGC => ${ifo}_${sys}_${bld}_${loc}_TY${inst}_REGEN_HTR_CTRL_DEGC
	);

	${ifo}_${sys}_${bld}_${loc}_TY${inst}_REGEN_HTR_CTRL_MA := RegenHeaterDegreesCelsiusToMilliAmpsFun(${ifo}_${sys}_${bld}_${loc}_TY${inst}_REGEN_HTR_CTRL_DEGC);
	${io_out_ma} := ${ifo}_${sys}_${bld}_${loc}_TY${inst}_REGEN_HTR_CTRL_MA;
"@

return $text
}


#-------------------------------------------------------------------------------
# H2O Pressure

function h2o_pressure_declaration ($ifo, $sys, $bld, $loc) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} H2O Pressure *)

	${ifo}_${sys}_${bld}_${loc}_H2O_PRESS_VOLTS: | | LREAL; | (*~ ${loc} H2O Pressure (Volts)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :Volts: EGU)
	| | | (OPC_PROP[0101] :${loc} H2O Pressure: DESC) *)

	${ifo}_${sys}_${bld}_${loc}_H2O_PRESS_VOLTS_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)


	${ifo}_${sys}_${bld}_${loc}_H2O_PRESS_PSI: | | LREAL; | (*~ ${loc} H2O Pressure (PSI) [${ifo}:${sys}-${bld}_${loc}_H2O_PRESS]
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0100] :PSI: EGU)
	| | | (OPC_PROP[0101] :${loc} H2O Pressure: DESC)
	| | | (OPC_PROP[0102] :100: HOPR)
	| | | (OPC_PROP[0308] :80: HIGH)
	| | | (OPC_PROP[0309] :10: LOW)
	| | | (OPC_PROP[8728] :MINOR: HSV)
	| | | (OPC_PROP[8729] :MAJOR: LSV)
	| | | (OPC_PROP[8800] :${ifo}_${sys}-${bld}_${loc}_H2O_PRESS: ALIAS) *)

	${ifo}_${sys}_${bld}_${loc}_H2O_PRESS_PSI_ERROR: | | BOOL; | (*~ (FALSE = No Error, TRUE = Error)
	| | | (OPC :1: Make variable visible)
	| | | (OPC_PROP[0005] :1: read-only)
	| | | (OPC_PROP[0106] :Error: ONAM)
	| | | (OPC_PROP[0107] :No Error: ZNAM)
	| | | (OPC_PROP[8700] :MAJOR: OSV)
	| | | (OPC_PROP[8701] :NO_ALARM: ZSV) *)
"@

return $text
}

function h2o_pressure_implementation ($ifo, $sys, $bld, $loc, $io_in_volts) {
$text = @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
	(* ${loc} H2O Pressure *)

	${ifo}_${sys}_${bld}_${loc}_H2O_PRESS_VOLTS := ${io_in_volts};
	${ifo}_${sys}_${bld}_${loc}_H2O_PRESS_VOLTS_ERROR := ${io_in_volts}_ERROR;

	${ifo}_${sys}_${bld}_${loc}_H2O_PRESS_PSI := WaterPressureVoltsToPoundsPerSquareInchFun(${ifo}_${sys}_${bld}_${loc}_H2O_PRESS_VOLTS);
	${ifo}_${sys}_${bld}_${loc}_H2O_PRESS_PSI_ERROR := ${ifo}_${sys}_${bld}_${loc}_H2O_PRESS_VOLTS_ERROR;
"@

return $text
}
