﻿# Patrick J. Thomas, California Institute of Technology, LIGO Hanford


. "./message_filter.ps1"
. "./h0vacly.ps1"


$root_path = Split-Path -Parent (Split-Path -Parent (Split-Path -Parent $MyInvocation.MyCommand.Definition))

$solution_name = "H0VACLY"
$solution_folder_path = [System.IO.Path]::GetFullPath((Join-Path $root_path ".\Target\H0VACLY\H0VACLY"))
$solution_file_path = [System.IO.Path]::GetFullPath((Join-Path $root_path ".\Target\H0VACLY\H0VACLY\H0VACLY.sln"))

$project_name = "H0VACLY"
$project_folder_path = [System.IO.Path]::GetFullPath((Join-Path $root_path ".\Target\H0VACLY\H0VACLY\H0VACLY"))

$template_path = "C:\TwinCAT\3.1\Components\Base\PrjTemplate\TwinCAT Project.tsproj"


if (Test-Path $solution_folder_path -PathType Container) {
    Remove-Item $solution_folder_path -Recurse -Force
}

New-Item $solution_folder_path -ItemType Directory


AddMessageFilterClass

[EnvDteUtils.MessageFilter]::Register()

$dte = New-Object -ComObject "VisualStudio.DTE.16.0"
#$dte.MainWindow.Activate()

$solution = $dte.Solution
$solution.Create($solution_folder_path, $solution_name)
$solution.SaveAs($solution_file_path)

$project = $solution.AddFromTemplate($template_path, $project_folder_path, $project_name)
$system_manager = $project.Object


$su1_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit1</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su2_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit2</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su3_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit3</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su4_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit4</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su5_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit5</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su6_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit6</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su7_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit7</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su8_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit8</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"


# Creates the device.
$tiid = $system_manager.LookupTreeItem("TIID")
$ethercat_master = $tiid.CreateChild("EtherCAT Master", 111)

# Creates the terminals.

$item = $ethercat_master.CreateChild("Term M0 (EK1101)", 9099, "", "EK1101-0000-0018")

$item = $item.CreateChild("Term M1 (EL1004)", 9099, "", "EL1004-0000-0018")
$item = $item.CreateChild("Term M2 (EL1004)", 9099, "", "EL1004-0000-0018")
$item = $item.CreateChild("Term M3 (EL1004)", 9099, "", "EL1004-0000-0018")
$item = $item.CreateChild("Term M4 (EL1004)", 9099, "", "EL1004-0000-0018")

$item = $item.CreateChild("Term M5 (EL2624)", 9099, "", "EL2624-0000-0018")
$item = $item.CreateChild("Term M6 (EL2624)", 9099, "", "EL2624-0000-0018")
$item = $item.CreateChild("Term M7 (EL2624)", 9099, "", "EL2624-0000-0018")
$item = $item.CreateChild("Term M8 (EL2624)", 9099, "", "EL2624-0000-0018")
$item = $item.CreateChild("Term M9 (EL2624)", 9099, "", "EL2624-0000-0018")

$item = $item.CreateChild("Term M10 (EL9400)", 9099, "", "EL9400")

$item = $item.CreateChild("Term M11 (EL3004)", 9099, "", "EL3004-0000-0020")
$item = $item.CreateChild("Term M12 (EL3004)", 9099, "", "EL3004-0000-0020")
$item = $item.CreateChild("Term M13 (EL3004)", 9099, "", "EL3004-0000-0020")
$item = $item.CreateChild("Term M14 (EL3004)", 9099, "", "EL3004-0000-0020")
$item = $item.CreateChild("Term M15 (EL3004)", 9099, "", "EL3004-0000-0020")
$item = $item.CreateChild("Term M16 (EL3004)", 9099, "", "EL3004-0000-0020")

$item = $item.CreateChild("Term M17 (EL3024)", 9099, "", "EL3024-0000-0018")
$item = $item.CreateChild("Term M18 (EL3024)", 9099, "", "EL3024-0000-0018")

$item = $item.CreateChild("Term M19 (EL3314)", 9099, "", "EL3314-0000-0019")

$item = $item.CreateChild("Term M20 (EL9400)", 9099, "", "EL9400")

$item = $item.CreateChild("Term M21 (EL3314)", 9099, "", "EL3314-0000-0019")

$item = $item.CreateChild("Term M22 (EL4024)", 9099, "", "EL4024-0000-0017")
$item = $item.CreateChild("Term M23 (EL4024)", 9099, "", "EL4024-0000-0017")

$item = $item.CreateChild("Term M24 (EL9011)", 9099, "", "EL9011")


# Box 1

$box_1 = $ethercat_master.CreateChild("Box 1 (CU1128)", 9099, "", "CU1128-0000-0001")

# MKS Instruments, Inc. 390 Micro-Ion ATM
$item = $box_1.CreateChild("Pressure Gauge PT153 (390 Micro-Ion ATM)", 9099, "", "V50B_P177A_R01030000")
$item.ConsumeXml($su1_xml)

# MKS Instruments, Inc. 390 Micro-Ion ATM
$item = $item.CreateChild("Pressure Gauge PT152 (390 Micro-Ion ATM)", 9099, "", "V50B_P177A_R01030000")
$item.ConsumeXml($su2_xml)

# MKS Instruments, Inc. 390 Micro-Ion ATM
$item = $box_1.CreateChild("Pressure Gauge PT154 (390 Micro-Ion ATM)", 9099, "", "V50B_P177A_R01030000")
$item.ConsumeXml($su3_xml)

# Box 2

$box_2 = $system_manager.LookupTreeItem("TIID^EtherCAT Master^Box 1 (CU1128)^Box 2 (CU1128-B)")

# MKS Instruments, Inc. 390 Micro-Ion ATM
$item = $box_2.CreateChild("Pressure Gauge PT155 (390 Micro-Ion ATM)", 9099, "", "V50B_P177A_R01030000")
$item.ConsumeXml($su4_xml)

# Box 3

$box_3 = $system_manager.LookupTreeItem("TIID^EtherCAT Master^Box 1 (CU1128)^Box 3 (CU1128-C)")

$item = $box_3.CreateChild("Media Converter 1 (CU1521)", 9099, "", "CU1521-0010-0000")
$item = $item.CreateChild("Media Converter 2 (CU1521)", 9099, "", "CU1521-0010-0000")

$coupler = $item.CreateChild("Term 0 (EK1101)", 9099, "", "EK1101-0000-0018")

$item = $coupler.CreateChild("Term 1 (EL3004)", 9099, "", "EL3004-0000-0021")
$item = $item.CreateChild("Term 2 (EL3004)", 9099, "", "EL3004-0000-0021")

$item = $item.CreateChild("Term 3 (EL9011)", 9099, "", "EL9011")


# MKS Instruments, Inc. 390 Micro-Ion ATM
$item = $box_3.CreateChild("Pressure Gauge PT157 (390 Micro-Ion ATM)", 9099, "", "V50B_P177A_R01030000")
$item.ConsumeXml($su6_xml)

# MKS Instruments, Inc. 390 Micro-Ion ATM
$item = $box_3.CreateChild("Pressure Gauge PT156 (390 Micro-Ion ATM)", 9099, "", "V50B_P177A_R01030000")
$item.ConsumeXml($su5_xml)


# Box 4

$box_4 = $coupler.CreateChild("Box 4 (CU1128)", 9099, "", "CU1128-0000-0001")

# MKS Instruments, Inc. 390 Micro-Ion ATM
$item = $box_4.CreateChild("Pressure Gauge PT158 (390 Micro-Ion ATM)", 9099, "", "V50B_P177A_R01030000")
$item.ConsumeXml($su7_xml)

# MKS Instruments, Inc. 390 Micro-Ion ATM
$item = $box_4.CreateChild("Pressure Gauge PT159 (390 Micro-Ion ATM)", 9099, "", "V50B_P177A_R01030000")
$item.ConsumeXml($su8_xml)

# Box 5

$box_5 = $system_manager.LookupTreeItem("TIID^EtherCAT Master^Box 1 (CU1128)^Box 3 (CU1128-C)^Box 4 (CU1128)^Box 5 (CU1128-B)")

# Box 6

$box_6 = $system_manager.LookupTreeItem("TIID^EtherCAT Master^Box 1 (CU1128)^Box 3 (CU1128-C)^Box 4 (CU1128)^Box 6 (CU1128-C)")


# Creates the PLC project.
$tipc = $system_manager.LookupTreeItem("TIPC")
$tipc.CreateChild("PLC1", 0, "", "Standard PLC Template.plcproj")

# Adds a reference to the Vacuum library.
$references = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^References")
$references.AddLibrary("Vacuum", "*", "LIGO")


# Creates the GVL file.
$gvls_folder = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^GVLs")
$gvls_folder.CreateChild("GVL", 615)


# Sets the GVL declaration text.
$gvl = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^GVLs^GVL")
$gvl.DeclarationText = gvl_declaration


# Sets the MAIN implementation text.
$main = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^POUs^MAIN")
$main.ImplementationText = main_implementation


# Activates the configuration.
# If this is not done a subsequent activate configuration will not create the links. It is not clear why.
$system_manager.ActivateConfiguration()


$project.Save()
$solution.SaveAs($solution_file_path)

$dte.Quit()

[EnvDteUtils.MessageFilter]::Revoke()
