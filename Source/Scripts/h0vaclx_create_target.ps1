﻿# Patrick J. Thomas, California Institute of Technology, LIGO Hanford


. "./message_filter.ps1"
. "./h0vaclx.ps1"


$root_path = Split-Path (Split-Path $PSScriptRoot -Parent) -Parent

$solution_name = "H0VACLX"
$solution_folder_path = [System.IO.Path]::GetFullPath((Join-Path $root_path ".\Target\H0VACLX\H0VACLX"))
$solution_file_path = [System.IO.Path]::GetFullPath((Join-Path $root_path ".\Target\H0VACLX\H0VACLX\H0VACLX.sln"))

$project_name = "H0VACLX"
$project_folder_path = [System.IO.Path]::GetFullPath((Join-Path $root_path ".\Target\H0VACLX\H0VACLX\H0VACLX"))

$template_path = "C:\TwinCAT\3.1\Components\Base\PrjTemplate\TwinCAT Project.tsproj"


if (Test-Path $solution_folder_path -PathType Container) {
    Remove-Item $solution_folder_path -Recurse -Force
}

New-Item $solution_folder_path -ItemType Directory


AddMessageFilterClass

[EnvDteUtils.MessageFilter]::Register()

$dte = New-Object -ComObject "VisualStudio.DTE.15.0"
$dte.MainWindow.Activate()

$solution = $dte.Solution
$solution.Create($solution_folder_path, $solution_name)
$solution.SaveAs($solution_file_path)

$project = $solution.AddFromTemplate($template_path, $project_folder_path, $project_name)
$system_manager = $project.Object


$su1_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit1</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su2_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit2</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su3_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit3</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su4_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit4</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su5_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit5</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su6_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit6</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su7_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit7</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"
$su8_xml = "<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>SyncUnit8</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>"


# Creates the device.
$tiid = $system_manager.LookupTreeItem("TIID")
$ethercat_master = $tiid.CreateChild("EtherCAT Master", 111)

# Creates the terminals.

$term_m0 = $ethercat_master.CreateChild("Term M0 (EK1101)", 9099, "", "EK1101-0000-0018")


# Box 1

$box_1 = $ethercat_master.CreateChild("Box 1 (CU1128)", 9099, "", "CU1128-0000-0001")

# Inficon BPG402 Pressure Gauges: Vendor id = 00000644, Product code = 0C, Revision no = 06
$item = $box_1.CreateChild("Pressure Gauge PT191 (BPG 402)", 9099, "", "V00000644_P0C_R06")
$item.ConsumeXml($su1_xml);

# Inficon BPG402 Pressure Gauges: Vendor id = 00000644, Product code = 0C, Revision no = 06
$item = $item.CreateChild("Pressure Gauge PT192 (BPG 402)", 9099, "", "V00000644_P0C_R06")
$item.ConsumeXml($su2_xml);

# Inficon BPG402 Pressure Gauges: Vendor id = 00000644, Product code = 0C, Revision no = 06
$item = $item.CreateChild("Pressure Gauge PT193 (BPG 402)", 9099, "", "V00000644_P0C_R06")
$item.ConsumeXml($su3_xml)


# MKS Instruments, Inc. 390 Micro-Ion ATM
$item = $box_1.CreateChild("Pressure Gauge PT132 (390 Micro-Ion ATM)", 9099, "", "V50B_P177A_R01030000")
$item.ConsumeXml($su8_xml)

# Box 2

$box_2 = $system_manager.LookupTreeItem("TIID^EtherCAT Master^Box 1 (CU1128)^Box 2 (CU1128-B)")

# Inficon BCG450 Pressure Gauges: Vendor id = 00000644, Product code = 0D, Revision no = 08
$item = $box_2.CreateChild("Pressure Gauge PT170 (BCG 450)", 9099, "", "V00000644_P0D_R08")
$item.ConsumeXml($su4_xml)

# Inficon BCG450 Pressure Gauges: Vendor id = 00000644, Product code = 0D, Revision no = 08
$item = $box_2.CreateChild("Pressure Gauge PT180 (BCG 450)", 9099, "", "V00000644_P0D_R08")
$item.ConsumeXml($su5_xml)

# Box 3

$box_3 = $system_manager.LookupTreeItem("TIID^EtherCAT Master^Box 1 (CU1128)^Box 3 (CU1128-C)")

# Inficon BCG450 Pressure Gauges: Vendor id = 00000644, Product code = 0D, Revision no = 08
$item = $box_3.CreateChild("Pressure Gauge PT110 (BCG 450)", 9099, "", "V00000644_P0D_R08")
$item.ConsumeXml($su6_xml)

# Inficon BPG402 Pressure Gauges: Vendor id = 00000644, Product code = 0C, Revision no = 06
$item = $box_3.CreateChild("Pressure Gauge PT140 (BPG 402)", 9099, "", "V00000644_P0C_R06")
$item.ConsumeXml($su7_xml)


$term_m0.CreateChild("Term M1 (EL1004)", 9099, "", "EL1004-0000-0018")
$term_m0.CreateChild("Term M2 (EL1004)", 9099, "", "EL1004-0000-0018")
$term_m0.CreateChild("Term M3 (EL1004)", 9099, "", "EL1004-0000-0018")
$term_m0.CreateChild("Term M4 (EL1004)", 9099, "", "EL1004-0000-0018")

$term_m0.CreateChild("Term M5 (EL2624)", 9099, "", "EL2624-0000-0018")
$term_m0.CreateChild("Term M6 (EL2624)", 9099, "", "EL2624-0000-0018")
$term_m0.CreateChild("Term M7 (EL2624)", 9099, "", "EL2624-0000-0018")
$term_m0.CreateChild("Term M8 (EL2624)", 9099, "", "EL2624-0000-0018")
$term_m0.CreateChild("Term M9 (EL2624)", 9099, "", "EL2624-0000-0018")

$term_m0.CreateChild("Term M10 (EL9400)", 9099, "", "EL9400")

$term_m0.CreateChild("Term M11 (EL3004)", 9099, "", "EL3004-0000-0020")
$term_m0.CreateChild("Term M12 (EL3004)", 9099, "", "EL3004-0000-0020")
$term_m0.CreateChild("Term M13 (EL3004)", 9099, "", "EL3004-0000-0020")
$term_m0.CreateChild("Term M14 (EL3004)", 9099, "", "EL3004-0000-0020")
$term_m0.CreateChild("Term M15 (EL3004)", 9099, "", "EL3004-0000-0020")
$term_m0.CreateChild("Term M16 (EL3004)", 9099, "", "EL3004-0000-0020")

$term_m0.CreateChild("Term M17 (EL3024)", 9099, "", "EL3024-0000-0018")
$term_m0.CreateChild("Term M18 (EL3024)", 9099, "", "EL3024-0000-0018")

$term_m0.CreateChild("Term M19 (EL3314)", 9099, "", "EL3314-0000-0019")

$term_m0.CreateChild("Term M20 (EL9400)", 9099, "", "EL9400")

$term_m0.CreateChild("Term M21 (EL3314)", 9099, "", "EL3314-0000-0019")
$term_m0.CreateChild("Term M22 (EL3314)", 9099, "", "EL3314-0000-0019")

$term_m0.CreateChild("Term M23 (EL4024)", 9099, "", "EL4024-0000-0017")
$term_m0.CreateChild("Term M24 (EL4024)", 9099, "", "EL4024-0000-0017")

$term_m0.CreateChild("Term M25 (EL9011)", 9099, "", "EL9011")


# Creates the PLC project.
$tipc = $system_manager.LookupTreeItem("TIPC")
$tipc.CreateChild("PLC1", 0, "", "Standard PLC Template.plcproj")

# Adds a reference to the Vacuum library.
$references = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^References")
$references.AddLibrary("Vacuum", "*", "LIGO")


# Creates the GVL file.
$gvls_folder = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^GVLs")
$gvls_folder.CreateChild("GVL", 615)


# Sets the GVL declaration text.
$gvl = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^GVLs^GVL")
$gvl.DeclarationText = gvl_declaration


# Sets the MAIN implementation text.
$main = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^POUs^MAIN")
$main.ImplementationText = main_implementation


# Activates the configuration.
# If this is not done a subsequent activate configuration will not create the links. It is not clear why.
$system_manager.ActivateConfiguration()


$project.Save()
$solution.SaveAs($solution_file_path)

$dte.Quit()

[EnvDteUtils.MessageFilter]::Revoke()
