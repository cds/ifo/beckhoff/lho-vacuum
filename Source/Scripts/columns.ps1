# $Header$

# Patrick J. Thomas, California Institute of Technology, LIGO Hanford

# Formats input into space padded columns. Lines without delimiters are not considered in determining column widths and are printed in place without modification.


function columns ($text, $sep) {
	# Splits the string into an array of lines
	$lines = $text.Split([string[]]"`r`n", 'None')

	# Gets the total number of columns
	$total_number_of_columns = 0
	foreach ($line in $lines) {
		$columns = $line.Split([string[]]$sep, 'None')
		$number_of_columns = $columns.Count

		if ($number_of_columns -gt $total_number_of_columns) {
			$total_number_of_columns = $number_of_columns
		}
	}

	# Creates an array containing the maximum width of each column
	$max_column_widths = @(0) * $total_number_of_columns
	foreach ($line in $lines) {
		$columns = $line.Split([string[]]$sep, 'None')
		$number_of_columns = $columns.Count

		if ($number_of_columns -gt 1) {
			# The line has at least one delimiter
			for ($column_number = 0; $column_number -lt $number_of_columns; $column_number++) {
				$column_text = $columns[$column_number]
				$column_width = $column_text.Length

				if ($column_width -gt $max_column_widths[$column_number]) {
					$max_column_widths[$column_number] = $column_width
				}
			}
		}
	}

	# Prints each column space padded to the maximum width of each column
	foreach ($line in $lines) {
		$columns = $line.Split([string[]]$sep, 'None')
		$number_of_columns = $columns.Count

		if ($number_of_columns -gt 1) {
			# The line has at least one delimiter
			$formatted_line = ''
			for ($column_number = 0; $column_number -lt $number_of_columns; $column_number++) {
				$column_text = $columns[$column_number]
				$padded_column_width = $max_column_widths[$column_number]

				$padded_column_text = $column_text.PadRight($padded_column_width)
				$formatted_line += $padded_column_text
			}
		}
		else {
			$formatted_line = $line
		}

		$formatted_line = $formatted_line.TrimEnd()
		$formatted_text += $formatted_line + "`r`n"
	}

	return $formatted_text
}

