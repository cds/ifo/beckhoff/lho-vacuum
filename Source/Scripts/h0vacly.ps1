# $Header$

# Patrick J. Thomas, California Institute of Technology, LIGO Hanford


. ".\macro_functions.ps1"
. ".\columns.ps1"


function gvl_declaration {
$declaration_text += "VAR_GLOBAL`r`n"


# EtherCAT devices


$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M1" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M1 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M2" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M2 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M3" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M3 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M4" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M4 (EL1004)") + "`r`n`r`n`r`n"


$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M5" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M5 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M6" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M6 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M7" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M7 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M8" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M8 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M9" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M9 (EL2624)") + "`r`n`r`n`r`n"


$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M11" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M11 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M12" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M12 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M13" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M13 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M14" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M14 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M15" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M15 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M16" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M16 (EL3004)") + "`r`n`r`n`r`n"


$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M17" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M17 (EL3024)") + "`r`n`r`n`r`n"

$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M18" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M18 (EL3024)") + "`r`n`r`n`r`n"


$declaration_text += (el3314_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M19" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M19 (EL3314)") + "`r`n`r`n`r`n"

$declaration_text += (el3314_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M21" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M21 (EL3314)") + "`r`n`r`n`r`n"


$declaration_text += (el4024_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M22" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M22 (EL4024)") + "`r`n`r`n`r`n"

$declaration_text += (el4024_declaration -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M23" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M23 (EL4024)") + "`r`n`r`n`r`n"


$declaration_text += (_390_micro_ion_atm_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "HAM7" -inst "PT153" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Pressure Gauge PT153 (390 Micro-Ion ATM)") + "`r`n`r`n`r`n"

$declaration_text += (_390_micro_ion_atm_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "RT" -inst "PT152" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Pressure Gauge PT152 (390 Micro-Ion ATM)") + "`r`n`r`n`r`n"

$declaration_text += (_390_micro_ion_atm_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "FCTA" -inst "PT154" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Pressure Gauge PT154 (390 Micro-Ion ATM)") + "`r`n`r`n`r`n"

$declaration_text += (_390_micro_ion_atm_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "FCTB" -inst "PT155" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Box 2 (CU1128-B)^Pressure Gauge PT155 (390 Micro-Ion ATM)") + "`r`n`r`n`r`n"

$declaration_text += (_390_micro_ion_atm_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "FCTC" -inst "PT156" -link_prefix "TIID^EtherCAT Master^Pressure Gauge PT156 (390 Micro-Ion ATM)") + "`r`n`r`n`r`n"

$declaration_text += (_390_micro_ion_atm_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "FCTC" -inst "PT157" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Box 3 (CU1128-C)^Pressure Gauge PT157 (390 Micro-Ion ATM)") + "`r`n`r`n`r`n"

$declaration_text += (_390_micro_ion_atm_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "FCTC" -inst "PT158" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Box 3 (CU1128-C)^Box 4 (CU1128)^Pressure Gauge PT158 (390 Micro-Ion ATM)") + "`r`n`r`n`r`n"

$declaration_text += (_390_micro_ion_atm_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "HAM8" -inst "PT159" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Box 3 (CU1128-C)^Box 4 (CU1128)^Pressure Gauge PT159 (390 Micro-Ion ATM)") + "`r`n`r`n`r`n"


$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "FCES" -term "TERM_1" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Box 3 (CU1128-C)^Term 0 (EK1101)^Term 1 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "FCES" -term "TERM_2" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Box 3 (CU1128-C)^Term 0 (EK1101)^Term 2 (EL3004)") + "`r`n`r`n`r`n"


$declaration_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# Hardware devices


$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "HAM10" -inst "193") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "HAM11" -inst "195") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "HAM12" -inst "197") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "BSC8" -inst "181") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "BM" -inst "186") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "IBM" -inst "192") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "OBM" -inst "196") + "`r`n`r`n`r`n"

$declaration_text += (cyropump_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "170") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "GV1" -inst "119") + "`r`n`r`n`r`n"

$declaration_text += (electric_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "GV1" -inst "119") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "GV3" -inst "139") + "`r`n`r`n`r`n"

$declaration_text += (electric_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "GV3" -inst "139") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "GV5" -inst "159") + "`r`n`r`n`r`n"

$declaration_text += (pneumatic_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "GV5" -inst "159") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "GV6" -inst "169") + "`r`n`r`n`r`n"

$declaration_text += (pneumatic_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "GV6" -inst "169") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "Y1" -inst "120") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "Y1" -inst "120") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "Y1" -inst "120") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "Y3" -inst "114") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "Y3" -inst "114") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "Y3" -inst "114") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "Y4" -inst "124") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "Y4" -inst "124") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "Y4" -inst "124") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "X0" -inst "100") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "X0" -inst "100") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "X0" -inst "100") + "`r`n`r`n`r`n"


$declaration_text += (dewar_level_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "105") + "`r`n`r`n`r`n"


$declaration_text += (pump_level_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "100") + "`r`n`r`n`r`n"

$declaration_text += (liquid_level_control_valve_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "100") + "`r`n`r`n`r`n"


$declaration_text += (discharge_pressure_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "101") + "`r`n`r`n`r`n"


$declaration_text += (discharge_temperature_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "102") + "`r`n`r`n`r`n"

$declaration_text += (discharge_temperature_spare_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "102") + "`r`n`r`n`r`n"

$declaration_text += (discharge_temperature_comparison_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "102") + "`r`n`r`n`r`n"


$declaration_text += (liquid_level_control_valve_enable_position_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "100") + "`r`n`r`n`r`n"

$declaration_text += (liquid_level_control_valve_enable_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "100") + "`r`n`r`n`r`n"


$declaration_text += (regen_temperature_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "103") + "`r`n`r`n`r`n"

$declaration_text += (regen_temperature_alarm_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "103") + "`r`n`r`n`r`n"

$declaration_text += (regen_declaration -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "103") + "`r`n"


$declaration_text += (ion_pump_controller_ipcmini_declaration -ifo "H0" -sys "VAC" -bld "FCES" -loc "IPFCC6" -inst "C6") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_ipcmini_declaration -ifo "H0" -sys "VAC" -bld "FCES" -loc "IPFCC8" -inst "C8") + "`r`n`r`n`r`n"


$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "FCES" -loc "IPFCC9" -inst "C9") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "FCES" -loc "IPFCD1" -inst "D1") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_gamma_single_channel_declaration -ifo "H0" -sys "VAC" -bld "FCES" -loc "IPFCH8A" -inst "H8A") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "FCES" -loc "IP26" -inst "126") + "`r`n`r`n`r`n"


$declaration_text += "END_VAR"


$declaration_text = columns -text $declaration_text -sep "|"

$declaration_text
}


#-------------------------------------------------------------------------------


function main_implementation {


# EtherCAT devices error checking


$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M1") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M2") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M3") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M4") + "`r`n`r`n`r`n"


$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M5") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M6") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M7") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M8") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M9") + "`r`n`r`n`r`n"


$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M11") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M12") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M13") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M14") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M15") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M16") + "`r`n`r`n`r`n"


$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M17") + "`r`n`r`n`r`n"

$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M18") + "`r`n`r`n`r`n"


$implementation_text += (el3314_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M19") + "`r`n`r`n`r`n"

$implementation_text += (el3314_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M21") + "`r`n`r`n`r`n"


$implementation_text += (el4024_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M22") + "`r`n`r`n`r`n"

$implementation_text += (el4024_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M23") + "`r`n`r`n`r`n"


$implementation_text += (_390_micro_ion_atm_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "HAM7" -inst "PT153") + "`r`n`r`n`r`n"

$implementation_text += (_390_micro_ion_atm_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "RT" -inst "PT152") + "`r`n`r`n`r`n"

$implementation_text += (_390_micro_ion_atm_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "FCTA" -inst "PT154") + "`r`n`r`n`r`n"

$implementation_text += (_390_micro_ion_atm_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "FCTB" -inst "PT155") + "`r`n`r`n`r`n"

$implementation_text += (_390_micro_ion_atm_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "FCTC" -inst "PT156") + "`r`n`r`n`r`n"

$implementation_text += (_390_micro_ion_atm_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "FCTC" -inst "PT157") + "`r`n`r`n`r`n"

$implementation_text += (_390_micro_ion_atm_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "FCTC" -inst "PT158") + "`r`n`r`n`r`n"

$implementation_text += (_390_micro_ion_atm_error_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "HAM8" -inst "PT159") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# EtherCAT devices counts to engineering units


$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M11") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M12") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M13") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M14") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M15") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M16") + "`r`n`r`n`r`n"


$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M17") + "`r`n`r`n`r`n"

$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M18") + "`r`n`r`n`r`n"


$implementation_text += (el3314_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M19") + "`r`n`r`n`r`n"

$implementation_text += (el3314_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M21") + "`r`n`r`n`r`n"


$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "FCES" -term "TERM_1") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "FCES" -term "TERM_2") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# Hardware devices


$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "HAM10" -inst "193" -io_in_volts "H0_VAC_LY_TERM_M11_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "HAM11" -inst "195" -io_in_volts "H0_VAC_LY_TERM_M12_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "HAM12" -inst "197" -io_in_volts "H0_VAC_LY_TERM_M12_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "BSC8" -inst "181" -io_in_volts "H0_VAC_LY_TERM_M12_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "BM" -inst "186" -io_in_volts "H0_VAC_LY_TERM_M11_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "IBM" -inst "192" -io_in_volts "H0_VAC_LY_TERM_M12_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "OBM" -inst "196" -io_in_volts "H0_VAC_LY_TERM_M13_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cyropump_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "170" -io_in_volts "H0_VAC_LY_TERM_M15_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "GV1" -inst "119" -io_in_volts "H0_VAC_LY_TERM_M14_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (electric_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "GV1" -inst "119" -io_in_mtr "H0_VAC_LY_TERM_M2_CHAN1_IN_BOOL" -io_in_close_lim "H0_VAC_LY_TERM_M2_CHAN2_IN_BOOL" -io_in_open_lim "H0_VAC_LY_TERM_M2_CHAN3_IN_BOOL" -io_out_open "H0_VAC_LY_TERM_M7_CHAN1_OUT_BOOL" -io_out_run_mtr "H0_VAC_LY_TERM_M7_CHAN2_OUT_BOOL" -io_out_close "H0_VAC_LY_TERM_M8_CHAN3_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "GV3" -inst "139" -io_in_volts "H0_VAC_LY_TERM_M15_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (electric_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "GV3" -inst "139" -io_in_mtr "H0_VAC_LY_TERM_M1_CHAN2_IN_BOOL" -io_in_close_lim "H0_VAC_LY_TERM_M1_CHAN3_IN_BOOL" -io_in_open_lim "H0_VAC_LY_TERM_M1_CHAN4_IN_BOOL" -io_out_open "H0_VAC_LY_TERM_M6_CHAN3_OUT_BOOL" -io_out_run_mtr "H0_VAC_LY_TERM_M6_CHAN4_OUT_BOOL" -io_out_close "H0_VAC_LY_TERM_M8_CHAN4_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "GV5" -inst "159" -io_in_volts "H0_VAC_LY_TERM_M15_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (pneumatic_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "GV5" -inst "159" -io_in_close_lim "H0_VAC_LY_TERM_M2_CHAN4_IN_BOOL" -io_in_open_lim "H0_VAC_LY_TERM_M3_CHAN1_IN_BOOL" -io_out_open "H0_VAC_LY_TERM_M7_CHAN3_OUT_BOOL" -io_out_close "H0_VAC_LY_TERM_M7_CHAN4_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "GV6" -inst "169" -io_in_volts "H0_VAC_LY_TERM_M15_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (pneumatic_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "GV6" -inst "169" -io_in_close_lim "H0_VAC_LY_TERM_M3_CHAN2_IN_BOOL" -io_in_open_lim "H0_VAC_LY_TERM_M3_CHAN3_IN_BOOL" -io_out_open "H0_VAC_LY_TERM_M8_CHAN1_OUT_BOOL" -io_out_close "H0_VAC_LY_TERM_M8_CHAN2_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "Y1" -inst "120" -io_in_volts "H0_VAC_LY_TERM_M13_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "Y1" -inst "120" -io_out_pwr_ctrl "H0_VAC_LY_TERM_M6_CHAN1_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "Y1" -inst "120" -io_in_volts "H0_VAC_LY_TERM_M13_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "Y3" -inst "114" -io_in_volts "H0_VAC_LY_TERM_M11_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "Y3" -inst "114" -io_out_pwr_ctrl "H0_VAC_LY_TERM_M5_CHAN1_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "Y3" -inst "114" -io_in_volts "H0_VAC_LY_TERM_M11_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "Y4" -inst "124" -io_in_volts "H0_VAC_LY_TERM_M13_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "Y4" -inst "124" -io_out_pwr_ctrl "H0_VAC_LY_TERM_M5_CHAN3_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "Y4" -inst "124" -io_in_volts "H0_VAC_LY_TERM_M14_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "X0" -inst "100" -io_in_volts "H0_VAC_LY_TERM_M14_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "X0" -inst "100" -io_out_pwr_ctrl "H0_VAC_LY_TERM_M6_CHAN2_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "X0" -inst "100" -io_in_volts "H0_VAC_LY_TERM_M14_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (dewar_level_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "105" -io_in_ma "H0_VAC_LY_TERM_M17_CHAN3_IN_MA") + "`r`n`r`n`r`n"


$implementation_text += (pump_level_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "100" -io_in_ma "H0_VAC_LY_TERM_M17_CHAN1_IN_MA") + "`r`n`r`n`r`n"

$implementation_text += (liquid_level_control_valve_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "100" -pirani_press_torr "H0_VAC_LY_Y3_PT114A_PRESS_TORR" -discharge_press_psig "H0_VAC_LY_CP1_PT101_DISCHARGE_PRESS_PSIG" -cp_pump_level_pct "H0_VAC_LY_CP1_LT100_PUMP_LEVEL_PCT" -io_out_ma "H0_VAC_LY_TERM_M22_CHAN1_OUT_MA") + "`r`n`r`n`r`n"


$implementation_text += (discharge_pressure_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "101" -io_in_ma "H0_VAC_LY_TERM_M17_CHAN2_IN_MA") + "`r`n`r`n`r`n"


$implementation_text += (discharge_temperature_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "102" -io_in_degc "H0_VAC_LY_TERM_M19_CHAN2_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (discharge_temperature_spare_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "102" -io_in_degc "H0_VAC_LY_TERM_M19_CHAN3_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (discharge_temperature_comparison_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "102") + "`r`n`r`n`r`n"


$implementation_text += (liquid_level_control_valve_enable_position_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "100" -io_in_pos "H0_VAC_LY_TERM_M1_CHAN1_IN_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (liquid_level_control_valve_enable_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "100" -io_out_ctrl "H0_VAC_LY_TERM_M5_CHAN2_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (regen_temperature_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "103" -io_in_degc "H0_VAC_LY_TERM_M19_CHAN1_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (regen_temperature_alarm_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "103" -io_in_temp_alrm "H0_VAC_LY_TERM_M3_CHAN4_IN_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (regen_implementation -ifo "H0" -sys "VAC" -bld "LY" -loc "CP1" -inst "103" -cp_regen_temp_degc "H0_VAC_LY_CP1_TE103A_REGEN_TEMP_DEGC" -gv_a_valve_close_lim "H0_VAC_LY_GV6_ZSC169_VALVE_CLOSE_LIM" -gv_b_valve_close_lim "H0_VAC_LY_GV5_ZSC159_VALVE_CLOSE_LIM" -cp_llcv_pos_pct "H0_VAC_LY_CP1_LIC100_LLCV_POS_CTRL_PCT" -cp_regen_temp_alrm "H0_VAC_LY_CP1_TSH103_REGEN_TEMP_ALRM" -io_out_ma "H0_VAC_LY_TERM_M22_CHAN2_OUT_MA") + "`r`n`r`n`r`n"


$implementation_text += (ion_pump_controller_ipcmini_implementation -ifo "H0" -sys "VAC" -bld "FCES" -loc "IPFCC6" -inst "C6" -io_in_hv_volts "H0_VAC_?_TERM_?_CHAN?_IN_VOLTS" -io_in_press_volts "H0_VAC_?_TERM_?_CHAN?_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (ion_pump_controller_ipcmini_implementation -ifo "H0" -sys "VAC" -bld "FCES" -loc "IPFCC8" -inst "C8" -io_in_hv_volts "H0_VAC_?_TERM_?_CHAN?_IN_VOLTS" -io_in_press_volts "H0_VAC_?_TERM_?_CHAN?_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "FCES" -loc "IPFCC9" -inst "C9" -io_in_volts "H0_VAC_FCES_TERM_1_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "FCES" -loc "IPFCD1" -inst "D1" -io_in_volts "H0_VAC_FCES_TERM_1_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (ion_pump_controller_gamma_single_channel_implementation -ifo "H0" -sys "VAC" -bld "FCES" -loc "IPFCH8A" -inst "H8A" -io_in_press_volts "H0_VAC_FCES_TERM_1_CHAN3_IN_VOLTS" -pump_size "500") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "FCES" -loc "IP26" -inst "126" -io_in_volts "H0_VAC_FCES_TERM_1_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# EtherCAT devices engineering units to counts


$implementation_text += (el4024_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M22") + "`r`n`r`n`r`n"

$implementation_text += (el4024_implementation -ifo "H0" -sys "VAC" -bld "LY" -term "TERM_M23")


# Code formatting
$implementation_text = columns -text $implementation_text -sep "|"

$implementation_text
}
