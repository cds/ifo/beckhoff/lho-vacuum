﻿# Patrick J. Thomas, California Institute of Technology, LIGO Hanford


. "./message_filter.ps1"
. "./h0vacmr.ps1"


$root_path = Split-Path -Parent (Split-Path -Parent (Split-Path -Parent $MyInvocation.MyCommand.Definition))

$solution_name = "H0VACMR"
$solution_folder_path = [System.IO.Path]::GetFullPath((Join-Path $root_path ".\Target\H0VACMR\H0VACMR"))
$solution_file_path = [System.IO.Path]::GetFullPath((Join-Path $root_path ".\Target\H0VACMR\H0VACMR\H0VACMR.sln"))

$project_name = "H0VACMR"
$project_folder_path = [System.IO.Path]::GetFullPath((Join-Path $root_path ".\Target\H0VACMR\H0VACMR\H0VACMR"))

$template_path = "C:\TwinCAT\3.1\Components\Base\PrjTemplate\TwinCAT Project.tsproj"


if (Test-Path $solution_folder_path -PathType Container) {
    Remove-Item $solution_folder_path -Recurse -Force
}

New-Item $solution_folder_path -ItemType Directory


AddMessageFilterClass

[EnvDteUtils.MessageFilter]::Register()

$dte = New-Object -ComObject "VisualStudio.DTE.16.0"
#$dte.MainWindow.Activate()

$solution = $dte.Solution
$solution.Create($solution_folder_path, $solution_name)
$solution.SaveAs($solution_file_path)

$project = $solution.AddFromTemplate($template_path, $project_folder_path, $project_name)
$system_manager = $project.Object


# Creates the device.
$tiid = $system_manager.LookupTreeItem("TIID")
$ethercat_master = $tiid.CreateChild("EtherCAT Master", 111)

# Creates the terminals.

$term = $ethercat_master.CreateChild("Term M0 (EK1101)", 9099, "", "EK1101-0000-0018")

$term = $term.CreateChild("Term M1 (EL1004)", 9099, "", "EL1004-0000-0018")
$term = $term.CreateChild("Term M2 (EL1004)", 9099, "", "EL1004-0000-0018")
$term = $term.CreateChild("Term M3 (EL1004)", 9099, "", "EL1004-0000-0018")
$term = $term.CreateChild("Term M4 (EL1004)", 9099, "", "EL1004-0000-0018")
$term = $term.CreateChild("Term M5 (EL1004)", 9099, "", "EL1004-0000-0018")

$term = $term.CreateChild("Term M6 (EL2624)", 9099, "", "EL2624-0000-0018")
$term = $term.CreateChild("Term M7 (EL2624)", 9099, "", "EL2624-0000-0018")
$term = $term.CreateChild("Term M8 (EL2624)", 9099, "", "EL2624-0000-0018")
$term = $term.CreateChild("Term M9 (EL2624)", 9099, "", "EL2624-0000-0018")

$term = $term.CreateChild("Term M10 (EL9400)", 9099, "", "EL9400")

$term = $term.CreateChild("Term M11 (EL2624)", 9099, "", "EL2624-0000-0018")
$term = $term.CreateChild("Term M12 (EL2624)", 9099, "", "EL2624-0000-0018")
$term = $term.CreateChild("Term M13 (EL2624)", 9099, "", "EL2624-0000-0018")
$term = $term.CreateChild("Term M14 (EL2624)", 9099, "", "EL2624-0000-0018")
$term = $term.CreateChild("Term M15 (EL2624)", 9099, "", "EL2624-0000-0018")

$term = $term.CreateChild("Term M16 (EL3004)", 9099, "", "EL3004-0000-0020")
$term = $term.CreateChild("Term M17 (EL3004)", 9099, "", "EL3004-0000-0020")
$term = $term.CreateChild("Term M18 (EL3004)", 9099, "", "EL3004-0000-0020")
$term = $term.CreateChild("Term M19 (EL3004)", 9099, "", "EL3004-0000-0020")

$term = $term.CreateChild("Term M20 (EL9400)", 9099, "", "EL9400")

$term = $term.CreateChild("Term M21 (EL3004)", 9099, "", "EL3004-0000-0020")
$term = $term.CreateChild("Term M22 (EL3004)", 9099, "", "EL3004-0000-0020")
$term = $term.CreateChild("Term M23 (EL3004)", 9099, "", "EL3004-0000-0020")
$term = $term.CreateChild("Term M24 (EL3004)", 9099, "", "EL3004-0000-0020")
$term = $term.CreateChild("Term M25 (EL3004)", 9099, "", "EL3004-0000-0020")
$term = $term.CreateChild("Term M26 (EL3004)", 9099, "", "EL3004-0000-0020")
$term = $term.CreateChild("Term M27 (EL3004)", 9099, "", "EL3004-0000-0020")
$term = $term.CreateChild("Term M28 (EL3004)", 9099, "", "EL3004-0000-0020")

$term = $term.CreateChild("Term M29 (EL9011)", 9099, "", "EL9011")


$term = $ethercat_master.CreateChild("Term L0 (EK1101)", 9099, "", "EK1101-0000-0018")

$term = $term.CreateChild("Term L1 (EL3024)", 9099, "", "EL3024-0000-0018")
$term = $term.CreateChild("Term L2 (EL3024)", 9099, "", "EL3024-0000-0018")
$term = $term.CreateChild("Term L3 (EL3024)", 9099, "", "EL3024-0000-0018")
$term = $term.CreateChild("Term L4 (EL3024)", 9099, "", "EL3024-0000-0018")

$term = $term.CreateChild("Term L5 (EL9011)", 9099, "", "EL9011")


# Creates the PLC project.
$tipc = $system_manager.LookupTreeItem("TIPC")
$tipc.CreateChild("PLC1", 0, "", "Standard PLC Template.plcproj")

# Adds a reference to the Vacuum library.
$references = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^References")
$references.AddLibrary("Vacuum", "*", "LIGO")


# Creates the GVL file.
$gvls_folder = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^GVLs")
$gvls_folder.CreateChild("GVL", 615)


# Sets the GVL declaration text.
$gvl = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^GVLs^GVL")
$gvl.DeclarationText = gvl_declaration


# Sets the MAIN implementation text.
$main = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^POUs^MAIN")
$main.ImplementationText = main_implementation


# Activates the configuration.
# If this is not done a subsequent activate configuration will not create the links. It is not clear why.
$system_manager.ActivateConfiguration()


$project.Save()
$solution.SaveAs($solution_file_path)

$dte.Quit()

[EnvDteUtils.MessageFilter]::Revoke()
