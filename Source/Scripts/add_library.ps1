﻿# Patrick J. Thomas, California Institute of Technology, LIGO Hanford


. "./message_filter.ps1"


$solution_file_path = "C:\SlowControls\TwinCAT3\Vacuum\LHO\Library\Vacuum\Vacuum.sln"
$library_file_path = "C:\SlowControls\TwinCAT3\Vacuum\LHO\Library\Vacuum\Vacuum.library"


AddMessageFilterClass

[EnvDteUtils.MessageFilter]::Register()

$dte = New-Object -ComObject "VisualStudio.DTE.16.0"

$solution = $dte.Solution

$solution.Open($solution_file_path)

$project = $solution.Projects.Item(1)

$system_manager = $project.Object

# Sets the target platform.
$configuration_manager = $system_manager.ConfigurationManager
$configuration_manager.ActiveTargetPlatform = "TwinCAT RT (x86)"

$plc_project = $system_manager.LookupTreeItem("TIPC^Vacuum^Vacuum Project")
$plc_project.SaveAsLibrary($library_file_path, $true)

$dte.Quit()

[EnvDteUtils.MessageFilter]::Revoke()
