# $Header$

# Patrick J. Thomas, California Institute of Technology, LIGO Hanford


. ".\macro_functions.ps1"
. ".\columns.ps1"


function gvl_declaration {
$declaration_text += "VAR_GLOBAL`r`n"


# EtherCAT devices


$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M1" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M1 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M2" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M2 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M3" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M3 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M4" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M4 (EL1004)") + "`r`n`r`n`r`n"


$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M5" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M5 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M6" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M6 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M7" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M7 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M8" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M8 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M9" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M9 (EL2624)") + "`r`n`r`n`r`n"


$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M11" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M11 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M12" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M12 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M13" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M13 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M14" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M14 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M15" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M15 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M16" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M16 (EL3004)") + "`r`n`r`n`r`n"


$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M17" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M17 (EL3024)") + "`r`n`r`n`r`n"

$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M18" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M18 (EL3024)") + "`r`n`r`n`r`n"


$declaration_text += (el3314_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M19" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M19 (EL3314)") + "`r`n`r`n`r`n"

$declaration_text += (el3314_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M21" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M21 (EL3314)") + "`r`n`r`n`r`n"

$declaration_text += (el3314_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M22" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M22 (EL3314)") + "`r`n`r`n`r`n"


$declaration_text += (el4024_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M23" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M23 (EL4024)") + "`r`n`r`n`r`n"

$declaration_text += (el4024_declaration -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M24" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M24 (EL4024)") + "`r`n`r`n`r`n"


$declaration_text += (bpg402_v1_2_1_0_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "Y5" -inst "PT191" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Pressure Gauge PT191 (BPG 402)") + "`r`n`r`n`r`n"

$declaration_text += (bpg402_v1_2_1_0_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "Y6" -inst "PT192" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Pressure Gauge PT192 (BPG 402)") + "`r`n`r`n`r`n"

$declaration_text += (bpg402_v1_2_1_0_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "Y7" -inst "PT193" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Pressure Gauge PT193 (BPG 402)") + "`r`n`r`n`r`n"

$declaration_text += (_390_micro_ion_atm_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "Y8" -inst "PT132" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Pressure Gauge PT132 (390 Micro-Ion ATM)") + "`r`n`r`n`r`n"


$declaration_text += (bcg450_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "X2" -inst "PT170" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Box 2 (CU1128-B)^Pressure Gauge PT170 (BCG 450)") + "`r`n`r`n`r`n"

$declaration_text += (bcg450_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "Y2" -inst "PT180" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Box 2 (CU1128-B)^Pressure Gauge PT180 (BCG 450)") + "`r`n`r`n`r`n"

$declaration_text += (bcg450_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "Y0" -inst "PT110" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Box 3 (CU1128-C)^Pressure Gauge PT110 (BCG 450)") + "`r`n`r`n`r`n"

$declaration_text += (bpg402_v1_2_1_0_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "X1" -inst "PT140" -link_prefix "TIID^EtherCAT Master^Box 1 (CU1128)^Box 3 (CU1128-C)^Pressure Gauge PT140 (BPG 402)") + "`r`n`r`n`r`n"


$declaration_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# Hardware devices


$declaration_text += (temperature_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "LVEA") + "`r`n`r`n`r`n"

$declaration_text += (temperature_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "BSC7") + "`r`n`r`n`r`n"


$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "HAM7" -inst "185") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "HAM8" -inst "187") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "HAM9" -inst "191") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "BSC4" -inst "141") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "BSC7" -inst "171") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "BM" -inst "188") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "IBM" -inst "122") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "OBM" -inst "136") + "`r`n`r`n`r`n"

$declaration_text += (cyropump_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "190") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "GV2" -inst "129") + "`r`n`r`n`r`n"

$declaration_text += (electric_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "GV2" -inst "129") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "GV4" -inst "149") + "`r`n`r`n`r`n"

$declaration_text += (electric_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "GV4" -inst "149") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "GV7" -inst "179") + "`r`n`r`n`r`n"

$declaration_text += (pneumatic_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "GV7" -inst "179") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "GV8" -inst "189") + "`r`n`r`n`r`n"

$declaration_text += (pneumatic_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "GV8" -inst "189") + "`r`n`r`n`r`n"


$declaration_text += (cc10_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "BSC3" -inst "131") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "X3" -inst "134") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "X3" -inst "134") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "X3" -inst "134") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "X4" -inst "144") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "X4" -inst "144") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "X4" -inst "144") + "`r`n`r`n`r`n"


$declaration_text += (dewar_level_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "155") + "`r`n`r`n`r`n"


$declaration_text += (dewar_lower_temperature_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "154") + "`r`n`r`n`r`n"

$declaration_text += (dewar_upper_temperature_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "155") + "`r`n`r`n`r`n"

$declaration_text += (dewar_future_use_temperature_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "156") + "`r`n`r`n`r`n"


$declaration_text += (pump_level_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "150") + "`r`n`r`n`r`n"

$declaration_text += (liquid_level_control_valve_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "150") + "`r`n`r`n`r`n"


$declaration_text += (discharge_pressure_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "151") + "`r`n`r`n`r`n"


$declaration_text += (discharge_temperature_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "152") + "`r`n`r`n`r`n"

$declaration_text += (discharge_temperature_spare_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "152") + "`r`n`r`n`r`n"

$declaration_text += (discharge_temperature_comparison_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "152") + "`r`n`r`n`r`n"


$declaration_text += (liquid_level_control_valve_enable_position_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "150") + "`r`n`r`n`r`n"

$declaration_text += (liquid_level_control_valve_enable_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "150") + "`r`n`r`n`r`n"


$declaration_text += (regen_temperature_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "153") + "`r`n`r`n`r`n"

$declaration_text += (regen_temperature_alarm_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "153") + "`r`n`r`n`r`n"

$declaration_text += (regen_declaration -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "153") + "`r`n"


$declaration_text += "END_VAR"


$declaration_text = columns -text $declaration_text -sep "|"

$declaration_text
}


#-------------------------------------------------------------------------------


function main_implementation {


# EtherCAT devices error checking


$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M1") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M2") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M3") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M4") + "`r`n`r`n`r`n"


$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M5") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M6") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M7") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M8") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M9") + "`r`n`r`n`r`n"


$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M11") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M12") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M13") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M14") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M15") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M16") + "`r`n`r`n`r`n"


$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M17") + "`r`n`r`n`r`n"

$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M18") + "`r`n`r`n`r`n"


$implementation_text += (el3314_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M19") + "`r`n`r`n`r`n"

$implementation_text += (el3314_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M21") + "`r`n`r`n`r`n"

$implementation_text += (el3314_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M22") + "`r`n`r`n`r`n"


$implementation_text += (el4024_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M23") + "`r`n`r`n`r`n"

$implementation_text += (el4024_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M24") + "`r`n`r`n`r`n"


$implementation_text += (bpg402_v1_2_1_0_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "Y5" -inst "PT191") + "`r`n`r`n`r`n"

$implementation_text += (bpg402_v1_2_1_0_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "Y6" -inst "PT192") + "`r`n`r`n`r`n"

$implementation_text += (bpg402_v1_2_1_0_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "Y7" -inst "PT193") + "`r`n`r`n`r`n"

$implementation_text += (_390_micro_ion_atm_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "Y8" -inst "PT132") + "`r`n`r`n`r`n"


$implementation_text += (bcg450_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "X2" -inst "PT170") + "`r`n`r`n`r`n"

$implementation_text += (bcg450_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "Y2" -inst "PT180") + "`r`n`r`n`r`n"

$implementation_text += (bcg450_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "Y0" -inst "PT110") + "`r`n`r`n`r`n"

$implementation_text += (bpg402_v1_2_1_0_error_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "X1" -inst "PT140") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# EtherCAT devices counts to engineering units


$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M11") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M12") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M13") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M14") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M15") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M16") + "`r`n`r`n`r`n"


$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M17") + "`r`n`r`n`r`n"

$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M18") + "`r`n`r`n`r`n"


$implementation_text += (el3314_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M19") + "`r`n`r`n`r`n"

$implementation_text += (el3314_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M21") + "`r`n`r`n`r`n"

$implementation_text += (el3314_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M22") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# Hardware devices


$implementation_text += (temperature_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "LVEA" -io_in_degc "H0_VAC_LX_TERM_M19_CHAN2_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (temperature_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "BSC7" -io_in_degc "H0_VAC_LX_TERM_M19_CHAN3_IN_DEGC") + "`r`n`r`n`r`n"


$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "HAM7" -inst "185" -io_in_volts "H0_VAC_LX_TERM_M11_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "HAM8" -inst "187" -io_in_volts "H0_VAC_LX_TERM_M11_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "HAM9" -inst "191" -io_in_volts "H0_VAC_LX_TERM_M12_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "BSC4" -inst "141" -io_in_volts "H0_VAC_LX_TERM_M13_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "BSC7" -inst "171" -io_in_volts "H0_VAC_LX_TERM_M12_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "BM" -inst "188" -io_in_volts "H0_VAC_LX_TERM_M12_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "IBM" -inst "122" -io_in_volts "H0_VAC_LX_TERM_M12_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "OBM" -inst "136" -io_in_volts "H0_VAC_LX_TERM_M13_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cyropump_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "190" -io_in_volts "H0_VAC_LX_TERM_M16_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "GV2" -inst "129" -io_in_volts "H0_VAC_LX_TERM_M15_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (electric_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "GV2" -inst "129" -io_in_mtr "H0_VAC_LX_TERM_M1_CHAN2_IN_BOOL" -io_in_close_lim "H0_VAC_LX_TERM_M1_CHAN3_IN_BOOL" -io_in_open_lim "H0_VAC_LX_TERM_M1_CHAN4_IN_BOOL" -io_out_open "H0_VAC_LX_TERM_M6_CHAN3_OUT_BOOL" -io_out_run_mtr "H0_VAC_LX_TERM_M6_CHAN4_OUT_BOOL" -io_out_close "H0_VAC_LX_TERM_M8_CHAN3_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "GV4" -inst "149" -io_in_volts "H0_VAC_LX_TERM_M15_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (electric_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "GV4" -inst "149" -io_in_mtr "H0_VAC_LX_TERM_M2_CHAN1_IN_BOOL" -io_in_close_lim "H0_VAC_LX_TERM_M2_CHAN2_IN_BOOL" -io_in_open_lim "H0_VAC_LX_TERM_M2_CHAN3_IN_BOOL" -io_out_open "H0_VAC_LX_TERM_M7_CHAN1_OUT_BOOL" -io_out_run_mtr "H0_VAC_LX_TERM_M7_CHAN2_OUT_BOOL" -io_out_close "H0_VAC_LX_TERM_M8_CHAN4_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "GV7" -inst "179" -io_in_volts "H0_VAC_LX_TERM_M15_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (pneumatic_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "GV7" -inst "179" -io_in_close_lim "H0_VAC_LX_TERM_M2_CHAN4_IN_BOOL" -io_in_open_lim "H0_VAC_LX_TERM_M3_CHAN1_IN_BOOL" -io_out_open "H0_VAC_LX_TERM_M7_CHAN3_OUT_BOOL" -io_out_close "H0_VAC_LX_TERM_M7_CHAN4_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "GV8" -inst "189" -io_in_volts "H0_VAC_LX_TERM_M15_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (pneumatic_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "GV8" -inst "189" -io_in_close_lim "H0_VAC_LX_TERM_M3_CHAN2_IN_BOOL" -io_in_open_lim "H0_VAC_LX_TERM_M3_CHAN3_IN_BOOL" -io_out_open "H0_VAC_LX_TERM_M8_CHAN1_OUT_BOOL" -io_out_close "H0_VAC_LX_TERM_M8_CHAN2_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (cc10_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "BSC3" -inst "131" -io_in_volts "H0_VAC_LX_TERM_M16_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "X3" -inst "134" -io_in_volts "H0_VAC_LX_TERM_M11_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "X3" -inst "134" -io_out_pwr_ctrl "H0_VAC_LX_TERM_M5_CHAN1_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "X3" -inst "134" -io_in_volts "H0_VAC_LX_TERM_M11_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "X4" -inst "144" -io_in_volts "H0_VAC_LX_TERM_M14_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "X4" -inst "144" -io_out_pwr_ctrl "H0_VAC_LX_TERM_M5_CHAN3_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "X4" -inst "144" -io_in_volts "H0_VAC_LX_TERM_M14_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (dewar_level_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "155" -io_in_ma "H0_VAC_LX_TERM_M17_CHAN3_IN_MA") + "`r`n`r`n`r`n"


$implementation_text += (dewar_lower_temperature_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "154" -io_in_degc "H0_VAC_LX_TERM_M21_CHAN2_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (dewar_upper_temperature_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "155" -io_in_degc "H0_VAC_LX_TERM_M21_CHAN3_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (dewar_future_use_temperature_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "156" -io_in_degc "H0_VAC_LX_TERM_M21_CHAN4_IN_DEGC") + "`r`n`r`n`r`n"


$implementation_text += (pump_level_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "150" -io_in_ma "H0_VAC_LX_TERM_M17_CHAN1_IN_MA") + "`r`n`r`n`r`n"

$implementation_text += (liquid_level_control_valve_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "150" -pirani_press_torr "H0_VAC_LX_X3_PT134A_PRESS_TORR" -discharge_press_psig "H0_VAC_LX_CP2_PT151_DISCHARGE_PRESS_PSIG" -cp_pump_level_pct "H0_VAC_LX_CP2_LT150_PUMP_LEVEL_PCT" -io_out_ma "H0_VAC_LX_TERM_M23_CHAN1_OUT_MA") + "`r`n`r`n`r`n"


$implementation_text += (discharge_pressure_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "151" -io_in_ma "H0_VAC_LX_TERM_M17_CHAN2_IN_MA") + "`r`n`r`n`r`n"


$implementation_text += (discharge_temperature_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "152" -io_in_degc "H0_VAC_LX_TERM_M19_CHAN4_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (discharge_temperature_spare_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "152" -io_in_degc "H0_VAC_LX_TERM_M21_CHAN1_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (discharge_temperature_comparison_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "152") + "`r`n`r`n`r`n"


$implementation_text += (liquid_level_control_valve_enable_position_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "150" -io_in_pos "H0_VAC_LX_TERM_M1_CHAN1_IN_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (liquid_level_control_valve_enable_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "150" -io_out_ctrl "H0_VAC_LX_TERM_M5_CHAN2_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (regen_temperature_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "153" -io_in_degc "H0_VAC_LX_TERM_M19_CHAN1_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (regen_temperature_alarm_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "153" -io_in_temp_alrm "H0_VAC_LX_TERM_M3_CHAN4_IN_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (regen_implementation -ifo "H0" -sys "VAC" -bld "LX" -loc "CP2" -inst "153" -cp_regen_temp_degc "H0_VAC_LX_CP2_TE153A_REGEN_TEMP_DEGC" -gv_a_valve_close_lim "H0_VAC_LX_GV8_ZSC189_VALVE_CLOSE_LIM" -gv_b_valve_close_lim "H0_VAC_LX_GV7_ZSC179_VALVE_CLOSE_LIM" -cp_llcv_pos_pct "H0_VAC_LX_CP2_LIC150_LLCV_POS_CTRL_PCT" -cp_regen_temp_alrm "H0_VAC_LX_CP2_TSH153_REGEN_TEMP_ALRM" -io_out_ma "H0_VAC_LX_TERM_M23_CHAN2_OUT_MA") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# EtherCAT devices engineering units to counts


$implementation_text += (el4024_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M23") + "`r`n`r`n`r`n"

$implementation_text += (el4024_implementation -ifo "H0" -sys "VAC" -bld "LX" -term "TERM_M24")


# Code formatting
$implementation_text = columns -text $implementation_text -sep "|"

$implementation_text
}
