# $Header$

# Patrick J. Thomas, California Institute of Technology, LIGO Hanford


. ".\macro_functions.ps1"
. ".\columns.ps1"


function gvl_declaration {
$declaration_text += "VAR_GLOBAL`r`n"


# EtherCAT devices


$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M1" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M1 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M2" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M2 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M3" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M3 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M4" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M4 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M5" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M5 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M6" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M6 (EL1004)") + "`r`n`r`n`r`n"


$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M7" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M7 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M8" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M8 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M9" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M9 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M11" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M11 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M12" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M12 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M13" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M13 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M14" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M14 (EL2624)") + "`r`n`r`n`r`n"


$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M15" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M15 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M16" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M16 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M17" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M17 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M18" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M18 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M19" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M19 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M21" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M21 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M22" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M22 (EL3004)") + "`r`n`r`n`r`n"


$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M23" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M23 (EL3024)") + "`r`n`r`n`r`n"

$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M24" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M24 (EL3024)") + "`r`n`r`n`r`n"

$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M25" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M25 (EL3024)") + "`r`n`r`n`r`n"

$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M26" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M26 (EL3024)") + "`r`n`r`n`r`n"


$declaration_text += (el3314_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L1" -link_prefix "TIID^EtherCAT Master^Term L0 (EK1101)^Term L1 (EL3314)") + "`r`n`r`n`r`n"

$declaration_text += (el3314_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L2" -link_prefix "TIID^EtherCAT Master^Term L0 (EK1101)^Term L2 (EL3314)") + "`r`n`r`n`r`n"

$declaration_text += (el3314_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L3" -link_prefix "TIID^EtherCAT Master^Term L0 (EK1101)^Term L3 (EL3314)") + "`r`n`r`n`r`n"


$declaration_text += (el4024_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L4" -link_prefix "TIID^EtherCAT Master^Term L0 (EK1101)^Term L4 (EL4024)") + "`r`n`r`n`r`n"

$declaration_text += (el4024_declaration -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L5" -link_prefix "TIID^EtherCAT Master^Term L0 (EK1101)^Term L5 (EL4024)") + "`r`n`r`n`r`n"


$declaration_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# Hardware devices


$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "BSC5" -inst "311") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "GV13" -inst "309") + "`r`n`r`n`r`n"

$declaration_text += (electric_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "GV13" -inst "309") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "GV14" -inst "319") + "`r`n`r`n`r`n"

$declaration_text += (electric_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "GV14" -inst "319") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "GV15" -inst "329") + "`r`n`r`n`r`n"

$declaration_text += (electric_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "GV15" -inst "329") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "GV16" -inst "339") + "`r`n`r`n`r`n"

$declaration_text += (electric_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "GV16" -inst "339") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X1" -inst "343") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X1" -inst "343") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X1" -inst "343") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X2" -inst "344") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X2" -inst "344") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X2" -inst "344") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X3" -inst "310") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X3" -inst "310") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X3" -inst "310") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X4" -inst "345") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X4" -inst "345") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X4" -inst "345") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X5" -inst "346") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X5" -inst "346") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "X5" -inst "346") + "`r`n`r`n`r`n"


$declaration_text += (ion_pump_controller_dualvac_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "IP10" -inst "347") + "`r`n`r`n`r`n"


$declaration_text += (instrument_air_declaration -ifo "H0" -sys "VAC" -bld "MX" -inst "399" -low "-5" -high "5") + "`r`n`r`n`r`n"


$declaration_text += (fan_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "1" -inst "370") + "`r`n`r`n`r`n"

$declaration_text += (fan_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "2" -inst "370") + "`r`n`r`n`r`n"


$declaration_text += (dewar_level_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "305") + "`r`n`r`n`r`n"


$declaration_text += (pump_level_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "300") + "`r`n`r`n`r`n"

$declaration_text += (liquid_level_control_valve_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "300") + "`r`n`r`n`r`n"


$declaration_text += (discharge_pressure_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "301") + "`r`n`r`n`r`n"


$declaration_text += (discharge_temperature_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "302") + "`r`n`r`n`r`n"

$declaration_text += (discharge_temperature_spare_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "302") + "`r`n`r`n`r`n"

$declaration_text += (discharge_temperature_comparison_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "302") + "`r`n`r`n`r`n"


$declaration_text += (liquid_level_control_valve_enable_position_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "300") + "`r`n`r`n`r`n"

$declaration_text += (liquid_level_control_valve_enable_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "300") + "`r`n`r`n`r`n"


$declaration_text += (regen_temperature_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "303") + "`r`n`r`n`r`n"

$declaration_text += (regen_temperature_alarm_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "303") + "`r`n`r`n`r`n"

$declaration_text += (regen_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "303") + "`r`n`r`n`r`n"


$declaration_text += (dewar_level_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "355") + "`r`n`r`n`r`n"


$declaration_text += (pump_level_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "350") + "`r`n`r`n`r`n"

$declaration_text += (liquid_level_control_valve_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "350") + "`r`n`r`n`r`n"


$declaration_text += (discharge_pressure_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "351") + "`r`n`r`n`r`n"


$declaration_text += (discharge_temperature_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "352") + "`r`n`r`n`r`n"

$declaration_text += (discharge_temperature_spare_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "352") + "`r`n`r`n`r`n"

$declaration_text += (discharge_temperature_comparison_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "352") + "`r`n`r`n`r`n"


$declaration_text += (liquid_level_control_valve_enable_position_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "350") + "`r`n`r`n`r`n"

$declaration_text += (liquid_level_control_valve_enable_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "350") + "`r`n`r`n`r`n"


$declaration_text += (regen_temperature_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "353") + "`r`n`r`n`r`n"

$declaration_text += (regen_temperature_alarm_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "353") + "`r`n`r`n`r`n"

$declaration_text += (regen_declaration -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "353") + "`r`n`r`n`r`n"


$declaration_text += (h2o_pressure_declaration -ifo "H0" -sys "FMC" -bld "MX" -loc "MR") + "`r`n"


$declaration_text += "END_VAR"


$declaration_text = columns -text $declaration_text -sep "|"

$declaration_text
}


#-------------------------------------------------------------------------------


function main_implementation {


# EtherCAT devices error checking


$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M1") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M2") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M3") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M4") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M5") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M6") + "`r`n`r`n`r`n"


$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M7") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M8") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M9") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M11") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M12") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M13") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M14") + "`r`n`r`n`r`n"


$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M15") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M16") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M17") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M18") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M19") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M21") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M22") + "`r`n`r`n`r`n"


$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M23") + "`r`n`r`n`r`n"

$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M24") + "`r`n`r`n`r`n"

$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M25") + "`r`n`r`n`r`n"

$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M26") + "`r`n`r`n`r`n"


$implementation_text += (el3314_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L1") + "`r`n`r`n`r`n"

$implementation_text += (el3314_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L2") + "`r`n`r`n`r`n"

$implementation_text += (el3314_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L3") + "`r`n`r`n`r`n"


$implementation_text += (el4024_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L4") + "`r`n`r`n`r`n"

$implementation_text += (el4024_error_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L5") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# EtherCAT devices counts to engineering units


$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M15") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M16") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M17") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M18") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M19") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M21") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M22") + "`r`n`r`n`r`n"


$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M23") + "`r`n`r`n`r`n"

$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M24") + "`r`n`r`n`r`n"

$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M25") + "`r`n`r`n`r`n"

$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_M26") + "`r`n`r`n`r`n"


$implementation_text += (el3314_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L1") + "`r`n`r`n`r`n"

$implementation_text += (el3314_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L2") + "`r`n`r`n`r`n"

$implementation_text += (el3314_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L3") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# Hardware devices


$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "BSC5" -inst "311" -io_in_volts "H0_VAC_MX_TERM_M16_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "GV13" -inst "309" -io_in_volts "H0_VAC_MX_TERM_M17_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (electric_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "GV13" -inst "309" -io_in_mtr "H0_VAC_MX_TERM_M1_CHAN3_IN_BOOL" -io_in_close_lim "H0_VAC_MX_TERM_M1_CHAN4_IN_BOOL" -io_in_open_lim "H0_VAC_MX_TERM_M2_CHAN1_IN_BOOL" -io_out_open "H0_VAC_MX_TERM_M8_CHAN4_OUT_BOOL" -io_out_run_mtr "H0_VAC_MX_TERM_M9_CHAN1_OUT_BOOL" -io_out_close "H0_VAC_MX_TERM_M12_CHAN4_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "GV14" -inst "319" -io_in_volts "H0_VAC_MX_TERM_M18_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (electric_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "GV14" -inst "319" -io_in_mtr "H0_VAC_MX_TERM_M2_CHAN2_IN_BOOL" -io_in_close_lim "H0_VAC_MX_TERM_M2_CHAN3_IN_BOOL" -io_in_open_lim "H0_VAC_MX_TERM_M2_CHAN4_IN_BOOL" -io_out_open "H0_VAC_MX_TERM_M9_CHAN2_OUT_BOOL" -io_out_run_mtr "H0_VAC_MX_TERM_M9_CHAN3_OUT_BOOL" -io_out_close "H0_VAC_MX_TERM_M13_CHAN1_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "GV15" -inst "329" -io_in_volts "H0_VAC_MX_TERM_M18_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (electric_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "GV15" -inst "329" -io_in_mtr "H0_VAC_MX_TERM_M3_CHAN1_IN_BOOL" -io_in_close_lim "H0_VAC_MX_TERM_M3_CHAN2_IN_BOOL" -io_in_open_lim "H0_VAC_MX_TERM_M3_CHAN3_IN_BOOL" -io_out_open "H0_VAC_MX_TERM_M9_CHAN4_OUT_BOOL" -io_out_run_mtr "H0_VAC_MX_TERM_M11_CHAN1_OUT_BOOL" -io_out_close "H0_VAC_MX_TERM_M13_CHAN2_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "GV16" -inst "339" -io_in_volts "H0_VAC_MX_TERM_M18_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (electric_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "GV16" -inst "339" -io_in_mtr "H0_VAC_MX_TERM_M3_CHAN4_IN_BOOL" -io_in_close_lim "H0_VAC_MX_TERM_M4_CHAN1_IN_BOOL" -io_in_open_lim "H0_VAC_MX_TERM_M4_CHAN2_IN_BOOL" -io_out_open "H0_VAC_MX_TERM_M11_CHAN2_OUT_BOOL" -io_out_run_mtr "H0_VAC_MX_TERM_M11_CHAN3_OUT_BOOL" -io_out_close "H0_VAC_MX_TERM_M13_CHAN3_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X1" -inst "343" -io_in_volts "H0_VAC_MX_TERM_M16_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X1" -inst "343" -io_out_pwr_ctrl "H0_VAC_MX_TERM_M8_CHAN2_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X1" -inst "343" -io_in_volts "H0_VAC_MX_TERM_M17_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X2" -inst "344" -io_in_volts "H0_VAC_MX_TERM_M15_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X2" -inst "344" -io_out_pwr_ctrl "H0_VAC_MX_TERM_M7_CHAN1_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X2" -inst "344" -io_in_volts "H0_VAC_MX_TERM_M15_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X3" -inst "310" -io_in_volts "H0_VAC_MX_TERM_M16_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X3" -inst "310" -io_out_pwr_ctrl "H0_VAC_MX_TERM_M8_CHAN1_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X3" -inst "310" -io_in_volts "H0_VAC_MX_TERM_M16_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X4" -inst "345" -io_in_volts "H0_VAC_MX_TERM_M15_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X4" -inst "345" -io_out_pwr_ctrl "H0_VAC_MX_TERM_M7_CHAN3_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X4" -inst "345" -io_in_volts "H0_VAC_MX_TERM_M15_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X5" -inst "346" -io_in_volts "H0_VAC_MX_TERM_M17_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X5" -inst "346" -io_out_pwr_ctrl "H0_VAC_MX_TERM_M8_CHAN3_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "X5" -inst "346" -io_in_volts "H0_VAC_MX_TERM_M17_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (ion_pump_controller_dualvac_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "IP10" -inst "347" -io_in_a_fault "H0_VAC_MX_TERM_M4_CHAN3_IN_BOOL" -io_in_a_hv_volts "H0_VAC_MX_TERM_M18_CHAN4_IN_VOLTS" -io_in_a_press_volts "H0_VAC_MX_TERM_M19_CHAN1_IN_VOLTS" -io_out_a_start "H0_VAC_MX_TERM_M11_CHAN4_OUT_BOOL" -io_out_a_stop "H0_VAC_MX_TERM_M12_CHAN1_OUT_BOOL" -io_in_b_fault "H0_VAC_MX_TERM_M4_CHAN4_IN_BOOL" -io_in_b_hv_volts "H0_VAC_MX_TERM_M19_CHAN2_IN_VOLTS" -io_in_b_press_volts "H0_VAC_MX_TERM_M19_CHAN3_IN_VOLTS" -io_out_b_start "H0_VAC_MX_TERM_M12_CHAN2_OUT_BOOL" -io_out_b_stop "H0_VAC_MX_TERM_M12_CHAN3_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (instrument_air_implementation -ifo "H0" -sys "VAC" -bld "MX" -inst "399" -io_in_volts "H0_VAC_MX_TERM_M19_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (fan_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "1" -inst "370" -io_in_1_ma "H0_VAC_MX_TERM_M23_CHAN1_IN_MA" -io_in_2_ma "H0_VAC_MX_TERM_M23_CHAN2_IN_MA") + "`r`n`r`n`r`n"

$implementation_text += (fan_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "2" -inst "370" -io_in_1_ma "H0_VAC_MX_TERM_M23_CHAN3_IN_MA" -io_in_2_ma "H0_VAC_MX_TERM_M23_CHAN4_IN_MA") + "`r`n`r`n`r`n"


$implementation_text += (dewar_level_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "305" -io_in_ma "H0_VAC_MX_TERM_M24_CHAN3_IN_MA") + "`r`n`r`n`r`n"


$implementation_text += (pump_level_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "300" -io_in_ma "H0_VAC_MX_TERM_M24_CHAN1_IN_MA") + "`r`n`r`n`r`n"

$implementation_text += (liquid_level_control_valve_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "300" -pirani_press_torr "H0_VAC_MX_X2_PT344A_PRESS_TORR" -discharge_press_psig "H0_VAC_MX_CP5_PT301_DISCHARGE_PRESS_PSIG" -cp_pump_level_pct "H0_VAC_MX_CP5_LT300_PUMP_LEVEL_PCT" -io_out_ma "H0_VAC_MX_TERM_L4_CHAN1_OUT_MA") + "`r`n`r`n`r`n"


$implementation_text += (discharge_pressure_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "301" -io_in_ma "H0_VAC_MX_TERM_M24_CHAN2_IN_MA") + "`r`n`r`n`r`n"


$implementation_text += (discharge_temperature_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "302" -io_in_degc "H0_VAC_MX_TERM_L1_CHAN2_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (discharge_temperature_spare_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "302" -io_in_degc "H0_VAC_MX_TERM_L1_CHAN3_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (discharge_temperature_comparison_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "302") + "`r`n`r`n`r`n"


$implementation_text += (liquid_level_control_valve_enable_position_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "300" -io_in_pos "H0_VAC_MX_TERM_M1_CHAN1_IN_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (liquid_level_control_valve_enable_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "300" -io_out_ctrl "H0_VAC_MX_TERM_M7_CHAN2_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (regen_temperature_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "303" -io_in_degc "H0_VAC_MX_TERM_L1_CHAN1_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (regen_temperature_alarm_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "303" -io_in_temp_alrm "H0_VAC_MX_TERM_M5_CHAN1_IN_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (regen_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP5" -inst "303" -cp_regen_temp_degc "H0_VAC_MX_CP5_TE303A_REGEN_TEMP_DEGC" -gv_a_valve_close_lim "H0_VAC_MX_GV14_ZSC319_VALVE_CLOSE_LIM" -gv_b_valve_close_lim "H0_VAC_MX_GV13_ZSC309_VALVE_CLOSE_LIM" -cp_llcv_pos_pct "H0_VAC_MX_CP5_LIC300_LLCV_POS_CTRL_PCT" -cp_regen_temp_alrm "H0_VAC_MX_CP5_TSH303_REGEN_TEMP_ALRM" -io_out_ma "H0_VAC_MX_TERM_L4_CHAN2_OUT_MA") + "`r`n`r`n`r`n"


$implementation_text += (dewar_level_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "355" -io_in_ma "H0_VAC_MX_TERM_M25_CHAN2_IN_MA") + "`r`n`r`n`r`n"


$implementation_text += (pump_level_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "350" -io_in_ma "H0_VAC_MX_TERM_M24_CHAN4_IN_MA") + "`r`n`r`n`r`n"

$implementation_text += (liquid_level_control_valve_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "350" -pirani_press_torr "H0_VAC_MX_X4_PT345A_PRESS_TORR" -discharge_press_psig "H0_VAC_MX_CP6_PT351_DISCHARGE_PRESS_PSIG" -cp_pump_level_pct "H0_VAC_MX_CP6_LT350_PUMP_LEVEL_PCT" -io_out_ma "H0_VAC_MX_TERM_L4_CHAN3_OUT_MA") + "`r`n`r`n`r`n"


$implementation_text += (discharge_pressure_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "351" -io_in_ma "H0_VAC_MX_TERM_M25_CHAN1_IN_MA") + "`r`n`r`n`r`n"


$implementation_text += (discharge_temperature_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "352" -io_in_degc "H0_VAC_MX_TERM_L2_CHAN1_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (discharge_temperature_spare_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "352" -io_in_degc "H0_VAC_MX_TERM_L2_CHAN2_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (discharge_temperature_comparison_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "352") + "`r`n`r`n`r`n"


$implementation_text += (liquid_level_control_valve_enable_position_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "350" -io_in_pos "H0_VAC_MX_TERM_M1_CHAN2_IN_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (liquid_level_control_valve_enable_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "350" -io_out_ctrl "H0_VAC_MX_TERM_M7_CHAN4_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (regen_temperature_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "353" -io_in_degc "H0_VAC_MX_TERM_L1_CHAN4_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (regen_temperature_alarm_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "353" -io_in_temp_alrm "H0_VAC_MX_TERM_M5_CHAN2_IN_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (regen_implementation -ifo "H0" -sys "VAC" -bld "MX" -loc "CP6" -inst "353" -cp_regen_temp_degc "H0_VAC_MX_CP6_TE353A_REGEN_TEMP_DEGC" -gv_a_valve_close_lim "H0_VAC_MX_GV16_ZSC339_VALVE_CLOSE_LIM" -gv_b_valve_close_lim "H0_VAC_MX_GV15_ZSC329_VALVE_CLOSE_LIM" -cp_llcv_pos_pct "H0_VAC_MX_CP6_LIC350_LLCV_POS_CTRL_PCT" -cp_regen_temp_alrm "H0_VAC_MX_CP6_TSH353_REGEN_TEMP_ALRM" -io_out_ma "H0_VAC_MX_TERM_L4_CHAN4_OUT_MA") + "`r`n`r`n`r`n"


$implementation_text += (h2o_pressure_implementation -ifo "H0" -sys "FMC" -bld "MX" -loc "MR" -io_in_volts "H0_VAC_MX_TERM_M21_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# EtherCAT devices engineering units to counts


$implementation_text += (el4024_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L4") + "`r`n`r`n`r`n"

$implementation_text += (el4024_implementation -ifo "H0" -sys "VAC" -bld "MX" -term "TERM_L5")


# Code formatting
$implementation_text = columns -text $implementation_text -sep "|"

$implementation_text
}
