# $Header$

# Patrick J. Thomas, California Institute of Technology, LIGO Hanford


. ".\macro_functions.ps1"
. ".\columns.ps1"


function gvl_declaration {
$declaration_text += "VAR_GLOBAL`r`n"


# EtherCAT devices


$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M1" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M1 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M2" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M2 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M3" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M3 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M4" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M4 (EL1004)") + "`r`n`r`n`r`n"


$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M5" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M5 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M6" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M6 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M7" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M7 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M8" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M8 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M9" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M9 (EL2624)") + "`r`n`r`n`r`n"


$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M11" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M11 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M12" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M12 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M13" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M13 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M14" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M14 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M15" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M15 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M16" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M16 (EL3004)") + "`r`n`r`n`r`n"


$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M17" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M17 (EL3024)") + "`r`n`r`n`r`n"

$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M18" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M18 (EL3024)") + "`r`n`r`n`r`n"

$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M19" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M19 (EL3024)") + "`r`n`r`n`r`n"


$declaration_text += (el3314_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M21" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M21 (EL3314)") + "`r`n`r`n`r`n"

$declaration_text += (el3314_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M22" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M22 (EL3314)") + "`r`n`r`n`r`n"


$declaration_text += (el4024_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M23" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M23 (EL4024)") + "`r`n`r`n`r`n"

$declaration_text += (el4024_declaration -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M24" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M24 (EL4024)") + "`r`n`r`n`r`n"


$declaration_text += (bpg402_v1_2_1_0_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "X4" -inst "PT525" -link_prefix "TIID^EtherCAT Master^Pressure Gauge ETM (BPG 402)") + "`r`n`r`n`r`n"

$declaration_text += (bpg402_v1_2_1_0_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "X5" -inst "PT526" -link_prefix "TIID^EtherCAT Master^Pressure Gauge NEG (BPG 402)") + "`r`n`r`n`r`n"

$declaration_text += (bpg402_v1_2_1_0_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "X6" -inst "PT527" -link_prefix "TIID^EtherCAT Master^Pressure Gauge X2-8 (BPG 402)") + "`r`n`r`n`r`n"


$declaration_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# Hardware devices


$declaration_text += (ion_pump_controller_gamma_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "IP18" -inst "527") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_minivac_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "IP16" -inst "523") + "`r`n`r`n`r`n"


$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "BSC5" -inst "525") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "BSC9" -inst "511") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "GV19" -inst "509") + "`r`n`r`n`r`n"

$declaration_text += (electric_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "GV19" -inst "509") + "`r`n`r`n`r`n"


$declaration_text += (gate_valve_annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "GV20" -inst "519") + "`r`n`r`n`r`n"

$declaration_text += (electric_gate_valve_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "GV20" -inst "519") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "X1" -inst "523") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "X1" -inst "523") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "X1" -inst "523") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "X2" -inst "524") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "X2" -inst "524") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "X2" -inst "524") + "`r`n`r`n`r`n"


$declaration_text += (pirani_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "X3" -inst "510") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_power_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "X3" -inst "510") + "`r`n`r`n`r`n"

$declaration_text += (cold_cathode_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "X3" -inst "510") + "`r`n`r`n`r`n"


$declaration_text += (ion_pump_controller_gamma_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "IP12" -inst "522") + "`r`n`r`n`r`n"


$declaration_text += (instrument_air_declaration -ifo "H0" -sys "VAC" -bld "EX" -inst "599" -low "-5" -high "10") + "`r`n`r`n`r`n"


$declaration_text += (fan_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "1" -inst "570") + "`r`n`r`n`r`n"

$declaration_text += (fan_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "2" -inst "570") + "`r`n`r`n`r`n"


$declaration_text += (dewar_level_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "505") + "`r`n`r`n`r`n"


$declaration_text += (pump_level_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "500") + "`r`n`r`n`r`n"

$declaration_text += (liquid_level_control_valve_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "500") + "`r`n`r`n`r`n"


$declaration_text += (discharge_pressure_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "501") + "`r`n`r`n`r`n"


$declaration_text += (discharge_temperature_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "502") + "`r`n`r`n`r`n"

$declaration_text += (discharge_temperature_spare_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "502") + "`r`n`r`n`r`n"

$declaration_text += (discharge_temperature_comparison_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "502") + "`r`n`r`n`r`n"


$declaration_text += (liquid_level_control_valve_enable_position_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "500") + "`r`n`r`n`r`n"

$declaration_text += (liquid_level_control_valve_enable_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "500") + "`r`n`r`n`r`n"


$declaration_text += (regen_temperature_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "503") + "`r`n`r`n`r`n"

$declaration_text += (regen_temperature_alarm_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "503") + "`r`n`r`n`r`n"

$declaration_text += (regen_declaration -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "503") + "`r`n`r`n`r`n"


$declaration_text += (h2o_pressure_declaration -ifo "H0" -sys "FMC" -bld "EX" -loc "MR") + "`r`n"


$declaration_text += "END_VAR"


$declaration_text = columns -text $declaration_text -sep "|"

$declaration_text
}


#-------------------------------------------------------------------------------


function main_implementation {


# EtherCAT devices error checking


$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M1") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M2") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M3") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M4") + "`r`n`r`n`r`n"


$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M5") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M6") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M7") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M8") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M9") + "`r`n`r`n`r`n"


$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M11") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M12") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M13") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M14") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M15") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M16") + "`r`n`r`n`r`n"


$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M17") + "`r`n`r`n`r`n"

$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M18") + "`r`n`r`n`r`n"

$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M19") + "`r`n`r`n`r`n"


$implementation_text += (el3314_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M21") + "`r`n`r`n`r`n"

$implementation_text += (el3314_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M22") + "`r`n`r`n`r`n"


$implementation_text += (el4024_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M23") + "`r`n`r`n`r`n"

$implementation_text += (el4024_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M24") + "`r`n`r`n`r`n"


$implementation_text += (bpg402_v1_2_1_0_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "X4" -inst "PT525") + "`r`n`r`n`r`n"

$implementation_text += (bpg402_v1_2_1_0_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "X5" -inst "PT526") + "`r`n`r`n`r`n"

$implementation_text += (bpg402_v1_2_1_0_error_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "X6" -inst "PT527") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# EtherCAT devices counts to engineering units


$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M11") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M12") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M13") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M14") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M15") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M16") + "`r`n`r`n`r`n"


$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M17") + "`r`n`r`n`r`n"

$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M18") + "`r`n`r`n`r`n"

$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M19") + "`r`n`r`n`r`n"


$implementation_text += (el3314_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M21") + "`r`n`r`n`r`n"

$implementation_text += (el3314_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M22") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# Hardware devices


$implementation_text += (ion_pump_controller_gamma_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "IP18" -inst "527" -io_in_hv_volts "H0_VAC_EX_TERM_M14_CHAN1_IN_VOLTS" -io_in_press_volts "H0_VAC_EX_TERM_M14_CHAN2_IN_VOLTS" -pump_size "1200") + "`r`n`r`n`r`n"

$implementation_text += (ion_pump_controller_minivac_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "IP16" -inst "523" -io_in_hv_volts "H0_VAC_EX_TERM_M15_CHAN1_IN_VOLTS" -io_in_press_volts "H0_VAC_EX_TERM_M15_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "BSC5" -inst "525" -io_in_volts "H0_VAC_EX_TERM_M12_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "BSC9" -inst "511" -io_in_volts "H0_VAC_EX_TERM_M11_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "GV19" -inst "509" -io_in_volts "H0_VAC_EX_TERM_M13_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (electric_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "GV19" -inst "509" -io_in_mtr "H0_VAC_EX_TERM_M1_CHAN2_IN_BOOL" -io_in_close_lim "H0_VAC_EX_TERM_M1_CHAN3_IN_BOOL" -io_in_open_lim "H0_VAC_EX_TERM_M1_CHAN4_IN_BOOL" -io_out_open "H0_VAC_EX_TERM_M6_CHAN1_OUT_BOOL" -io_out_run_mtr "H0_VAC_EX_TERM_M6_CHAN2_OUT_BOOL" -io_out_close "H0_VAC_EX_TERM_M8_CHAN1_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (gate_valve_annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "GV20" -inst "519" -io_in_volts "H0_VAC_EX_TERM_M13_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (electric_gate_valve_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "GV20" -inst "519" -io_in_mtr "H0_VAC_EX_TERM_M2_CHAN1_IN_BOOL" -io_in_close_lim "H0_VAC_EX_TERM_M2_CHAN2_IN_BOOL" -io_in_open_lim "H0_VAC_EX_TERM_M2_CHAN3_IN_BOOL" -io_out_open "H0_VAC_EX_TERM_M6_CHAN3_OUT_BOOL" -io_out_run_mtr "H0_VAC_EX_TERM_M6_CHAN4_OUT_BOOL" -io_out_close "H0_VAC_EX_TERM_M8_CHAN2_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "X1" -inst "523" -io_in_volts "H0_VAC_EX_TERM_M12_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "X1" -inst "523" -io_out_pwr_ctrl "H0_VAC_EX_TERM_M5_CHAN4_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "X1" -inst "523" -io_in_volts "H0_VAC_EX_TERM_M12_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "X2" -inst "524" -io_in_volts "H0_VAC_EX_TERM_M11_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "X2" -inst "524" -io_out_pwr_ctrl "H0_VAC_EX_TERM_M5_CHAN1_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "X2" -inst "524" -io_in_volts "H0_VAC_EX_TERM_M11_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (pirani_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "X3" -inst "510" -io_in_volts "H0_VAC_EX_TERM_M11_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_power_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "X3" -inst "510" -io_out_pwr_ctrl "H0_VAC_EX_TERM_M5_CHAN3_OUT_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (cold_cathode_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "X3" -inst "510" -io_in_volts "H0_VAC_EX_TERM_M12_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (ion_pump_controller_gamma_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "IP12" -inst "522" -io_in_hv_volts "H0_VAC_EX_TERM_M13_CHAN3_IN_VOLTS" -io_in_press_volts "H0_VAC_EX_TERM_M13_CHAN4_IN_VOLTS" -pump_size "1200") + "`r`n`r`n`r`n"


$implementation_text += (instrument_air_implementation -ifo "H0" -sys "VAC" -bld "EX" -inst "599" -io_in_volts "H0_VAC_EX_TERM_M14_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (fan_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "1" -inst "570" -io_in_1_ma "H0_VAC_EX_TERM_M17_CHAN1_IN_MA" -io_in_2_ma "H0_VAC_EX_TERM_M17_CHAN2_IN_MA") + "`r`n`r`n`r`n"

$implementation_text += (fan_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "2" -inst "570" -io_in_1_ma "H0_VAC_EX_TERM_M17_CHAN3_IN_MA" -io_in_2_ma "H0_VAC_EX_TERM_M17_CHAN4_IN_MA") + "`r`n`r`n`r`n"


$implementation_text += (dewar_level_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "505" -io_in_ma "H0_VAC_EX_TERM_M18_CHAN3_IN_MA") + "`r`n`r`n`r`n"


$implementation_text += (pump_level_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "500" -io_in_ma "H0_VAC_EX_TERM_M18_CHAN1_IN_MA") + "`r`n`r`n`r`n"

$implementation_text += (liquid_level_control_valve_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "500" -pirani_press_torr "H0_VAC_EX_X2_PT524A_PRESS_TORR" -discharge_press_psig "H0_VAC_EX_CP8_PT501_DISCHARGE_PRESS_PSIG" -cp_pump_level_pct "H0_VAC_EX_CP8_LT500_PUMP_LEVEL_PCT" -io_out_ma "H0_VAC_EX_TERM_M23_CHAN1_OUT_MA") + "`r`n`r`n`r`n"


$implementation_text += (discharge_pressure_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "501" -io_in_ma "H0_VAC_EX_TERM_M18_CHAN2_IN_MA") + "`r`n`r`n`r`n"


$implementation_text += (discharge_temperature_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "502" -io_in_degc "H0_VAC_EX_TERM_M21_CHAN2_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (discharge_temperature_spare_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "502" -io_in_degc "H0_VAC_EX_TERM_M21_CHAN3_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (discharge_temperature_comparison_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "502") + "`r`n`r`n`r`n"


$implementation_text += (liquid_level_control_valve_enable_position_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "500" -io_in_pos "H0_VAC_EX_TERM_M1_CHAN1_IN_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (liquid_level_control_valve_enable_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "500" -io_out_ctrl "H0_VAC_EX_TERM_M5_CHAN2_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (regen_temperature_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "503" -io_in_degc "H0_VAC_EX_TERM_M21_CHAN1_IN_DEGC") + "`r`n`r`n`r`n"

$implementation_text += (regen_temperature_alarm_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "503" -io_in_temp_alrm "H0_VAC_EX_TERM_M3_CHAN2_IN_BOOL") + "`r`n`r`n`r`n"

$implementation_text += (regen_implementation -ifo "H0" -sys "VAC" -bld "EX" -loc "CP8" -inst "503" -cp_regen_temp_degc "H0_VAC_EX_CP8_TE503A_REGEN_TEMP_DEGC" -gv_a_valve_close_lim "H0_VAC_EX_GV20_ZSC519_VALVE_CLOSE_LIM" -gv_b_valve_close_lim "H0_VAC_EX_GV19_ZSC509_VALVE_CLOSE_LIM" -cp_llcv_pos_pct "H0_VAC_EX_CP8_LIC500_LLCV_POS_CTRL_PCT" -cp_regen_temp_alrm "H0_VAC_EX_CP8_TSH503_REGEN_TEMP_ALRM" -io_out_ma "H0_VAC_EX_TERM_M23_CHAN2_OUT_MA") + "`r`n`r`n`r`n"


$implementation_text += (h2o_pressure_implementation -ifo "H0" -sys "FMC" -bld "EX" -loc "MR" -io_in_volts "H0_VAC_EX_TERM_M14_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# EtherCAT devices engineering units to counts


$implementation_text += (el4024_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M23") + "`r`n`r`n`r`n"

$implementation_text += (el4024_implementation -ifo "H0" -sys "VAC" -bld "EX" -term "TERM_M24")


# Code formatting
$implementation_text = columns -text $implementation_text -sep "|"

$implementation_text
}
