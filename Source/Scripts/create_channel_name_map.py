# $Header$

# Patrick J. Thomas, California Institute of Technology, LIGO Hanford

# Parses the .tpy file at the supplied path. For each channel containing an OPC_PROP[8800] comment, the channel name supplied with the OPC_PROP[8800] comment is printed, followed by a space and then the name of the channel containing the OPC_PROP[8800] comment.

import re
import sys
import xml.etree.ElementTree as ET


tree = ET.parse(sys.argv[1])

root = tree.getroot()

symbols = root.find('Symbols')
for symbol in symbols:
	symbol_name = symbol.find('Name')

	symbol_properties = symbol.find('Properties')
	if symbol_properties is not None:
		for symbol_property in symbol_properties:
			symbol_property_name = symbol_property.find('Name')
			symbol_property_value = symbol_property.find('Value')

			if symbol_property_name.text.strip() == 'OPC_PROP[8800]':
				# Gets the portion of the symbol name following the last period
				channel_name = re.split(r'\.', symbol_name.text)
				channel_name = channel_name[-1]

				# Replaces the first underscore in the channel name with a colon and the second underscore in the channel name with a hyphen
				channel_name = re.split(r'_', channel_name, 2)
				channel_name = channel_name[0] + ':' + channel_name[1] + '-' + channel_name[2]


				# Replaces the first underscore in the OPC_PROP[8800] channel name with a colon
				if symbol_property_value is None:
					error_message = 'Error: No value declared for OPC_PROP[8800] for channel ' + channel_name
					sys.exit(error_message)
					
				alias_channel_name = re.split(r'_', symbol_property_value.text, 1)
				alias_channel_name = alias_channel_name[0] + ':' + alias_channel_name[1]


				print alias_channel_name + ' ' + channel_name
