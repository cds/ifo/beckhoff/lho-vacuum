# $Header$

# Patrick J. Thomas, California Institute of Technology, LIGO Hanford


. ".\macro_functions.ps1"
. ".\columns.ps1"


function gvl_declaration {
$declaration_text += "VAR_GLOBAL`r`n"


# EtherCAT devices


$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M1" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M1 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M2" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M2 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M3" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M3 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M4" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M4 (EL1004)") + "`r`n`r`n`r`n"

$declaration_text += (el1004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M5" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M5 (EL1004)") + "`r`n`r`n`r`n"


$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M6" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M6 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M7" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M7 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M8" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M8 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M9" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M9 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M11" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M11 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M12" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M12 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M13" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M13 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M14" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M14 (EL2624)") + "`r`n`r`n`r`n"

$declaration_text += (el2624_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M15" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M15 (EL2624)") + "`r`n`r`n`r`n"


$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M16" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M16 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M17" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M17 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M18" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M18 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M19" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M19 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M21" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M21 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M22" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M22 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M23" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M23 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M24" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M24 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M25" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M25 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M26" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M26 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M27" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M27 (EL3004)") + "`r`n`r`n`r`n"

$declaration_text += (el3004_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M28" -link_prefix "TIID^EtherCAT Master^Term M0 (EK1101)^Term M28 (EL3004)") + "`r`n`r`n`r`n"


$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_L1" -link_prefix "TIID^EtherCAT Master^Term L0 (EK1101)^Term L1 (EL3024)") + "`r`n`r`n`r`n"

$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_L2" -link_prefix "TIID^EtherCAT Master^Term L0 (EK1101)^Term L2 (EL3024)") + "`r`n`r`n`r`n"

$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_L3" -link_prefix "TIID^EtherCAT Master^Term L0 (EK1101)^Term L3 (EL3024)") + "`r`n`r`n`r`n"

$declaration_text += (el3024_declaration -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_L4" -link_prefix "TIID^EtherCAT Master^Term L0 (EK1101)^Term L4 (EL3024)") + "`r`n`r`n`r`n"


$declaration_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# Hardware devices


$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "BSC1" -inst "111") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "BSC2" -inst "121") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "BSC3" -inst "131") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "HAM1" -inst "115") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "HAM2" -inst "117") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "HAM3" -inst "125") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "HAM4" -inst "127") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "HAM5" -inst "135") + "`r`n`r`n`r`n"

$declaration_text += (annulus_ion_pump_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "HAM6" -inst "137") + "`r`n`r`n`r`n"


$declaration_text += (ion_pump_controller_dualvac_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IP1" -inst "161") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_dualvac_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IP2" -inst "162") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_dualvac_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IP3" -inst "163") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_dualvac_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IP4" -inst "164") + "`r`n`r`n`r`n"

#$declaration_text += (ion_pump_controller_gamma_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IP5" -inst "165") + "`r`n`r`n`r`n"

#$declaration_text += (ion_pump_controller_gamma_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IP6" -inst "166") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_multivac_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IP7" -inst "167") + "`r`n`r`n`r`n"

$declaration_text += (gauge_controller_mks_937_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IP8" -inst "168") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_dualvac_single_controller_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IP13" -inst "169") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_gamma_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IP14" -inst "170") + "`r`n`r`n`r`n"


$declaration_text += (ion_pump_controller_gamma_single_channel_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCH7" -inst "H7") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_agilent_uhv_single_controller_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCA2" -inst "A2") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_agilent_uhv_single_controller_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCB1" -inst "B1") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_agilent_uhv_single_controller_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCB2" -inst "B2") + "`r`n`r`n`r`n"


$declaration_text += (ion_pump_controller_gamma_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCB4" -inst "B4") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_gamma_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCB5" -inst "B5") + "`r`n`r`n`r`n"

$declaration_text += (ion_pump_controller_gamma_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCC1" -inst "C1") + "`r`n`r`n`r`n"


$declaration_text += (instrument_air_declaration -ifo "H0" -sys "VAC" -bld "MR" -inst "199" -low "59" -high "127") + "`r`n`r`n`r`n"


$declaration_text += (fan_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "1" -inst "170") + "`r`n`r`n`r`n"

$declaration_text += (fan_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "2" -inst "170") + "`r`n`r`n`r`n"

$declaration_text += (fan_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "3" -inst "170") + "`r`n`r`n`r`n"

$declaration_text += (fan_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "4" -inst "170") + "`r`n`r`n`r`n"

$declaration_text += (fan_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "5" -inst "170") + "`r`n`r`n`r`n"

$declaration_text += (fan_declaration -ifo "H0" -sys "VAC" -bld "MR" -loc "6" -inst "170") + "`r`n"


$declaration_text += "END_VAR"


$declaration_text = columns -text $declaration_text -sep "|"

$declaration_text
}


#-------------------------------------------------------------------------------


function main_implementation {


# EtherCAT devices error checking


$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M1") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M2") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M3") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M4") + "`r`n`r`n`r`n"

$implementation_text += (el1004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M5") + "`r`n`r`n`r`n"


$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M6") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M7") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M8") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M9") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M11") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M12") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M13") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M14") + "`r`n`r`n`r`n"

$implementation_text += (el2624_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M15") + "`r`n`r`n`r`n"


$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M16") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M17") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M18") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M19") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M21") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M22") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M23") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M24") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M25") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M26") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M27") + "`r`n`r`n`r`n"

$implementation_text += (el3004_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M28") + "`r`n`r`n`r`n"


$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_L1") + "`r`n`r`n`r`n"

$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_L2") + "`r`n`r`n`r`n"

$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_L3") + "`r`n`r`n`r`n"

$implementation_text += (el3024_error_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_L4") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# EtherCAT devices counts to engineering units


$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M16") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M17") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M18") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M19") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M21") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M22") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M23") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M24") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M25") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M26") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M27") + "`r`n`r`n`r`n"

$implementation_text += (el3004_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_M28") + "`r`n`r`n`r`n"


$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_L1") + "`r`n`r`n`r`n"

$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_L2") + "`r`n`r`n`r`n"

$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_L3") + "`r`n`r`n`r`n"

$implementation_text += (el3024_implementation -ifo "H0" -sys "VAC" -bld "MR" -term "TERM_L4") + "`r`n`r`n`r`n"


$implementation_text += @"
	(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)



"@


# Hardware devices


$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "BSC1" -inst "111" -io_in_volts "H0_VAC_MR_TERM_M16_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "BSC2" -inst "121" -io_in_volts "H0_VAC_MR_TERM_M16_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "BSC3" -inst "131" -io_in_volts "H0_VAC_MR_TERM_M17_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "HAM1" -inst "115" -io_in_volts "H0_VAC_MR_TERM_M16_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "HAM2" -inst "117" -io_in_volts "H0_VAC_MR_TERM_M16_CHAN3_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "HAM3" -inst "125" -io_in_volts "H0_VAC_MR_TERM_M17_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "HAM4" -inst "127" -io_in_volts "H0_VAC_MR_TERM_M17_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "HAM5" -inst "135" -io_in_volts "H0_VAC_MR_TERM_M17_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (annulus_ion_pump_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "HAM6" -inst "137" -io_in_volts "H0_VAC_MR_TERM_M18_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (ion_pump_controller_dualvac_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IP1" -inst "161" -io_in_a_fault "H0_VAC_MR_TERM_M1_CHAN1_IN_BOOL" -io_in_a_hv_volts "H0_VAC_MR_TERM_M18_CHAN3_IN_VOLTS" -io_in_a_press_volts "H0_VAC_MR_TERM_M18_CHAN4_IN_VOLTS" -io_out_a_start "H0_VAC_MR_TERM_M6_CHAN1_OUT_BOOL" -io_out_a_stop "H0_VAC_MR_TERM_M6_CHAN2_OUT_BOOL" -io_in_b_fault "H0_VAC_MR_TERM_M1_CHAN2_IN_BOOL" -io_in_b_hv_volts "H0_VAC_MR_TERM_M19_CHAN1_IN_VOLTS" -io_in_b_press_volts "H0_VAC_MR_TERM_M19_CHAN2_IN_VOLTS" -io_out_b_start "H0_VAC_MR_TERM_M6_CHAN3_OUT_BOOL" -io_out_b_stop "H0_VAC_MR_TERM_M6_CHAN4_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (ion_pump_controller_dualvac_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IP2" -inst "162" -io_in_a_fault "H0_VAC_MR_TERM_M1_CHAN3_IN_BOOL" -io_in_a_hv_volts "H0_VAC_MR_TERM_M19_CHAN3_IN_VOLTS" -io_in_a_press_volts "H0_VAC_MR_TERM_M19_CHAN4_IN_VOLTS" -io_out_a_start "H0_VAC_MR_TERM_M7_CHAN1_OUT_BOOL" -io_out_a_stop "H0_VAC_MR_TERM_M7_CHAN2_OUT_BOOL" -io_in_b_fault "H0_VAC_MR_TERM_M1_CHAN4_IN_BOOL" -io_in_b_hv_volts "H0_VAC_MR_TERM_M21_CHAN1_IN_VOLTS" -io_in_b_press_volts "H0_VAC_MR_TERM_M21_CHAN2_IN_VOLTS" -io_out_b_start "H0_VAC_MR_TERM_M7_CHAN3_OUT_BOOL" -io_out_b_stop "H0_VAC_MR_TERM_M7_CHAN4_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (ion_pump_controller_dualvac_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IP3" -inst "163" -io_in_a_fault "H0_VAC_MR_TERM_M2_CHAN1_IN_BOOL" -io_in_a_hv_volts "H0_VAC_MR_TERM_M21_CHAN3_IN_VOLTS" -io_in_a_press_volts "H0_VAC_MR_TERM_M21_CHAN4_IN_VOLTS" -io_out_a_start "H0_VAC_MR_TERM_M8_CHAN1_OUT_BOOL" -io_out_a_stop "H0_VAC_MR_TERM_M8_CHAN2_OUT_BOOL" -io_in_b_fault "H0_VAC_MR_TERM_M2_CHAN2_IN_BOOL" -io_in_b_hv_volts "H0_VAC_MR_TERM_M22_CHAN1_IN_VOLTS" -io_in_b_press_volts "H0_VAC_MR_TERM_M22_CHAN2_IN_VOLTS" -io_out_b_start "H0_VAC_MR_TERM_M8_CHAN3_OUT_BOOL" -io_out_b_stop "H0_VAC_MR_TERM_M8_CHAN4_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (ion_pump_controller_dualvac_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IP4" -inst "164" -io_in_a_fault "H0_VAC_MR_TERM_M2_CHAN3_IN_BOOL" -io_in_a_hv_volts "H0_VAC_MR_TERM_M22_CHAN3_IN_VOLTS" -io_in_a_press_volts "H0_VAC_MR_TERM_M22_CHAN4_IN_VOLTS" -io_out_a_start "H0_VAC_MR_TERM_M9_CHAN1_OUT_BOOL" -io_out_a_stop "H0_VAC_MR_TERM_M9_CHAN2_OUT_BOOL" -io_in_b_fault "H0_VAC_MR_TERM_M2_CHAN4_IN_BOOL" -io_in_b_hv_volts "H0_VAC_MR_TERM_M23_CHAN1_IN_VOLTS" -io_in_b_press_volts "H0_VAC_MR_TERM_M23_CHAN2_IN_VOLTS" -io_out_b_start "H0_VAC_MR_TERM_M9_CHAN3_OUT_BOOL" -io_out_b_stop "H0_VAC_MR_TERM_M9_CHAN4_OUT_BOOL") + "`r`n`r`n`r`n"


#$implementation_text += (ion_pump_controller_gamma_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IP5" -inst "165" -io_in_hv_volts "H0_VAC_MR_TERM_M23_CHAN3_IN_VOLTS" -io_in_press_volts "H0_VAC_MR_TERM_M23_CHAN4_IN_VOLTS" -pump_size "1200") + "`r`n`r`n`r`n"


#$implementation_text += (ion_pump_controller_gamma_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IP6" -inst "166" -io_in_hv_volts "H0_VAC_MR_TERM_M24_CHAN3_IN_VOLTS" -io_in_press_volts "H0_VAC_MR_TERM_M24_CHAN4_IN_VOLTS" -pump_size "1200") + "`r`n`r`n`r`n"


$implementation_text += (ion_pump_controller_multivac_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IP7" -inst "167" -io_in_a_fault "H0_VAC_MR_TERM_M4_CHAN1_IN_BOOL" -io_in_a_hv_volts "H0_VAC_MR_TERM_M25_CHAN3_IN_VOLTS" -io_in_a_press_volts "H0_VAC_MR_TERM_M25_CHAN4_IN_VOLTS" -io_out_a_start "H0_VAC_MR_TERM_M13_CHAN1_OUT_BOOL" -io_out_a_stop "H0_VAC_MR_TERM_M13_CHAN2_OUT_BOOL" -io_in_b_fault "H0_VAC_MR_TERM_M4_CHAN2_IN_BOOL" -io_in_b_hv_volts "H0_VAC_MR_TERM_M26_CHAN1_IN_VOLTS" -io_in_b_press_volts "H0_VAC_MR_TERM_M26_CHAN2_IN_VOLTS" -io_out_b_start "H0_VAC_MR_TERM_M13_CHAN3_OUT_BOOL" -io_out_b_stop "H0_VAC_MR_TERM_M13_CHAN4_OUT_BOOL") + "`r`n`r`n`r`n"


$implementation_text += (gauge_controller_mks_937_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IP8" -inst "168" -io_in_press_volts "H0_VAC_MR_TERM_M26_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (ion_pump_controller_dualvac_single_controller_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IP13" -inst "169" -io_in_hv_volts "H0_VAC_MR_TERM_M19_CHAN1_IN_VOLTS" -io_in_press_volts "H0_VAC_MR_TERM_M19_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (ion_pump_controller_gamma_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IP14" -inst "170" -io_in_hv_volts "H0_VAC_MR_TERM_M27_CHAN1_IN_VOLTS" -io_in_press_volts "H0_VAC_MR_TERM_M27_CHAN2_IN_VOLTS" -pump_size "1200") + "`r`n`r`n`r`n"


$implementation_text += (ion_pump_controller_gamma_single_channel_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCH7" -inst "H7" -io_in_press_volts "H0_VAC_MR_TERM_M27_CHAN3_IN_VOLTS" -pump_size "1200") + "`r`n`r`n`r`n"

$implementation_text += (ion_pump_controller_agilent_uhv_single_controller_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCA2" -inst "A2" -io_in_press_volts "H0_VAC_MR_TERM_M27_CHAN4_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (ion_pump_controller_agilent_uhv_single_controller_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCB1" -inst "B1" -io_in_press_volts "H0_VAC_MR_TERM_M28_CHAN1_IN_VOLTS") + "`r`n`r`n`r`n"

$implementation_text += (ion_pump_controller_agilent_uhv_single_controller_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCB2" -inst "B2" -io_in_press_volts "H0_VAC_MR_TERM_M28_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (ion_pump_controller_gamma_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCB4" -inst "B4" -io_in_hv_volts "H0_VAC_MR_TERM_M23_CHAN3_IN_VOLTS" -io_in_press_volts "H0_VAC_MR_TERM_M23_CHAN4_IN_VOLTS" -pump_size "150") + "`r`n`r`n`r`n"

$implementation_text += (ion_pump_controller_gamma_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCB5" -inst "B5" -io_in_hv_volts "H0_VAC_MR_TERM_M24_CHAN1_IN_VOLTS" -io_in_press_volts "H0_VAC_MR_TERM_M24_CHAN2_IN_VOLTS" -pump_size "150") + "`r`n`r`n`r`n"

$implementation_text += (ion_pump_controller_gamma_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "IPFCC1" -inst "C1" -io_in_hv_volts "H0_VAC_MR_TERM_M24_CHAN3_IN_VOLTS" -io_in_press_volts "H0_VAC_MR_TERM_M24_CHAN4_IN_VOLTS" -pump_size "150") + "`r`n`r`n`r`n"


$implementation_text += (instrument_air_implementation -ifo "H0" -sys "VAC" -bld "MR" -inst "199" -io_in_volts "H0_VAC_MR_TERM_M18_CHAN2_IN_VOLTS") + "`r`n`r`n`r`n"


$implementation_text += (fan_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "1" -inst "170" -io_in_1_ma "H0_VAC_MR_TERM_L1_CHAN1_IN_MA" -io_in_2_ma "H0_VAC_MR_TERM_L1_CHAN2_IN_MA") + "`r`n`r`n`r`n"

$implementation_text += (fan_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "2" -inst "170" -io_in_1_ma "H0_VAC_MR_TERM_L1_CHAN3_IN_MA" -io_in_2_ma "H0_VAC_MR_TERM_L1_CHAN4_IN_MA") + "`r`n`r`n`r`n"

$implementation_text += (fan_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "3" -inst "170" -io_in_1_ma "H0_VAC_MR_TERM_L2_CHAN1_IN_MA" -io_in_2_ma "H0_VAC_MR_TERM_L2_CHAN2_IN_MA") + "`r`n`r`n`r`n"

$implementation_text += (fan_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "4" -inst "170" -io_in_1_ma "H0_VAC_MR_TERM_L2_CHAN3_IN_MA" -io_in_2_ma "H0_VAC_MR_TERM_L2_CHAN4_IN_MA") + "`r`n`r`n`r`n"

$implementation_text += (fan_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "5" -inst "170" -io_in_1_ma "H0_VAC_MR_TERM_L3_CHAN1_IN_MA" -io_in_2_ma "H0_VAC_MR_TERM_L3_CHAN2_IN_MA") + "`r`n`r`n`r`n"

$implementation_text += (fan_implementation -ifo "H0" -sys "VAC" -bld "MR" -loc "6" -inst "170" -io_in_1_ma "H0_VAC_MR_TERM_L3_CHAN3_IN_MA" -io_in_2_ma "H0_VAC_MR_TERM_L3_CHAN4_IN_MA")


# Code formatting
$implementation_text = columns -text $implementation_text -sep "|"

$implementation_text
}
