﻿# Patrick J. Thomas, California Institute of Technology, LIGO Hanford


. "./message_filter.ps1"
. "./h0vacmy.ps1"


$root_path = Split-Path (Split-Path $PSScriptRoot -Parent) -Parent

$solution_name = "H0VACMY"
$solution_folder_path = [System.IO.Path]::GetFullPath((Join-Path $root_path ".\Target\H0VACMY\H0VACMY"))
$solution_file_path = [System.IO.Path]::GetFullPath((Join-Path $root_path ".\Target\H0VACMY\H0VACMY\H0VACMY.sln"))

$project_name = "H0VACMY"
$project_folder_path = [System.IO.Path]::GetFullPath((Join-Path $root_path ".\Target\H0VACMY\H0VACMY\H0VACMY"))

$template_path = "C:\TwinCAT\3.1\Components\Base\PrjTemplate\TwinCAT Project.tsproj"


if (Test-Path $solution_folder_path -PathType Container) {
    Remove-Item $solution_folder_path -Recurse -Force
}

New-Item $solution_folder_path -ItemType Directory


AddMessageFilterClass

[EnvDteUtils.MessageFilter]::Register()

$dte = New-Object -ComObject "VisualStudio.DTE.15.0"
$dte.MainWindow.Activate()

$solution = $dte.Solution
$solution.Create($solution_folder_path, $solution_name)
$solution.SaveAs($solution_file_path)

$project = $solution.AddFromTemplate($template_path, $project_folder_path, $project_name)
$system_manager = $project.Object


# Creates the device.
$tiid = $system_manager.LookupTreeItem("TIID")
$ethercat_master = $tiid.CreateChild("EtherCAT Master", 111)

# Creates the terminals.

$term_m0 = $ethercat_master.CreateChild("Term M0 (EK1101)", 9099, "", "EK1101-0000-0018")

$term_m0.CreateChild("Term M1 (EL1004)", 9099, "", "EL1004-0000-0018")
$term_m0.CreateChild("Term M2 (EL1004)", 9099, "", "EL1004-0000-0018")
$term_m0.CreateChild("Term M3 (EL1004)", 9099, "", "EL1004-0000-0018")
$term_m0.CreateChild("Term M4 (EL1004)", 9099, "", "EL1004-0000-0018")
$term_m0.CreateChild("Term M5 (EL1004)", 9099, "", "EL1004-0000-0018")
$term_m0.CreateChild("Term M6 (EL1004)", 9099, "", "EL1004-0000-0018")

$term_m0.CreateChild("Term M7 (EL2624)", 9099, "", "EL2624-0000-0018")
$term_m0.CreateChild("Term M8 (EL2624)", 9099, "", "EL2624-0000-0018")
$term_m0.CreateChild("Term M9 (EL2624)", 9099, "", "EL2624-0000-0018")

$term_m0.CreateChild("Term M10 (EL9400)", 9099, "", "EL9400")

$term_m0.CreateChild("Term M11 (EL2624)", 9099, "", "EL2624-0000-0018")
$term_m0.CreateChild("Term M12 (EL2624)", 9099, "", "EL2624-0000-0018")
$term_m0.CreateChild("Term M13 (EL2624)", 9099, "", "EL2624-0000-0018")
$term_m0.CreateChild("Term M14 (EL2624)", 9099, "", "EL2624-0000-0018")

$term_m0.CreateChild("Term M15 (EL3004)", 9099, "", "EL3004-0000-0020")
$term_m0.CreateChild("Term M16 (EL3004)", 9099, "", "EL3004-0000-0020")
$term_m0.CreateChild("Term M17 (EL3004)", 9099, "", "EL3004-0000-0020")
$term_m0.CreateChild("Term M18 (EL3004)", 9099, "", "EL3004-0000-0020")
$term_m0.CreateChild("Term M19 (EL3004)", 9099, "", "EL3004-0000-0020")

$term_m0.CreateChild("Term M20 (EL9400)", 9099, "", "EL9400")

$term_m0.CreateChild("Term M21 (EL3004)", 9099, "", "EL3004-0000-0020")
$term_m0.CreateChild("Term M22 (EL3004)", 9099, "", "EL3004-0000-0020")

$term_m0.CreateChild("Term M23 (EL3024)", 9099, "", "EL3024-0000-0018")
$term_m0.CreateChild("Term M24 (EL3024)", 9099, "", "EL3024-0000-0018")
$term_m0.CreateChild("Term M25 (EL3024)", 9099, "", "EL3024-0000-0018")
$term_m0.CreateChild("Term M26 (EL3024)", 9099, "", "EL3024-0000-0018")

$term_m0.CreateChild("Term M27 (EL9011)", 9099, "", "EL9011")


$term_l0 = $ethercat_master.CreateChild("Term L0 (EK1101)", 9099, "", "EK1101-0000-0018")

$term_l0.CreateChild("Term L1 (EL3314)", 9099, "", "EL3314-0000-0019")
$term_l0.CreateChild("Term L2 (EL3314)", 9099, "", "EL3314-0000-0019")
$term_l0.CreateChild("Term L3 (EL3314)", 9099, "", "EL3314-0000-0019")

$term_l0.CreateChild("Term L4 (EL4024)", 9099, "", "EL4024-0000-0017")
$term_l0.CreateChild("Term L5 (EL4024)", 9099, "", "EL4024-0000-0017")

$term_l0.CreateChild("Term L6 (EL9011)", 9099, "", "EL9011")


# Creates the PLC project.
$tipc = $system_manager.LookupTreeItem("TIPC")
$tipc.CreateChild("PLC1", 0, "", "Standard PLC Template.plcproj")

# Adds a reference to the Vacuum library.
$references = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^References")
$references.AddLibrary("Vacuum", "*", "LIGO")


# Creates the GVL file.
$gvls_folder = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^GVLs")
$gvls_folder.CreateChild("GVL", 615)


# Sets the GVL declaration text.
$gvl = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^GVLs^GVL")
$gvl.DeclarationText = gvl_declaration


# Sets the MAIN implementation text.
$main = $system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^POUs^MAIN")
$main.ImplementationText = main_implementation


# Activates the configuration.
# If this is not done a subsequent activate configuration will not create the links. It is not clear why.
$system_manager.ActivateConfiguration()


$project.Save()
$solution.SaveAs($solution_file_path)

$dte.Quit()

[EnvDteUtils.MessageFilter]::Revoke()
