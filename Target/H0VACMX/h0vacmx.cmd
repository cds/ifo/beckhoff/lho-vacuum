# This file requires a blank line at the end.

dbLoadDatabase("./tCat.dbd",0,0)
tCat_registerRecordDeviceDriver(pdbbase)
callbackSetQueueSize(10000)

tcSetScanRate(10, 5)
tcGenerateList ("C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACMX\h0vacmx.txt", "-rv -l")
tcGenerateList ("C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACMX\h0vacmx.req", "-rv -lb")
tcGenerateList ("C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACMX\h0vacmx.ini", "-rv -l -ns")
tcLoadRecords ("C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACMX\H0VACMX\H0VACMX\PLC1\PLC1.tpy", "-rv")

iocInit()
