# This file requires a blank line at the end.

dbLoadDatabase("./tCat.dbd",0,0)
tCat_registerRecordDeviceDriver(pdbbase)
callbackSetQueueSize(10000)

tcSetScanRate(10, 5)
tcGenerateList ("C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACEY\h0vacey.txt", "-rv -l")
tcGenerateList ("C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACEY\h0vacey.req", "-rv -lb")
tcGenerateList ("C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACEY\h0vacey.ini", "-rv -l -ns")
tcLoadRecords ("C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACEY\H0VACEY\H0VACEY\PLC1\PLC1.tpy", "-rv")

iocInit()
