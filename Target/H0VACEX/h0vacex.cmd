# This file requires a blank line at the end.

dbLoadDatabase("./tCat.dbd",0,0)
tCat_registerRecordDeviceDriver(pdbbase)
callbackSetQueueSize(10000)

tcSetScanRate(10, 5)
tcGenerateList ("C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACEX\h0vacex.txt", "-rv -l")
tcGenerateList ("C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACEX\h0vacex.req", "-rv -lb")
tcGenerateList ("C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACEX\h0vacex.ini", "-rv -l -ns")
tcLoadRecords ("C:\SlowControls\TwinCAT3\Vacuum\LHO\Target\H0VACEX\H0VACEX\H0VACEX\PLC1\PLC1.tpy", "-rv")

iocInit()
